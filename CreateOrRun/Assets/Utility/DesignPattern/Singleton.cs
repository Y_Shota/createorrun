using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//シングルトン
public class Singleton<T> where T : class, new()
{
    private static T _instance;
    public static T Instance => _instance ??= new T();

    protected Singleton()
    {
        Debug.Assert(null == _instance);
    }

    public static void Destroy()
    {
        _instance = null;
    }
}