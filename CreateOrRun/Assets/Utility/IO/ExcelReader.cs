using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.IO;
using System.Xml.Linq;

/// <summary>
/// xlsxファイルを読み込み用クラス
/// セル結合、関数などは未対応
/// </summary>
public class ExcelReader
{
    /// <summary>
    /// xlsxのデータを扱いやすい形式に読み込む
    /// </summary>
    /// <param name="path">ファイルパス</param>
    /// <returns>
    /// CsvReader型の実体を返す
    /// 拡張子がcsv出ないまたは存在しない場合nullを返す
    /// </returns>
    public static ExcelReader Read(string path)
    {
        if (!File.Exists(path)) return null;
        if (!IsExcel(path)) return null;

        var excel = new ExcelReader();

        excel.Load(path);

        return excel;
    }

    /// <summary>
    /// パスがxlsxファイルかどうか
    /// </summary>
    /// <param name="path">検証するパス</param>
    /// <returns>csvファイルなら真</returns>
    public static bool IsExcel(string path)
    {
        var extension = Path.GetExtension(path);
        return extension == ".xlsx";
    }

    /// <summary>
    /// シートのデータを格納する連想配列
    /// </summary>
    private readonly Dictionary<string, ExcelSheet> _sheets;

    /// <summary>
    /// シートアクセス用インデクサ
    /// </summary>
    /// <param name="i">取得したいシートのインデックス</param>
    /// <returns>シート</returns>
    public ExcelSheet this[int i]
    {
        get
        {
            foreach (var (value, index) in _sheets.Values.Select((value, index) => (value, index)))
            {
                if (index == i) return value;
            }
            throw new IndexOutOfRangeException();
        }
    }
    /// <summary>
    /// シートアクセス用インデクサ
    /// </summary>
    /// <param name="sheetName">シートの名前</param>
    /// <returns>シート</returns>
    public ExcelSheet this[string sheetName] => _sheets[sheetName];
    /// <summary>
    /// 読み込まれたシートの数を取得する
    /// </summary>
    public int SheetCount => _sheets.Count;
    /// <summary>
    /// シートの名前のリストを取得する
    /// </summary>
    public IReadOnlyList<string> SheetNames => _sheets.Keys.ToList();
    /// <summary>
    /// シートのリストを取得する
    /// </summary>
    public IReadOnlyList<ExcelSheet> Sheets => _sheets.Values.ToList();

    /// <summary>
    /// コンストラクタ
    /// </summary>
    private ExcelReader()
    {
        _sheets = new Dictionary<string, ExcelSheet>();
    }

    /// <summary>
    /// xlsxを読み込み連想配列に落とし込む
    /// </summary>
    /// <param name="path">パス</param>
    private void Load(string path)
    {
        //xlsx形式の圧縮はzipと共通
        var excel = ZipFile.OpenRead(path);

        //シート名とセルの全値取得
        var sheetNames = GetSheetNames(excel);
        var sharedStrings = GetSharedStrings(excel);

        //各シートを読み込む
        foreach (var (name, index) in sheetNames.Select((value, index) => (value, index + 1)))
        {
            var xml = GetXml(excel, @"xl/worksheets/sheet" + index + @".xml");
            _sheets.Add(name, new ExcelSheet(name, xml, sharedStrings));
        }
        excel.Dispose();
    }

    /// <summary>
    /// シートの名前のリストを取得する
    /// </summary>
    /// <param name="excel">読み込むエクセル</param>
    /// <returns>シートの名前のリスト</returns>
    private static IReadOnlyList<string> GetSheetNames(ZipArchive excel)
    {
        //保存用
        var names = new List<string>();

        //シート名を保持しているxmlを開く
        var xml = GetXml(excel, @"xl/workbook.xml");
        if (xml == null) return names;

        //名前空間取得
        var nameSpace = xml.Name.Namespace;

        //全シート名取得
        foreach (var sheets in xml.Elements(nameSpace + "sheets"))
        {
            foreach (var sheet in sheets.Elements(nameSpace + "sheet"))
            {
                var attribute = sheet.Attribute("name");
                if (attribute == null) continue;
                names.Add(attribute.Value);
            }
        }

        return names;
    }

    /// <summary>
    /// エクセル内のセルで使われる全文字列を取得する
    /// </summary>
    /// <param name="excel">読み込むエクセル</param>
    /// <returns>全値</returns>
    private static IReadOnlyList<string> GetSharedStrings(ZipArchive excel)
    {
        //保存用
        var sharedStrings = new List<string>();

        //内部から要素が文字列列挙されたxmlを取得し開く
        var xml = GetXml(excel, @"xl/sharedStrings.xml");
        if (xml == null) return sharedStrings;

        //名前空間取得
        var nameSpace = xml.Name.Namespace;

        //要素全取得
        foreach (var si in xml.Elements(nameSpace + "si"))
        {
            foreach (var t in si.Elements(nameSpace + "t"))
            {
                sharedStrings.Add(t.Value);
            }
        }

        return sharedStrings;
    }

    /// <summary>
    /// エクセルファイルからxmlを抽出する
    /// </summary>
    /// <param name="excel">読み取るエクセル</param>
    /// <param name="path">読み取るxmlのパス</param>
    /// <returns>xml</returns>
    private static XElement GetXml(ZipArchive excel, string path)
    {
        var strEntry = excel.GetEntry(path);
        if (strEntry == null) return null;

        return XElement.Load(strEntry.Open());
    }
}
