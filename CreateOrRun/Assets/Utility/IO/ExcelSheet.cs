using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

/// <summary>
/// エクセルのシートを管理するためのクラス
/// </summary>
public class ExcelSheet
{
    /// <summary>
    /// シートのデータを格納する配列
    /// </summary>
    private readonly List<List<string>> _data;

    /// <summary>
    /// 要素アクセス用
    /// </summary>
    /// <param name="x">X</param>
    /// <param name="y">Y</param>
    /// <returns>要素</returns>
    public string this[int x, int y] => _data[y][x];
    /// <summary>
    /// シートの名前を取得する
    /// </summary>
    public string Name { get; }
    /// <summary>
    /// シートの幅を取得する
    /// </summary>
    public int Width => _data.Max(item => item.Count);
    /// <summary>
    /// シートの高さを取得する
    /// </summary>
    public int Height => _data.Count;
    /// <summary>
    /// シートのサイズを取得する
    /// </summary>
    public Vector2Int Size => new Vector2Int(Width, Height);
    
    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="sheetName">シートの名前</param>
    /// <param name="xml">読み取るxml</param>
    /// <param name="values">セルの値</param>
    public ExcelSheet(string sheetName, XElement xml, IReadOnlyList<string> values)
    {
        Name = sheetName;

        _data = new List<List<string>>();

        //読み込めない場合終了
        if (xml == null) return;

        //名前空間取得
        var nameSpace = xml.Name.Namespace;

        var sheetData = xml.Element(nameSpace + "sheetData");
        if (sheetData == null) return;

        //行取得
        foreach (var row in sheetData.Elements(nameSpace + "row"))
        {
            var list = new List<string>();

            foreach (var c in row.Elements(nameSpace + "c"))
            {
                var typeAttr = c.Attribute("t");
                if (typeAttr == null)
                {
                    list.Add(c.Value);
                }
                else
                {
                    var valueId = int.Parse(c.Value);
                    list.Add(values[valueId]);
                }
            }
            if (list.Count == 0) continue;
            _data.Add(list);
        }
        _data.Reverse();
    }
}
