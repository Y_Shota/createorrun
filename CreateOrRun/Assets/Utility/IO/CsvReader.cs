using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// csvファイルを読み込み用クラス
/// </summary>
public class CsvReader
{
    /// <summary>
    /// csvのデータを扱いやすい形式に読み込む
    /// </summary>
    /// <param name="path">ファイルパス</param>
    /// <returns>
    /// CsvReader型の実体を返す
    /// 拡張子がcsv出ないまたは存在しない場合nullを返す
    /// </returns>
    public static CsvReader Read(string path)
    {
        if (!File.Exists(path)) return null;
        if (!IsCsv(path))       return null;

        var csv = new CsvReader();

        csv.Load(path);

        return csv;
    }

    /// <summary>
    /// パスがcsvファイルかどうか
    /// </summary>
    /// <param name="path">検証するパス</param>
    /// <returns>csvファイルなら真</returns>
    public static bool IsCsv(string path)
    {
        var extension = Path.GetExtension(path);
        return extension == ".csv";
    }

    /// <summary>
    /// csvのデータを格納する配列
    /// </summary>
    private readonly List<List<string>> _data;

    /// <summary>
    /// 要素アクセス用
    /// </summary>
    /// <param name="w"></param>
    /// <param name="h"></param>
    /// <returns>要素</returns>
    public string this[int x, int y] => _data[y][x];
    
    /// <summary>
    /// 幅を取得する
    /// </summary>
    public int Width => _data[0].Count;
    /// <summary>
    /// 高さを取得する
    /// </summary>
    public int Height => _data.Count;
    /// <summary>
    /// サイズを取得する
    /// </summary>
    public Vector2Int Size => new Vector2Int(Width, Height);

    /// <summary>
    /// コンストラクタ
    /// </summary>
    private CsvReader()
    {
        _data = new List<List<string>>();
    }

    /// <summary>
    /// csvを読み込み配列に落とし込む
    /// </summary>
    /// <param name="path"></param>
    private void Load(string path)
    {
        var streamReader = new StreamReader(path);

        while (!streamReader.EndOfStream)
        {
            var list = new List<string>();

            var line = streamReader.ReadLine();
            list.AddRange(line.Split(','));

            _data.Add(list);
        }
        _data.Reverse();
    }
}
