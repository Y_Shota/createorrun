using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

public class AssetLoader
{
    public class AsyncAsset<T>
    {
        public T Result => Instance._loadedAssetMap[Path].Convert<T>().Result;
        public string Path { get; }
        public AsyncOperationStatus Status => Instance._loadedAssetMap[Path].Status;
        public AsyncAsset(string path)
        {
            Path = path;
        }

        public void Wait()
        {
            Instance._loadedAssetMap[Path].WaitForCompletion();
        } 
    }

    private static AssetLoader _instance;
    public static AssetLoader Instance => _instance ??= new AssetLoader();

    private UnityAction<Scene> _sceneUnloaded;
    private readonly Dictionary<string, AsyncOperationHandle> _loadedAssetMap = new Dictionary<string, AsyncOperationHandle>();

    private AssetLoader()
    {
        _sceneUnloaded = scene => Release();
        SceneManager.sceneUnloaded += _sceneUnloaded;
    }

    ~AssetLoader()
    {
        SceneManager.sceneUnloaded -= _sceneUnloaded;
        _sceneUnloaded = null;
    }

    public static void Destroy()
    {
        _instance = null;
    }

    /// <summary>
    /// AddressablesからGameObjectを生成する
    /// </summary>
    /// <param name="path">生成元のパス</param>
    /// <param name="parent">親</param>
    /// <returns>生成したオブジェクト</returns>
    public GameObject Instantiate(string path, Transform parent = null)
    {
        return Instantiate(path, Vector3.zero, Quaternion.identity, parent);
    }
    /// <summary>
    /// AddressablesからGameObjectを生成する
    /// </summary>
    /// <param name="path">生成元のパス</param>
    /// <param name="position">座標</param>
    /// <param name="rotation">回転</param>
    /// <param name="parent">親</param>
    /// <returns>生成したオブジェクト</returns>
    public GameObject Instantiate(string path, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        var handle = InstantiateAsync(path, position, rotation, parent);
        handle.WaitForCompletion();
        return handle.Result;
    }

    /// <summary>
    /// 非同期でAddressablesからGameObjectを生成する
    /// </summary>
    /// <param name="path">生成元のパス</param>
    /// <param name="completed">生成完了時のイベント</param>
    /// <returns>非同期操作用オブジェクト</returns>
    public AsyncOperationHandle<GameObject> InstantiateAsync(string path, Action<GameObject> completed = null)
    {
        return InstantiateAsync(path, Vector3.zero, Quaternion.identity, null, completed);
    }
    /// <summary>
    /// 非同期でAddressablesからGameObjectを生成する
    /// </summary>
    /// <param name="path">生成元のパス</param>
    /// <param name="parent">親オブジェクト</param>
    /// <param name="completed">生成完了時のイベント</param>
    /// <returns>非同期操作用オブジェクト</returns>
    public AsyncOperationHandle<GameObject> InstantiateAsync(string path, Transform parent = null, Action<GameObject> completed = null)
    {
        return InstantiateAsync(path, Vector3.zero, Quaternion.identity, parent, completed);
    }
    /// <summary>
    /// 非同期でAddressablesからGameObjectを生成する
    /// </summary>
    /// <param name="path">生成元のパス</param>
    /// <param name="position">座標</param>
    /// <param name="rotation">回転</param>
    /// <param name="parent">親オブジェクト</param>
    /// <param name="completed">生成完了時のイベント</param>
    /// <returns>非同期操作用オブジェクト</returns>
    public AsyncOperationHandle<GameObject> InstantiateAsync(string path, Vector3 position, Quaternion rotation, Transform parent = null, Action<GameObject> completed = null)
    {
        var handle = Addressables.InstantiateAsync(path, position, rotation, parent);
        if (completed != null)
        {
            handle.Completed += op =>
            {
                if (op.Status != AsyncOperationStatus.Succeeded) return;
                completed(op.Result);
            };
        }
        return handle;  
    }

    /// <summary>
    /// Addressablesからリソースをロードする
    /// </summary>
    /// <typeparam name="T">ロードしたい形式</typeparam>
    /// <param name="path">生成元のパス</param>
    /// <returns>アセット</returns>
    public T LoadAsset<T>(string path)
    {
        AsyncOperationHandle<T> handle;

        //既にロード開始している場合
        if (_loadedAssetMap.ContainsKey(path))
        {
            handle = _loadedAssetMap[path].Convert<T>();
            handle.WaitForCompletion();
            return handle.Result;
        }

        //新規読み込み
        handle = Addressables.LoadAssetAsync<T>(path);
        _loadedAssetMap.Add(path, handle);
        handle.WaitForCompletion();
        return handle.Result;
    }
    public IList<T> LoadAssets<T>(IEnumerable<string> paths)
    {
        AsyncOperationHandle<IList<T>> handle;

        var key = string.Join(", ", paths);

        //既にロード開始している場合
        if (_loadedAssetMap.ContainsKey(key))
        {
            handle = _loadedAssetMap[key].Convert<IList<T>>();
            handle.WaitForCompletion();
            return handle.Result;
        }

        //新規読み込み
        handle = Addressables.LoadAssetsAsync<T>(key, null);
        _loadedAssetMap.Add(key, handle);
        handle.WaitForCompletion();
        return handle.Result;
    }

    public IList<T> LoadAssetsFromLabel<T>(string label)
    {
        AsyncOperationHandle<IList<T>> handle;

        //既にロード開始している場合
        if (_loadedAssetMap.ContainsKey(label))
        {
            handle = _loadedAssetMap[label].Convert<IList<T>>();
            handle.WaitForCompletion();
            return handle.Result;
        }

        //新規読み込み
        handle = Addressables.LoadAssetsAsync<T>(label, null);
        _loadedAssetMap.Add(label, handle);
        handle.WaitForCompletion();
        return handle.Result;
    }

    /// <summary>
    /// 非同期でAddressablesからリソースをロードする
    /// </summary>
    /// <typeparam name="T">ロードしたい形式</typeparam>
    /// <param name="path">生成元のパス</param>
    /// <param name="completed">ロード完了時のイベント</param>
    /// <returns>非同期操作用オブジェクト</returns>
    public AsyncAsset<T> LoadAssetAsync<T>(string path, Action<T> completed = null)
    {
        AsyncOperationHandle<T> handle;

        //既にロード開始している場合
        if (_loadedAssetMap.ContainsKey(path))
        {
            handle = _loadedAssetMap[path].Convert<T>();

            //事後処理がないならすぐ返す
            if (completed == null) return new AsyncAsset<T>(path);

            //読み込み完了時
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                Task.Run(() => completed(handle.Result));
            }
            //読み込み中
            else
            {
                handle.Completed += op =>
                {
                    if (op.Status != AsyncOperationStatus.Succeeded) return;
                    completed(op.Result);
                };
            }
            return new AsyncAsset<T>(path);
        }

        //新規読み込み
        handle = Addressables.LoadAssetAsync<T>(path);
        if (completed != null)
        {
            handle.Completed += op =>
            {
                if (op.Status != AsyncOperationStatus.Succeeded) return;
                completed(op.Result);
            };
        }
        _loadedAssetMap.Add(path, handle);
        return new AsyncAsset<T>(path);
    }

    /// <summary>
    /// 非同期でAddressablesからリソースをロードする
    /// </summary>
    /// <typeparam name="T">ロードしたい形式</typeparam>
    /// <param name="paths">生成元のパス群</param>
    /// <param name="completed">ロード完了時のイベント</param>
    /// <returns>非同期操作用オブジェクト</returns>
    public AsyncAsset<IList<T>> LoadAssetsAsync<T>(IEnumerable<string> paths, Action<IList<T>> completed = null)
    {
        AsyncOperationHandle<IList<T>> handle;

        var key = string.Join(", ", paths);

        //既にロード開始している場合
        if (_loadedAssetMap.ContainsKey(key))
        {
            handle = _loadedAssetMap[key].Convert<IList<T>>();

            //事後処理がないならすぐ返す
            if (completed == null) return new AsyncAsset<IList<T>>(key);

            //読み込み完了時
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                Task.Run(() => completed(handle.Result));
            }
            //読み込み中
            else
            {
                handle.Completed += op =>
                {
                    if (op.Status != AsyncOperationStatus.Succeeded) return;
                    Task.Run(() => completed(handle.Result));
                };
            }
            return new AsyncAsset<IList<T>>(key);
        }

        //新規読み込み
        handle = Addressables.LoadAssetsAsync<T>(paths, null);
        if (completed != null)
        {
            handle.Completed += op =>
            {
                if (op.Status != AsyncOperationStatus.Succeeded) return;
                completed(op.Result);
            };
        }
        _loadedAssetMap.Add(key, handle);
        return new AsyncAsset<IList<T>>(key);
    }

    /// <summary>
    /// 非同期でAddressablesからリソースをロードする
    /// </summary>
    /// <typeparam name="T">ロードしたい形式</typeparam>
    /// <param name="label">生成元のラベル</param>
    /// <param name="completed">ロード完了時のイベント</param>
    /// <returns>非同期操作用オブジェクト</returns>
    public AsyncAsset<IList<T>> LoadAssetsFromLabelAsync<T>(string label, Action<IList<T>> completed = null)
    {
        AsyncOperationHandle<IList<T>> handle;

        //既にロード開始している場合
        if (_loadedAssetMap.ContainsKey(label))
        {
            handle = _loadedAssetMap[label].Convert<IList<T>>();

            //事後処理がないならすぐ返す
            if (completed == null) return new AsyncAsset<IList<T>>(label);

            //読み込み完了時
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                Task.Run(() => completed(handle.Result));
            }
            //読み込み中
            else
            {
                handle.Completed += op =>
                {
                    if (op.Status != AsyncOperationStatus.Succeeded) return;
                    Task.Run(() => completed(handle.Result));
                };
            }
            return new AsyncAsset<IList<T>>(label);
        }

        //新規読み込み
        handle = Addressables.LoadAssetsAsync<T>(label, null);
        if (completed != null)
        {
            handle.Completed += op =>
            {
                if (op.Status != AsyncOperationStatus.Succeeded) return;
                completed(op.Result);
            };
        }
        _loadedAssetMap.Add(label, handle);
        return new AsyncAsset<IList<T>>(label);
    }

    public void Release<T>(AsyncAsset<T> asyncAssetData)
    {
        Release(asyncAssetData.Path);
    }

    public void Release(string path)
    {
        if (!_loadedAssetMap.ContainsKey(path)) return;

        Addressables.Release(_loadedAssetMap[path]);
        _loadedAssetMap.Remove(path);
    }

    private void Release()
    {
        foreach (var handle in _loadedAssetMap.Values)
        {
            Addressables.Release(handle);
        }
        _loadedAssetMap.Clear();
    }
}