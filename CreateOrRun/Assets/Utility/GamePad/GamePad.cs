using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePad
{
    private readonly int _id;

    public int Id => _id;
    public bool A => Input.GetButton("GP_A_" + _id);
    public bool ADown => Input.GetButtonDown("GP_A_" + _id);
    public bool AUp => Input.GetButtonUp("GP_A_" + _id);
    public bool B => Input.GetButton("GP_B_" + _id);
    public bool BDown => Input.GetButtonDown("GP_B_" + _id);
    public bool BUp => Input.GetButtonUp("GP_B_" + _id);
    public bool X => Input.GetButton("GP_X_" + _id);
    public bool XDown => Input.GetButtonDown("GP_X_" + _id);
    public bool XUp => Input.GetButtonUp("GP_X_" + _id);
    public bool Y => Input.GetButton("GP_Y_" + _id);
    public bool YDown => Input.GetButtonDown("GP_Y_" + _id);
    public bool YUp => Input.GetButtonUp("GP_Y_" + _id);
    public bool LB => Input.GetButton("GP_LB_" + _id);
    public bool LBDown => Input.GetButtonDown("GP_LB_" + _id);
    public bool LBUp => Input.GetButtonUp("GP_LB_" + _id);
    public bool RL => Input.GetButton("GP_RB_" + _id);
    public bool RBDown => Input.GetButtonDown("GP_RB_" + _id);
    public bool RBUp => Input.GetButtonUp("GP_RB_" + _id);
    public bool Back => Input.GetButton("GP_Back_" + _id);
    public bool BackDown => Input.GetButtonDown("GP_Back_" + _id);
    public bool BackUp => Input.GetButtonUp("GP_Back_" + _id);
    public bool Start => Input.GetButton("GP_Start_" + _id);
    public bool StartDown => Input.GetButtonDown("GP_Start_" + _id);
    public bool StartUp => Input.GetButtonUp("GP_Start_" + _id);
    public bool LStickPress => Input.GetButton("GP_LStick_Press_" + _id);
    public bool LStickPressDown => Input.GetButtonDown("GP_LStick_Press_" + _id);
    public bool LStickPressUp => Input.GetButtonUp("GP_LStick_Press_" + _id);
    public bool RStickPress => Input.GetButton("GP_RStick_Press_" + _id);
    public bool RStickPressDown => Input.GetButtonDown("GP_RStick_Press_" + _id);
    public bool RStickPressUp => Input.GetButtonUp("GP_RStick_Press_" + _id);

    public float LStickX => Input.GetAxis("GP_LStick_X_" + _id);
    public float LStickY => Input.GetAxis("GP_LStick_Y_" + _id);
    public Vector2 LStick => new Vector2(LStickX, LStickY);
    public float RStickX => Input.GetAxis("GP_RStick_X_" + _id);
    public float RStickY => Input.GetAxis("GP_RStick_Y_" + _id);
    public Vector2 RStick => new Vector2(RStickX, RStickY);
    public float DpadX => Input.GetAxis("GP_Dpad_X_" + _id);
    public float DpadY => Input.GetAxis("GP_Dpad_Y_" + _id);
    public Vector2 Dpad => new Vector2(DpadX, DpadY);
    public float LTrigger => Mathf.Max(Input.GetAxis("GP_Trigger_" + _id), 0f);
    public float RTrigger => Mathf.Max(-Input.GetAxis("GP_Trigger_" + _id), 0f);
    public float Trigger => Input.GetAxis("GP_Trigger_" + _id);

    private GamePad(int id) => _id = id;


    public static readonly GamePad num1 = new GamePad(0);
    public static readonly GamePad num2 = new GamePad(1);
    public static readonly GamePad num3 = new GamePad(2);
    public static readonly GamePad num4 = new GamePad(3);

    public static readonly GamePad[] all = { num1, num2, num3, num4 };
}
