using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    [Tooltip("遷移するシーン名")]
    [SerializeField] private string SceneName;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnClick()
    {
        FadeManager.Instance.StartFadeOut(SceneName, 0.5f);
        TestSEDirector.Instance.PlayDecisionSE();
    }
}
