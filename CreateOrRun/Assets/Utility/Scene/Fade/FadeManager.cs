using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    private static FadeManager instance;
    public static FadeManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (FadeManager)FindObjectOfType(typeof(FadeManager));
                if (instance == null)
                {
                    var fadeCanvas = AssetLoader.Instance.Instantiate("Assets/Utility/Scene/Fade/FadeCanvas.prefab");
                    instance = fadeCanvas.AddComponent<FadeManager>();
                }
            }
            return instance;
        }
    }

    private Image _panel;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        _panel = GetComponentInChildren<Image>();
    }

    public void StartFadeOut(string nextScene, float time, Sprite sprite = null)
    {
        StartCoroutine(FadeOut(nextScene, time, sprite));
    }

    private IEnumerator FadeOut(string nextScene, float time, Sprite sprite)
    {
        var color = _panel.color;
        color.a = 0;

        if (sprite != null) _panel.sprite = sprite;
        _panel.color = color;

        var timer = 0f;
        for (;;)
        {
            color.a = timer / time;
            _panel.color = color;
            
            if (timer >= time)
            {
                color.a = 1;
                _panel.color = color;
                break;
            }
            yield return null;
            timer += Time.deltaTime;
        }
        yield return SceneManager.LoadSceneAsync(nextScene);
        yield return FadeIn(time);
    }

    private IEnumerator FadeIn(float time)
    {
        var color = _panel.color;
        color.a = 1;

        _panel.color = color;

        var timer = 0f;
        for (;;)
        {
            color.a = 1 - timer / time;
            _panel.color = color;

            if (timer >= time)
            {
                color.a = 0;
                _panel.color = color;
                break;
            }
            yield return null;
            timer += Time.deltaTime;
        }
        Destroy(gameObject);
    }
}
