using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//方向列挙クラス
public class Direction
{
    // 2次元ベクトルで取得
    public readonly Vector2 Vec2;
    // 3次元座標で取得
    public readonly Vector3 Vec3;
    //符号を取得
    public readonly int Sign;
    //インデックスを取得
    public int Index => _directions.FindIndex(value => value == this);
    //反対の方向を取得
    public Direction Opposite => _directions[_opposite[Index]];

    // +Y
    public static readonly Direction Up = new Direction(Vector3.up);
    // -Y
    public static readonly Direction Down = new Direction(Vector3.down);
    // +X
    public static readonly Direction Right = new Direction(Vector3.right);
    // -X
    public static readonly Direction Left = new Direction(Vector3.left);
    // +Z
    public static readonly Direction Forward = new Direction(Vector3.forward);
    // -Z
    public static readonly Direction Back = new Direction(Vector3.back);
    // 0
    public static readonly Direction Unknown = new Direction(Vector3.zero);

    //3次元上のすべての方向
    public static IReadOnlyList<Direction> Directions => _validDirections.AsReadOnly();
    //2次元上のすべての方向
    public static IReadOnlyList<Direction> Directions2d => _validDirections2d.AsReadOnly();
    
    private Direction(Vector3 vec)
    {
        Vec2 = vec;
        Vec3 = vec;
        Sign = (int)(vec.x + vec.y + vec.z);
    }

    private static readonly List<Direction> _directions = new List<Direction> { Up, Down, Right, Left, Forward, Back, Unknown };
    private static readonly List<Direction> _validDirections = new List<Direction> { Up, Down, Right, Left, Forward, Back };
    private static readonly List<Direction> _validDirections2d = new List<Direction> { Up, Down, Right, Left };
    private static readonly int[] _opposite = {1, 0, 3, 2, 5, 4, 6};
}
