using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyMath
{
    public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
    {
        var ab = b - a;
        var av = value - a;
        return Vector3.Dot(av, ab) / Vector3.Dot(ab, ab);
    }
}
