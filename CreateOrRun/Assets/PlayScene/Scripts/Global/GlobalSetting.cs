using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSetting : SingletonMonoBehaviour<GlobalSetting>
{
    [SerializeField] private int _audioNum;
    [SerializeField] private List<AudioClip> AudioClips;

    //ゲームのFPS
    private const int FPS = 60;

    void Start()
    {
        //FPSの指定
        Application.targetFrameRate = FPS;

        //BGM
        GetComponent<AudioSource>().clip = AudioClips[_audioNum];

        //コンフィグ
        if (!GameObject.Find("ConfigManager")) return;

        if (GetComponent<AudioSource>()) GetComponent<AudioSource>().volume = ConfigManager.Instance.VolumeBGM;
        if (ConfigManager.Instance.EffectQuality != 1) ParticleManager.Instance.gameObject.SetActive(false);

    }
}
