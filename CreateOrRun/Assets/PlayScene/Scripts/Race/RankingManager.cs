using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RankingManager : SingletonMonoBehaviour<RankingManager>
{

    private class RankData
    {
        public int FinalRank { get; set; } = int.MaxValue;
        public int Id { get; set; }
        public int Lap { get; set; }
        public int CurrentCheckPoint { get; set; }
        public int PrevCheckPoint { get; set; }
        public float Ratio { get; set; }
    }

    private float _startTime;
    private readonly List<RankData> _rankDataList = new List<RankData>();

    public bool IsAllFinished => _rankDataList.All(rankData => rankData.Lap >= RaceManager.Instance.MaxLap);
    public bool IsAnyFinished => _rankDataList.Any(rankData => rankData.Lap >= RaceManager.Instance.MaxLap);
    public int GetTopPlayerId => _rankDataList.First().Id;

    private void Start()
    {
        _startTime = Time.time;

        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "PlayScene")
        {
            foreach (var rankData in _rankDataList)
            {
                rankData.Ratio = CheckPointManager.Instance.GetCheckPointsRatio(rankData.CurrentCheckPoint, rankData.Id);
            }
            RankUpdate();
        }
        else
        {
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
        }
    }

    public void SetPlayerCount(int playerCount)
    {
        for (var i = 0; i < playerCount; ++i)
        {
            _rankDataList.Add(new RankData { Id = i });
        }
    }

    public int GetRank(int playerId)
    {
        for (var i = 0; i < _rankDataList.Count; ++i)
        {
            if (_rankDataList[i].Id == playerId) return i;
        }
        return -1;
    }

    public int GetPlayerId(int rank)
    {
        if(rank >= _rankDataList.Count) return -1;
        if(rank < 0)                    return -1;

        return _rankDataList[rank].Id;
    }

    public int GetLap(int playerId)
    {
        var data = _rankDataList.Find(rankData => rankData.Id == playerId);
        if (data == null) return -1;
        return data.Lap;
    }

    public int GetCurrentCheckPoint(int playerId)
    {
        var data = _rankDataList.Find(rankData => rankData.Id == playerId);
        if (data == null) return -1;
        return data.CurrentCheckPoint;
    }

    public void NotifyPassCheckPoint(int playerId, int checkPointOrder)
    {
        var data = _rankDataList.Find(rankData => rankData.Id == playerId);
        if (data == null) return;
        
        ////前回と同じ場合は無視
        //if (data.CurrentCheckPoint == checkPointOrder) return;
        ////インデックスが小さい時は無視、ただし最後のから最初のチェックポイント通過した場合は有効
        //if (data.CurrentCheckPoint >= checkPointOrder && (data.CurrentCheckPoint != CheckPointManager.Instance.MaxOrder || checkPointOrder != 0)) return;
        if (!IsNextCheckPoint(data.CurrentCheckPoint, checkPointOrder)) return;

        //チェックポイントと時間記録
        data.PrevCheckPoint = data.CurrentCheckPoint;
        data.CurrentCheckPoint = checkPointOrder;
        data.Ratio = Time.time - _startTime;

        if (checkPointOrder != 0) return;

        //ラップ更新タイミング
        ++data.Lap;
        if (data == _rankDataList.First()) SwitchManager.Instance.AllSwitchReset();
        if (data.Lap == RaceManager.Instance.MaxLap) 
            data.FinalRank = _rankDataList.IndexOf(data);
        else
            SEManager.Instance.PlaySE("Lap", 3);

    }

    private void RankUpdate()
    {
        //todo ソート作成
        _rankDataList.Sort(DataSorter);
    }

    private static bool IsNextCheckPoint(int current, int next)
    {
        var max = CheckPointManager.Instance.MaxOrder;
        if (current == CheckPointManager.Instance.MaxOrder && next == 0) return true;
        return current + 1 == next;
    }

    private static int DataSorter(RankData a, RankData b)
    {
        //順位
        var finalRank = a.FinalRank - b.FinalRank;
        if (finalRank != 0) return finalRank;

        //ラップ順
        var lap = b.Lap - a.Lap;
        if (lap != 0) return lap;

        //チェックポイント順
        var checkPoint = b.CurrentCheckPoint - a.CurrentCheckPoint;
        if (checkPoint != 0) return checkPoint;

        //割合
        return b.Ratio.CompareTo(a.Ratio);
    }
}
