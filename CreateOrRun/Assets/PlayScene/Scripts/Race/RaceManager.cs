using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RaceManager : SingletonMonoBehaviour<RaceManager>
{
    [SerializeField] public int MaxLap = 3;
    //カウント開始の数字
    [SerializeField] public int StartCount = 3;

    [SerializeField] public bool ShouldCountDown;

    [SerializeField] private Sprite FinishImage;
    [SerializeField] private float imageScale;
    
    private void Start()
    {
        if (ShouldCountDown)
        {
            foreach (var player in PlayerManager.Instance.PlayerControllers)
            {
                player.FreezeOrder(StartCount);
            }
        }
    }

    private void Update()
    {
        if (RankingManager.Instance.IsAnyFinished) StartCoroutine(nameof(Finishing));
    }

    private IEnumerator Finishing()
    {
        enabled = false;

        var image = UIManager.Instance.AddImageWhole();
        image.sprite = FinishImage;
        image.rectTransform.localScale *= imageScale;

        //効果音
        SEManager.Instance.PlaySE("finish", 3);

        yield return new WaitForSeconds(1.5f);

        //リザルトシーンへ
        //SceneManager.LoadScene("ResultScene");
        FadeManager.Instance.StartFadeOut("ResultScene", 0.5f);
    }
}
