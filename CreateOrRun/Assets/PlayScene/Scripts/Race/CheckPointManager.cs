using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class CheckPointManager : SingletonMonoBehaviour<CheckPointManager>
{
    private readonly SortedDictionary<int, List<CheckPoint>> _checkPoints = new SortedDictionary<int, List<CheckPoint>>();

    public IReadOnlyDictionary<int, ReadOnlyCollection<CheckPoint>> CheckPointsMap
        => new ReadOnlyDictionary<int, ReadOnlyCollection<CheckPoint>>(_checkPoints.ToDictionary(k => k.Key, v => v.Value.AsReadOnly()));

    public int MaxOrder { get; private set; }

    public Vector3 FirstCheckPointPos
    {
        get
        {
            var first = _checkPoints.First().Value;
            var minX = first.Min(a => a.transform.position.x);
            var mostLeft = first.FindAll(a => Math.Abs(a.transform.position.x - minX) < float.Epsilon);
            var minY = mostLeft.Min(a => a.transform.position.y);
            return first.Find(a => Math.Abs(a.transform.position.y - minY) < float.Epsilon).transform.position + new Vector3(0, 0.5f);
        }
    }

    //チェックポイントを登録する
    public void Register(CheckPoint checkPoint)
    {
        //登録するチェックポイントのオーダーの要素がないなら生成
        if (!_checkPoints.ContainsKey(checkPoint.Order))
        {
            _checkPoints.Add(checkPoint.Order, new List<CheckPoint>());
        }
        //登録
        _checkPoints[checkPoint.Order].Add(checkPoint);
        MaxOrder = _checkPoints.Count - 1;
    }
    
    //チェックポイント間の何割のとこにいるか
    public float GetCheckPointsRatio(int startCheckPointId, int playerId)
    {
        if (!_checkPoints.ContainsKey(startCheckPointId)) return 0;

        //プレイヤの座標
        var playerPos = PlayerManager.Instance.Players[playerId].transform.position;

        //それぞれのインデックスのプレイヤに最も近いチェックポイントの座標
        var startCheckPointPos = _checkPoints[startCheckPointId].OrderBy(a => (a.transform.position - playerPos).magnitude).First().transform.position;
        var endCheckPointPos = _checkPoints[(startCheckPointId + 1) % (MaxOrder + 1)].OrderBy(a => (a.transform.position - playerPos).magnitude).First().transform.position;

        //逆線形補間で割合を取得
        return Mathf.Clamp(MyMath.InverseLerp(startCheckPointPos, endCheckPointPos, playerPos), 0, 1);
    }
}
