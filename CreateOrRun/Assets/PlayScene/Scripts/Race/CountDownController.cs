using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownController : MonoBehaviour
{
    [Serializable]
    class SpriteSetting
    {
        public Sprite sourceImage;
        public float scale;
    }

    [SerializeField] private List<SpriteSetting> SpriteSettings;

    private void Start()
    {
        if (RaceManager.Instance.ShouldCountDown)
        {
            StartCoroutine(nameof(CountDown));
        }
        else
        {
            GlobalSetting.Instance.GetComponent<AudioSource>().enabled = true;
        }
    }

    private IEnumerator CountDown()
    {
        for (var i = 0; i < SpriteSettings.Count; ++i)
        {
            var image = GenerateImage(i);

            if (i < SpriteSettings.Count - 1)
            {
                StartCoroutine(nameof(RotateImage), image);
                SEManager.Instance.PlaySE("Count");
            }
            else
            {
                StartCoroutine(nameof(ImageScale), image);
                SEManager.Instance.PlaySE("Start", 0.5f);
                GlobalSetting.Instance.GetComponent<AudioSource>().enabled = true;
            }
            yield return new WaitForSeconds(1f);

            Destroy(image.gameObject);
        }
        Destroy(gameObject);
    }

    private IEnumerator RotateImage(Image image)
    {
        const float limit = 0.7f;

        var timer = 0f;
        var scale = image.rectTransform.localScale;

        for (;;)
        {
            image.rectTransform.localScale = Vector3.Lerp(Vector3.zero, scale, timer / limit);
            image.rectTransform.rotation = Quaternion.Euler(0, 0, 360 * timer / limit);

            if (timer >= limit)
            {
                image.rectTransform.localScale = Vector3.Lerp(Vector3.zero, scale, 1);
                image.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }

    private IEnumerator ImageScale(Image image)
    {
        const float limit = 0.3f;

        var timer = 0f;
        var scale = image.rectTransform.localScale;

        for (; ; )
        {
            image.rectTransform.localScale = Vector3.Lerp(Vector3.zero, scale, timer / limit);

            if (timer >= limit)
            {
                image.rectTransform.localScale = Vector3.Lerp(Vector3.zero, scale, 1);
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }

    private Image GenerateImage(int index)
    {
        var image = UIManager.Instance.AddImageWhole();
        image.sprite = SpriteSettings[index].sourceImage;
        image.rectTransform.localScale *= SpriteSettings[index].scale;

        return image;
    }
}
