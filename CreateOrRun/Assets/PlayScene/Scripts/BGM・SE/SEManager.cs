using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class SEManager : SingletonMonoBehaviour<SEManager>
{
    //SEで使用する
    [Serializable]
    struct SEAudioClip
    {
        public string name;
        public AudioClip audioClip;
    }
    [SerializeField] private List<SEAudioClip> SEAudioClips;

    private AudioSource _audioSource;
    private Dictionary<string, AudioClip> _audioClipMap;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        if (GameObject.Find("ConfigManager"))
            _audioSource.volume = ConfigManager.Instance.VolumeSE;
        else
            _audioSource.volume = 0.5f;

        _audioClipMap = SEAudioClips.ToDictionary(item => item.name, item => item.audioClip);
    }

    void Update()
    {
        
    }

    public void PlaySE(string name)
    {
        if (_audioClipMap == null) return; 
        if (!_audioClipMap.ContainsKey(name)) return;
        
        _audioSource.PlayOneShot(_audioClipMap[name]);
    }

    public void PlaySE(string name, float volume)
    {
        if (_audioClipMap == null) return; 
        if (!_audioClipMap.ContainsKey(name)) return;
        
        _audioSource.PlayOneShot(_audioClipMap[name], volume);        
    }

    public void PlayLoopSE(string name, float volume)
    {
        if (_audioClipMap == null) return;
        if (!_audioClipMap.ContainsKey(name)) return;

        _audioSource.clip = _audioClipMap[name];
        _audioSource.volume = volume;
        _audioSource.loop = true;
        _audioSource.Play();
    }

    public void SetLoopSE(bool isLoop)
    {
        _audioSource.loop = isLoop;
    }
}
