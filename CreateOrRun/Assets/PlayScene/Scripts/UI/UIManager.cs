using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : SingletonMonoBehaviour<UIManager>
{
    [Tooltip("表示テキスト系")]
    [SerializeField] private Text[] TextPrefab;
    [Tooltip("表示画像系")]
    [SerializeField] private Image[] ImagePrefab;
    [Tooltip("画面真ん中に出すテキストプレハブ")]
    [SerializeField] private GameObject EmptyTextPrefab;
    [Tooltip("画面真ん中に出す画像プレハブ")]
    [SerializeField] private GameObject EmptyImagePrefab;
    [Tooltip("プレイヤーIDプレハブ")]
    [SerializeField] private Text PlayerIdPrefab;
    [Tooltip("ミニマップの画像")]
    [SerializeField] private Sprite[] MiniMapSprite;
    [Tooltip("ミニマップのプレハブ")]
    [SerializeField] private Image MiniMapPrefab;
    [Tooltip("アイテム画像オブジェクトのプレハブ")]
    [SerializeField] private Image ItemImagePrefab;
    [Tooltip("順位画像オブジェクトのプレハブ")]
    [SerializeField] private Image RankingImagePrefab;
    [Tooltip("ラップ数画像のプレハブ")]
    [SerializeField] private GameObject LapCountPrefab;
    [Tooltip("キャンバス(プレハブ)")]
    [SerializeField] private Canvas CanvasPrefab;

    //カメラの数
    private int _camNum;

    //キャンバスのトランスフォーム
    RectTransform _canvasRectTransform;

    //キャンバスのリスト(プレイヤーIDぶん)
    private List<Canvas> _canvasList;
    public IReadOnlyList<Canvas> CanvasList => _canvasList.AsReadOnly();

    //画面全体のキャンバス
    private Canvas _wholeCanvas;
    public Canvas WholeCanvas
    {
        get
        {
            //子オブジェクトの画面全体のキャンバスを取得
            if (_wholeCanvas == null)
                _wholeCanvas = GameObject.Find("WholeCanvas").GetComponent<Canvas>();
            return _wholeCanvas;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //キャンバス
        _canvasList = new List<Canvas>();

        //プレイシーン内のカメラの数
        _camNum = CameraManager.Instance.CameraNum;

        //↑のカメラ数分だけUI準備
        for (int i = 0; i < _camNum; i++)
        {
            //キャンバス設定もろもろ
            CanvasPrefab.renderMode = RenderMode.ScreenSpaceCamera;
            //最前面設定
            CanvasPrefab.planeDistance = 1;
            CanvasPrefab.worldCamera = CameraManager.Instance.Cameras[i];

            //キャンバス生成
            Canvas canvas = Instantiate(CanvasPrefab, Vector3.zero, Quaternion.identity, transform);
            canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

            //UI生成
            DrawText(canvas);
            DrawImage(canvas);
            DrawMiniMapImage(canvas, i);
            InstantiateItemImage(canvas);
            InstantiateRankingImage(canvas, i);
            InstantiateLap(canvas, i);

            //リストに追加
            _canvasList.Add(canvas);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    //テキスト描画
    private void DrawText(Canvas canvas)
    {
        for (int i = 0; i < TextPrefab.Length; i++)
        {
            Text textUi = Instantiate(TextPrefab[i]);
            textUi.transform.SetParent(canvas.transform, false);

            //大きさや角度を変えるならここに
        }
    }

    //画像描画
    private void DrawImage(Canvas canvas)
    {
        for (int i = 0; i < ImagePrefab.Length; i++)
        {
            Image imageUi = Instantiate(ImagePrefab[i]);
            imageUi.transform.SetParent(canvas.transform, false);

            //大きさや角度を変えるならここに
        }
    }

    //〇Pの表示
    private void DrawPlayerId(Canvas canvas, int id)
    {
        //プレイヤーID準備
        ++id;
        Text playerId = Instantiate(PlayerIdPrefab);
        playerId.transform.SetParent(canvas.transform, false);
        playerId.text = id + "P";
    }

    //ミニマップの描画
    private void DrawMiniMapImage(Canvas canvas, int id)
    {
        //GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
        //quad.transform.SetParent(canvas.transform);
        //quad.transform.localScale *= 2.0f;
        //quad.AddComponent<RectTransform>().anchoredPosition = new Vector2(0.5f, 0.5f);
        //quad.transform.localPosition = Vector3.zero;
        //quad.GetComponent<Renderer>().material.mainTexture = MiniMapSprite[id];
        //quad.GetComponent<MeshCollider>().enabled = false;

        //Image image = Instantiate(EmptyImagePrefab, canvas.transform).GetComponent<Image>();
        Image image = Instantiate(MiniMapPrefab, Vector3.zero, Quaternion.identity, transform);
        image.rectTransform.pivot = new Vector2(0, 1);
        image.rectTransform.anchoredPosition = new Vector2(0.5f, 0.5f);
        image.rectTransform.localScale *= 3;
        image.transform.SetParent(canvas.transform, false);
        image.sprite = MiniMapSprite[id];

    }

    //アイテム表示用のオブジェクトを初期化
    private void InstantiateItemImage(Canvas canvas)
    {
        Image image = Instantiate(ItemImagePrefab, Vector3.zero, Quaternion.identity, transform);
        image.rectTransform.pivot = new Vector2(1, 1);
        image.rectTransform.localScale *= 3;
        image.transform.SetParent(canvas.transform, false);
    }

    private void InstantiateRankingImage(Canvas canvas, int id)
    {
        Image image = Instantiate(RankingImagePrefab, Vector3.zero, Quaternion.identity, transform);
        //image.rectTransform.pivot = new Vector2(0.5f, 0.5f);        
        image.rectTransform.localScale *= 3;
        image.transform.SetParent(canvas.transform, false);

        image.GetComponentInChildren<RankingUI>().PlayerIdEntry = id;
    }

    private void InstantiateLap(Canvas canvas, int id)
    {
        var lap = Instantiate(LapCountPrefab, transform);
        lap.GetComponent<LapCountUI>().PlayerIdEntry = id;
    }

    public Text AddTextWhole()
    {
        return Instantiate(EmptyTextPrefab, WholeCanvas.transform).GetComponent<Text>();
    }
    public Text AddTextPlayerCanvas(int i)
    {
        if (i >= _canvasList.Count) return null;
        return Instantiate(EmptyTextPrefab, _canvasList[i].transform).GetComponent<Text>();
    }

    public Image AddImageWhole()
    {
        return Instantiate(EmptyImagePrefab, WholeCanvas.transform).GetComponent<Image>();
    }
    public Image AddImagePlayerCanvas(int i)
    {
        if (i >= _canvasList.Count) return null;
        return Instantiate(EmptyImagePrefab, _canvasList[i].transform).GetComponent<Image>();
    }
}
