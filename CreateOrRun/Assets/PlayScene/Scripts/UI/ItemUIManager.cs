using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ItemUIManager : SingletonMonoBehaviour<ItemUIManager>
{
    [Serializable]
    public class UIData
    {
        public string _name;
        public Sprite _sprite;
    }
    [Tooltip("アイテムを表現するUI画像(x > 0)")]
    [SerializeField] private List<UIData> UIDataList;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void ChangeItemUI(PlayerController player)
    {
        string  holdItemName    = player.Handler.Name;
        foreach (UIData uiData in UIDataList)
        {
            if (uiData._name != holdItemName) continue;

            Canvas canvas = UIManager.Instance.CanvasList[player.PlayerId];
            Transform itemImageTransform = canvas.transform.Find("ItemImage(Clone)");
            itemImageTransform.GetComponent<ItemUI>().UISprite = uiData._sprite;
            return;
        }
    }
}
