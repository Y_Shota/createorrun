using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    
    public Sprite UISprite
    {
        get => transform.GetChild(0).GetComponent<Image>().sprite;
        set => transform.GetChild(0).GetComponent<Image>().sprite = value;
    }

    void Start()
    {
        Image UIImage = transform.GetChild(0).GetComponent<Image>();
        UIImage.rectTransform.localScale = new Vector3(0.5f, 0.5f);
    }

    void Update()
    {
        
    }
}
