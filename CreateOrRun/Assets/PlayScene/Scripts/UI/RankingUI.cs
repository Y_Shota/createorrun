using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingUI : MonoBehaviour
{
    [Tooltip("�\���摜�n")]
    [SerializeField] private List<Sprite> ImagePrefabList;

    public int PlayerIdEntry { get; set; }

    void Start()
    {        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Image>().sprite = ImagePrefabList[RankingManager.Instance.GetRank(PlayerIdEntry)];
    }
}
