using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenLineRenderer : MonoBehaviour
{
    //画面の四隅を取得してそこに境界線描画
    //カメラの分割数に対応するため分割画面分行う

    //短径トランスフォーム
    [SerializeField] private RectTransform _rectTrans;
    private Rect _screeenRect;

    //頂点
    private Vector3[] _vertices { get; set; }

    //カメラの個数
    private int _cameraNum;


    private void Start()
    {
        _cameraNum = CameraManager.Instance.CameraNum;
    }

    private void Update()
    {
        //カメラの個数分行う
        for (int i = 0; i < _cameraNum; i++)
        {
            _screeenRect = GetScreenRect(_rectTrans, i);
        }
    }

    //キャンバスの四隅をスクリーン座標で取得
    private static Rect GetScreenRect(RectTransform rectTrans, int cameraId)
    {
        //キャンバスにやるver
        var canvas = UIManager.Instance.CanvasList[cameraId];
        var camera = canvas.worldCamera;
        var corners = new Vector3[4];
        //画面四隅をスクリーン座標で取得
        rectTrans.GetWorldCorners(corners);
        var screenCorner1 = RectTransformUtility.WorldToScreenPoint(camera, corners[1]);
        var screenCorner3 = RectTransformUtility.WorldToScreenPoint(camera, corners[3]);
        //左下基準で
        var screenRect = new Rect();
        screenRect.x = screenCorner1.x;
        screenRect.width = screenCorner3.x - screenRect.x;
        screenRect.y = screenCorner3.y;
        screenRect.height = screenCorner1.y - screenRect.y;

        return screenRect;
    }
}

/*
 * https://gametukurikata.com/camera/splitcamera
 */