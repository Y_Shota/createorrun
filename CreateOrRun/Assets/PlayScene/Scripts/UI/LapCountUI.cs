using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapCountUI : MonoBehaviour
{
    [SerializeField] private Sprite Image;

    public int PlayerIdEntry { get; set; }

    private int _prevLap;
    private Text _text;

    private void Start()
    {
        _text = UIManager.Instance.AddTextPlayerCanvas(PlayerIdEntry);
        _text.text = "1";
        _text.name = "LapCountText" + PlayerIdEntry;
        _text.rectTransform.sizeDelta = new Vector2(500, 500);
        _text.rectTransform.anchorMin = new Vector2(0.5f, 1);
        _text.rectTransform.anchorMax = new Vector2(0.5f, 1);
        _text.rectTransform.pivot = new Vector2(0.5f, 1);
        _text.rectTransform.anchoredPosition = new Vector2(0.5f, 0.5f);
        _text.rectTransform.anchoredPosition = new Vector3(-140, 140);
        _text.fontSize = 150;
        _text.alignment = TextAnchor.MiddleCenter;
        _text.color = Color.red;
        _text.fontStyle = FontStyle.BoldAndItalic;

        var image = UIManager.Instance.AddImagePlayerCanvas(PlayerIdEntry);
        image.name = "LapCountImage" + PlayerIdEntry;
        image.sprite = Image;
        image.rectTransform.sizeDelta = Image.rect.size;
        image.rectTransform.localScale *= 3;
        image.rectTransform.anchorMin = new Vector2(0.5f, 1);
        image.rectTransform.anchorMax = new Vector2(0.5f, 1);
        image.rectTransform.pivot = new Vector2(0.5f, 1);
        image.rectTransform.anchoredPosition = new Vector2(0.5f, 0.5f);
    }

    private void Update()
    {
        var lap = RankingManager.Instance.GetLap(PlayerIdEntry);

        if (_prevLap == lap || lap >= RaceManager.Instance.MaxLap) return;

        _text.text = (lap + 1).ToString();
        StartCoroutine(nameof(RotateCor));
        _prevLap = lap;

        //GetComponent<Image>().sprite = ImagePrefabList[RankingManager.Instance.GetLap(PlayerIdEntry)];
    }

    private IEnumerator RotateCor()
    {
        const float limit = 0.7f;

        var timer = 0f;
        var scale = _text.rectTransform.localScale;

        for (; ; )
        {
            _text.rectTransform.localScale = Vector3.Lerp(Vector3.zero, scale, timer / limit);
            _text.rectTransform.rotation = Quaternion.Euler(0, 0, 360 * timer / limit);

            if (timer >= limit)
            {
                _text.rectTransform.localScale = Vector3.Lerp(Vector3.zero, scale, 1);
                _text.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }
}
