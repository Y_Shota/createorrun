using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Rendering;

public class StageManager : SingletonMonoBehaviour<StageManager>
{
    private static readonly string DirectoryPath = Application.streamingAssetsPath + "/Stages/";

    [SerializeField] private int StageId;
    [SerializeField] private Texture2D BackGroundTexture;
    [SerializeField] private float FarBackGround;
    [SerializeField] private List<string> FileNames;

    [Serializable]
    public class BlockData
    {
        public string _id;
        public GameObject _gameObject;
    }
    [SerializeField] private List<BlockData> BlockList;

    [Serializable]
    public class CheckPointData
    {
        public string _id;
        public int _order;
    }
    [SerializeField] private List<CheckPointData> CheckPointOrder;
    
    [Serializable]
    public class SwitchPair
    {
        public string _switchId;
        public string _blockId;
    }
    [SerializeField] private List<SwitchPair> SwitchPairList;

    [SerializeField] private List<string> CombinableIds;
    [SerializeField] private PhysicMaterial CombineMeshPhysicMaterial;


    private Dictionary<string, GameObject> _blockMap;
    private Texture2D _miniMapTexture;
    private readonly Dictionary<string, List<Matrix4x4>> _combineMap = new Dictionary<string, List<Matrix4x4>>();
    private readonly Dictionary<string, GameObject> _layer = new Dictionary<string, GameObject>();
    private readonly Dictionary<string, List<Material>> _customMaterialsMap = new Dictionary<string, List<Material>>();

    public Vector2Int Size { get; private set; }
    public Sprite MiniMapSprite { get; private set; }

    private void Start()
    {
        _blockMap = BlockList.ToDictionary(block => block._id, block => block._gameObject);

        var selectedDataStorageObject = GameObject.Find("SelectedDataStorage");
        StageId = selectedDataStorageObject
            ? selectedDataStorageObject.GetComponent<SelectedDataStorage>().StageId
            : StageId;

        Generate(StageId);
    }

    private void Generate(int stageId)
    {
        if (CsvReader.IsCsv(FileNames[stageId]))
        {
            LoadStageFromCsv(stageId);
        }
        else if (ExcelReader.IsExcel(FileNames[stageId]))
        {
            LoadStageFromExcel(stageId);
        }
        else if (Path.GetExtension(FileNames[stageId]) == ".json")
        {
            LoadStageFromJson(stageId);
        }
        CombineMesh();
        CameraManager.Instance.SetPositionWholeCamera(Size);
    }

    private void LoadStageFromCsv(int stageId)
    {

        //csv読み込み
        var csv = CsvReader.Read(DirectoryPath + FileNames[stageId]);
        if (csv == null) return;

        Size = csv.Size;

        //ミニマップ用
        _miniMapTexture = new Texture2D(csv.Width, csv.Height);

        for (var x = 0; x < csv.Width; ++x)
        {
            for (var y = 0; y < csv.Height; ++y)
            {
                var blockId = csv[x, y];

                if (!_blockMap.ContainsKey(blockId)) continue;

                LoadSingle(x, y, blockId, _blockMap[blockId]);
            }
        }
        PlayerManager.Instance.SetStartPosition(CheckPointManager.Instance.FirstCheckPointPos);

        _miniMapTexture.filterMode = FilterMode.Point;
        _miniMapTexture.Apply();

        MiniMapSprite = Sprite.Create(_miniMapTexture, new Rect(0, 0, _miniMapTexture.width, _miniMapTexture.height), new Vector2(0.5f, 0.5f), 1);

        FillOuterZone("1", _blockMap["1"]);

        //背景
        BackGroundManager.Instance.InstantiateBackGround(csv.Width, csv.Height, FarBackGround, BackGroundTexture);

        PlayerManager.Instance.ActivateMiniMap();
    }

    private void LoadStageFromExcel(int stageId)
    {
        var excel = ExcelReader.Read(DirectoryPath + FileNames[stageId]);
        if (excel == null) return;

        int fieldWidth = 0, fieldHeight = 0;
        var fieldSheetNames = excel.SheetNames.ToList().FindAll(str => str.Contains("field"));

        foreach (var fieldSheet in excel.Sheets.ToList().FindAll(sheet => fieldSheetNames.Contains(sheet.Name)))
        {
            if (_miniMapTexture == null) _miniMapTexture = new Texture2D(fieldSheet.Width, fieldSheet.Height);
            if (_miniMapTexture.width < fieldSheet.Width) _miniMapTexture.width = fieldSheet.Width;
            if (_miniMapTexture.height < fieldSheet.Height) _miniMapTexture.height = fieldSheet.Height;
            fieldWidth = fieldSheet.Width;
            fieldHeight = fieldSheet.Height;

            for (var x = 0; x < fieldSheet.Width; ++x)
            {
                for (var y = 0; y < fieldSheet.Height; ++y)
                {
                    var blockId = fieldSheet[x, y];

                    if (!_blockMap.ContainsKey(blockId)) continue;

                    LoadSingle(x, y, blockId, _blockMap[blockId]);
                }
            }
        }
        PlayerManager.Instance.SetStartPosition(CheckPointManager.Instance.FirstCheckPointPos);

        Size = new Vector2Int(_miniMapTexture.width, _miniMapTexture.height);

        _miniMapTexture.filterMode = FilterMode.Point;
        _miniMapTexture.Apply();

        MiniMapSprite = Sprite.Create(_miniMapTexture, new Rect(0, 0, _miniMapTexture.width, _miniMapTexture.height), new Vector2(0.5f, 0.5f), 1);

        FillOuterZone("1", _blockMap["1"]);

        //背景        
        BackGroundManager.Instance.InstantiateBackGround(fieldWidth, fieldHeight, FarBackGround, BackGroundTexture);

        PlayerManager.Instance.ActivateMiniMap();
    }

    private void LoadStageFromJson(int stageId)
    {
        //Json読み込み、パース
        var jsonStreamReader = new StreamReader(DirectoryPath + FileNames[stageId], System.Text.Encoding.GetEncoding("shift_jis"));
        var fieldData = JsonUtility.FromJson<FieldData>(jsonStreamReader.ReadToEnd());
        if (fieldData == null) return;

        //Json内にあったファイル名のファイルを開く
        var excel = ExcelReader.Read(DirectoryPath + fieldData.FileName);
        if (excel == null) return;

        _blockMap.Clear();
        //プレハブロード
        foreach (var blockData in fieldData.BlockDataList)
        {
            _blockMap.Add(blockData.Id, AssetLoader.Instance.LoadAsset<GameObject>("Assets/PlayScene/Prefab/Stage/" + blockData.PrefabPath));
        }

        //シート選択
        var sheets = fieldData.SheetNames.Count == 0
            ? excel.Sheets
            : fieldData.SheetNames.Select(sheetName => excel[sheetName]).Where(sheet => sheet != null);

        var pixelList = new List<Vector2Int>(); 

        //シート読み込み
        foreach (var sheet in sheets)
        {
            if (_miniMapTexture == null) _miniMapTexture = new Texture2D(sheet.Width * 4, sheet.Height * 4);
            if (_miniMapTexture.width < sheet.Width * 4) _miniMapTexture.width = sheet.Width * 4;
            if (_miniMapTexture.height < sheet.Height * 4) _miniMapTexture.height = sheet.Height * 4;

            for (var x = 0; x < sheet.Width; ++x)
            {
                for (var y = 0; y < sheet.Height; ++y)
                {
                    //ID取得
                    var blockId = sheet[x, y];

                    if (!_blockMap.ContainsKey(blockId))
                    {
                        if (!pixelList.Contains(new Vector2Int(x, y)))
                        {
                            for (var px = 0; px < 4; ++px)
                            {
                                for (var py = 0; py < 4; ++py)
                                {
                                    _miniMapTexture.SetPixel(x * 4 + px, y * 4 + py, new Color(1, 1, 1, 0.7f));
                                }
                            }
                            pixelList.Add(new Vector2Int(x, y));
                        }
                        continue;
                    }

                    //生成ブロック情報取得
                    var genBlock = _blockMap[blockId];
                    var blockData = fieldData.BlockDataList.Find(blockData => blockData.Id == blockId);
                    if (genBlock == null || blockData == null) continue;

                    //ピクセル打ち込み
                    for (var px = 0; px < 4; ++px)
                    {
                        for (var py = 0; py < 4; ++py)
                        {
                            _miniMapTexture.SetPixel(x * 4 + px, y * 4 + py, blockData.MapColor);
                        }
                    }
                    pixelList.Add(new Vector2Int(x, y));

                    var localPosition = blockData.Offset + new Vector3(x, y); ;
                    var localRotation = Quaternion.Euler(0, 0, blockData.Angle);
                    var localScale = new Vector3(blockData.Scale.x, blockData.Scale.y, 1);

                    //メッシュ統合が許可されている場合
                    if (blockData.Combinable)
                    {
                        if (!_combineMap.ContainsKey(blockId)) _combineMap.Add(blockId, new List<Matrix4x4>());

                        var localMat = Matrix4x4.TRS(localPosition, localRotation, localScale);
                        var mat = transform.localToWorldMatrix * genBlock.transform.localToWorldMatrix * localMat;
                        _combineMap[blockId].Add(mat);

                        if (_customMaterialsMap.ContainsKey(blockId)) continue;

                        var ren = genBlock.GetComponentInChildren<Renderer>();
                        if (ren == null || blockData.Materials.Count <= 0) continue;

                        var materials = new List<Material>();
                        var count = ren.sharedMaterials.Length < blockData.Materials.Count
                            ? ren.materials.Length
                            : blockData.Materials.Count;
                        var i = 0;
                        for (i = 0; i < count; ++i)
                        {
                            materials.Add(AssetLoader.Instance.LoadAsset<Material>("Assets/PlayScene/Material/" + blockData.Materials[i]));
                        }
                        if (i < ren.sharedMaterials.Length)
                        {
                            for (; i < ren.sharedMaterials.Length; ++i)
                            {
                                materials.Add(ren.sharedMaterials[i]);
                            }
                        }

                        _customMaterialsMap.Add(blockId, materials);

                        continue;
                    }

                    //同じブロックをまとめる
                    if (!_layer.ContainsKey(blockId))
                    {
                        var layer = new GameObject(blockId + "Layer")
                        {
                            transform =
                            {
                                parent = transform
                            },
                            tag = genBlock.tag,
                            layer = genBlock.layer,
                        };
                        _layer.Add(blockId, layer);
                    }
                    

                    var block = Instantiate(genBlock, _layer[blockId].transform);
                    block.transform.localPosition += localPosition;
                    block.transform.localRotation *= localRotation;
                    block.transform.localScale = Vector3.Scale(genBlock.transform.localScale, localScale);

                    var renderer = block.GetComponentInChildren<Renderer>();
                    if (renderer != null)
                    {
                        var materials = new List<Material>();
                        var count = renderer.materials.Length < blockData.Materials.Count
                            ? renderer.materials.Length
                            : blockData.Materials.Count;
                        var i = 0;
                        for (i = 0; i < count; ++i)
                        {
                            materials.Add(AssetLoader.Instance.LoadAsset<Material>("Assets/PlayScene/Material/" + blockData.Materials[i]));
                        }
                        if (i < renderer.sharedMaterials.Length)
                        {
                            for (; i < renderer.sharedMaterials.Length; ++i)
                            {
                                materials.Add(renderer.sharedMaterials[i]);
                            }
                        }
                        renderer.materials = materials.ToArray();
                    }

                    if (blockData.Method == "") continue;

                    var method = GetType().GetMethod(blockData.Method, BindingFlags.NonPublic | BindingFlags.Instance);
                    if (method != null)
                    {
                        method.Invoke(this, new object[] { block, blockData });
                    }
                }
            }
        }
        PlayerManager.Instance.SetStartPosition(CheckPointManager.Instance.FirstCheckPointPos);

        Size = new Vector2Int(_miniMapTexture.width / 4, _miniMapTexture.height / 4);

        _miniMapTexture.filterMode = FilterMode.Point;
        _miniMapTexture.Apply();

        MiniMapSprite = Sprite.Create(_miniMapTexture, new Rect(0, 0, _miniMapTexture.width, _miniMapTexture.height), new Vector2(0.5f, 0.5f), 1);

        var normal = _blockMap.FirstOrDefault(item => item.Value.name == "Normal");
        FillOuterZone(normal.Key, normal.Value);

        //背景        
        BackGroundManager.Instance.InstantiateBackGround(Size.x, Size.y, FarBackGround, BackGroundTexture);

        PlayerManager.Instance.ActivateMiniMap();
    }

    private void InitCheckPoint(GameObject block, FieldData.BlockData blockData)
    {
        var checkPoint = block.GetComponent<CheckPoint>();
        if (checkPoint == null) return;

        checkPoint.Order = (int)blockData.NumberParam[0];
    }

    private void InitSwitchLever(GameObject block, FieldData.BlockData blockData)
    {
        var switchLever = block.GetComponent<SwitchLever>();
        if (switchLever == null) return;

        SwitchManager.Instance.RegisterSwitchLever(blockData.StrParam[0], switchLever);
    }

    private void InitRotateBlock(GameObject block, FieldData.BlockData blockData)
    {
        var rotateBlock = block.GetComponent<RotateBlock>();
        if (rotateBlock == null) return;

        SwitchManager.Instance.RegisterRotateBlock(blockData.StrParam[0], rotateBlock);

        rotateBlock.DefaultAngle = blockData.NumberParam[0];
        rotateBlock.RotateAngle = blockData.NumberParam[1];
        rotateBlock.Length = blockData.NumberParam[2];
        rotateBlock.RotateTime = blockData.NumberParam[3];
    }

    private void InitWheel(GameObject block, FieldData.BlockData blockData)
    {
        var wheel = block.GetComponent<WheelController>();
        if (wheel == null) return;

        wheel.Amount = (int)Mathf.Clamp(blockData.NumberParam[0], 1, 8);
        wheel.Radius = blockData.NumberParam[1];
        wheel.Speed = blockData.NumberParam[2];
        wheel.Scale = new Vector2(blockData.NumberParam[3], blockData.NumberParam[4]);
        wheel.IsAnticlockwise = blockData.NumberParam[5] > 0;
    }

    private void InitMovingBlock(GameObject block, FieldData.BlockData blockData)
    {
        var movingBlock = block.GetComponent<MovingBlock>();
        if (movingBlock == null) return;

        movingBlock.Distance = blockData.NumberParam[0];
        movingBlock.Angle = blockData.NumberParam[1];
        movingBlock.Speed = blockData.NumberParam[2];
    }

    private void InitVectorZone(GameObject block, FieldData.BlockData blockData)
    {
        var vectorZone = block.GetComponent<VectorZone>();
        if (vectorZone == null) return;

        vectorZone.Force = blockData.NumberParam[0];
        vectorZone.Direction = new Vector2(blockData.NumberParam[1], blockData.NumberParam[2]);
    }

    private void InitVectorBlock(GameObject block, FieldData.BlockData blockData)
    {
        var vectorBlock = block.GetComponent<VectorBlock>();
        if (vectorBlock == null) return;

        vectorBlock.Force = blockData.NumberParam[0];
        vectorBlock.Direction = new Vector2(blockData.NumberParam[1], blockData.NumberParam[2]);
    }

    private void LoadSingle(int x, int y, string blockId, GameObject genBlock)
    {
        if (genBlock == null) return;

        //フィールドブロックならピクセル打ち込み
        _miniMapTexture.SetPixel(x, y, genBlock.CompareTag("Field") ? new Color(0.278f, 0.752f, 0.952f, 1) : new Color(1, 1, 1, 0.5f));

        //メッシュ統合が許可されている場合
        if (CombinableIds.Contains(blockId))
        {
            if (!_combineMap.ContainsKey(blockId)) _combineMap.Add(blockId, new List<Matrix4x4>());

            var mat = transform.localToWorldMatrix * genBlock.transform.localToWorldMatrix * Matrix4x4.Translate(new Vector3(x, y));
            _combineMap[blockId].Add(mat);

            return;
        }

        //同じブロックをまとめる
        if (!_layer.ContainsKey(blockId))
        {
            var layer = new GameObject(genBlock.name + "Layer")
            {
                transform =
                {
                    parent = transform
                },
                tag = genBlock.tag,
                layer = genBlock.layer
            };
            _layer.Add(blockId, layer);
        }

        var position = genBlock.transform.position + new Vector3(x, y);
        var rotation = genBlock.transform.rotation;

        var block = Instantiate(genBlock, position, rotation, _layer[blockId].transform);
        block.transform.localScale = genBlock.transform.localScale;

        switch (blockId)
        {
            //生成したものがチェックポイントの場合順番付加
            case { } when CheckPointOrder.Exists(checkPoint => checkPoint._id == blockId):
                {
                    var checkPointOrder = CheckPointOrder.Find(checkPoint => checkPoint._id == blockId);
                    var checkPoint = block.GetComponent<CheckPoint>();
                    if (checkPoint != null) checkPoint.Order = checkPointOrder._order;
                    break;
                }
            //回転ブロック＆レバー
            case { } when SwitchPairList.Exists(pair => pair._switchId == blockId):
                {
                    //ブロックIDからスイッチとブロックのIDを各登録
                    var switchLever = block.GetComponent<SwitchLever>();
                    if (switchLever != null) SwitchManager.Instance.SwitchesRegister(blockId, switchLever);
                    break;
                }
            case { } when SwitchPairList.Exists(pair => pair._blockId == blockId):
                {
                    var rotateBlock = block.GetComponent<RotateBlock>();
                    if (rotateBlock != null) rotateBlock.RotateBlockId = blockId;
                    break;
                }
        }
    }

    private void FillOuterZone(string blockId, GameObject genBlock)
    {
        for (var x = -9; x < Size.x + 9; ++x)
        {
            for (var y = -9; y < Size.y + 9; ++y)
            {
                if (x >= 0 && x < Size.x && y >= 0 && y < Size.y) continue;

                var mat = transform.localToWorldMatrix * genBlock.transform.localToWorldMatrix * Matrix4x4.Translate(new Vector3(x, y));
                _combineMap[blockId].Add(mat);
            }
        }
    }

    private void CombineMesh()
    {
        //メッシュ結合登録されたブロックを作成
        foreach (var combineMeshLayer in _combineMap)
        {
            var blockId = combineMeshLayer.Key;
            var matrices = combineMeshLayer.Value; 

            //原型のオブジェクト
            var original = _blockMap[blockId];

            var originalMesh = original.GetComponent<MeshFilter>();
            var originalMaterials = _customMaterialsMap.ContainsKey(blockId) ? _customMaterialsMap[blockId].ToArray() : original.GetComponent<MeshRenderer>().sharedMaterials;

            //統合するメッシュの配列
            var combine = new List<CombineInstance>();
            var triangleCount = new List<int>();

            for (var i = 0; i < originalMesh.sharedMesh.subMeshCount; ++i)
            {
                var triangle = 0;
                foreach (var ci in matrices.Select(t => new CombineInstance
                {
                    //共有するメッシュを登録
                    mesh = originalMesh.sharedMesh,
                    //座標系もWorldで合わせる
                    transform = t,
                    //サブメッシュ
                    subMeshIndex = i
                }))
                {
                    combine.Add(ci);
                    triangle += originalMesh.sharedMesh.GetTriangles(i).Length;
                }
                triangleCount.Add(triangle);
            }

            //メッシュレイヤーオブジェクト作成
            var meshLayer = new GameObject(original.name + "Layer")
            {
                transform =
                {
                    parent = transform
                },
                tag = original.tag,
                layer = original.layer
            };


            //メッシュ情報
            var meshFilter = meshLayer.AddComponent<MeshFilter>();
            meshFilter.mesh = new Mesh
            {
                indexFormat = IndexFormat.UInt32,
            };
            meshFilter.mesh.CombineMeshes(combine.ToArray());
            meshFilter.mesh.subMeshCount = triangleCount.Count;

            var triangles = meshFilter.mesh.triangles.ToList();
            for (var i = 0; i < meshFilter.mesh.subMeshCount; ++i)
            {
                meshFilter.mesh.SetTriangles(triangles.GetRange(triangleCount.Take(i).Sum(), triangleCount[i]).ToArray(), i);
            }

            //マテリアル情報
            var meshRenderer = meshLayer.AddComponent<MeshRenderer>();
            meshRenderer.materials = originalMaterials;
            
            if (original.GetComponent<Collider>() == null) continue;
            var meshCollider = meshLayer.AddComponent<MeshCollider>();
            meshCollider.material = CombineMeshPhysicMaterial;
        }
    }
}
