using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FieldData
{
    public string FileName;
    public List<string> SheetNames;
    public List<BlockData> BlockDataList;

    [Serializable]
    public class BlockData
    {
        public string PrefabPath;
        public string Id;
        public List<string> Materials;
        public Vector3 Offset = Vector3.zero;
        public Vector2 Scale = Vector2.one;
        public float Angle = 0;
        public bool Combinable;
        public Color MapColor = new Color(1, 1, 1, 0.7f);
        public List<float> NumberParam;
        public List<string> StrParam;
        public string Method = "";
    }
}
