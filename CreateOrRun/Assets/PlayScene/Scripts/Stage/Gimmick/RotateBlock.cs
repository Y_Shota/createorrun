using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBlock : MonoBehaviour
{
    [Tooltip("壁オブジェクトのプレハブ")]
    [SerializeField] private GameObject BlockPrefab;
    [Tooltip("時計回りか反時計回りか")]
    [SerializeField] private bool IsClockOrdrer = false;
    [Tooltip("縦方向か横方向か")]
    [SerializeField] private bool IsVertical = false;
    //[Tooltip("回転角度")]
    //[SerializeField] private int _RotateAngle = 0;
    [Tooltip("回転の速度")]
    [SerializeField] private float RotateSpeed;

    //各々の回転ブロックが持つID
    public string RotateBlockId { set; get; }
    //↑に対応するスイッチID
    private string _switchId;
    //回転ブロックのデフォルト姿勢
    private Quaternion _defaultRotation;
    //リセット回数
    //private int _resetCount;

    [SerializeField] private bool SmartRot = false;

    public float DefaultAngle = 0;
    public float RotateAngle = 90;
    public float Length = 3;
    public float RotateTime = 0.5f;

    public string Id { get; set; }

    private Quaternion _toRotation;
    private bool _isRotate = false;

    void Start()
    {
        if (SmartRot)
        {
            Setup();
            return;
        }

        //デフォの姿勢登録
        _defaultRotation = gameObject.transform.rotation;
        //スイッチID登録
        _switchId = (int.Parse(RotateBlockId) - 100).ToString();

        //_resetCount = 0;

        if (IsVertical)
        {
            float ResPosY = BlockPrefab.transform.localScale.y / 2 - 0.5f;
            Vector3 ResPos = gameObject.transform.position;
            ResPos.y -= ResPosY;
            Instantiate(BlockPrefab, ResPos, Quaternion.identity).transform.SetParent(gameObject.transform);
        }
        else
        {
            float ResPosX = -BlockPrefab.transform.localScale.y / 2 + 0.5f;
            Vector3 ResPos = gameObject.transform.position;
            ResPos.x += ResPosX;
            Instantiate(BlockPrefab, ResPos, Quaternion.Euler(0, 0, 90.0f)).transform.SetParent(gameObject.transform);
        }

        if (IsClockOrdrer)
            RotateAngle *= -1;
    }

    private void Setup()
    {
        _defaultRotation = Quaternion.Euler(0, 0, DefaultAngle);
        transform.rotation = _defaultRotation;

        _toRotation = Quaternion.Euler(0, 0, DefaultAngle + RotateAngle);

        var wall = transform.GetChild(0);
        wall.localScale = new Vector3(Length, 1, 1);
        wall.localPosition = new Vector3(0.5f * (Length - 1), 0);
    }

    void Update()
    {
        if (SmartRot) return;

        //リセット
        if (SwitchManager.Instance.IsReset) 
            BlockReset();
        //回転
        else if (RotateBlockId != null)
            BlockRotate();
    }

    //ブロックの回転
    private void BlockRotate()
    {
        if (SwitchManager.Instance.SwitchDictionaty[_switchId])
        {
            Quaternion quat1 = gameObject.transform.rotation;
            Quaternion quat2 = Quaternion.Euler(0, 0, RotateAngle);

            gameObject.transform.rotation = Quaternion.Slerp(quat1, quat2, RotateSpeed * Time.deltaTime);
            //gameObject.transform.RotateAround(_center, _axis, RotateSpeed * Time.deltaTime);
        }
    }

    //ブロックの反回転(リセット)
    private void BlockReset()
    {
        gameObject.transform.rotation = _defaultRotation;
        SwitchManager.Instance.ResetCount++;

        //リセットしたブロックが最後のものだったら(ブロックリセット回数がスイッチの個数と同じなら)
        if (SwitchManager.Instance.ResetCount != SwitchManager.Instance.SwitchDictionaty.Count) return;

        //リセットフラグを元に
        SwitchManager.Instance.IsReset = false;
        SwitchManager.Instance.ResetCount = 0;
    }

    public void OnRotate()
    {
        if (_isRotate) return;

        StartCoroutine(nameof(RotateCor));
    }

    public void OnReset()
    {
        if (!_isRotate) return;

        StartCoroutine(nameof(ResetCor));
    }

    private IEnumerator RotateCor()
    {
        _isRotate = true;
        var collider = GetComponentInChildren<Collider>();
        collider.enabled = false;
        var timer = 0f;
        for (;;)
        {
            transform.rotation = _defaultRotation * Quaternion.Euler(0, 0, Mathf.Lerp(0, RotateAngle, timer / RotateTime));

            if (timer >= RotateTime)
            {
                transform.rotation = _toRotation;
                collider.enabled = true;
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }
    private IEnumerator ResetCor()
    {
        _isRotate = false;
        var collider = GetComponentInChildren<Collider>();
        collider.enabled = false;
        var timer = 0f;
        for (;;)
        {
            transform.rotation = _toRotation * Quaternion.Euler(0, 0, Mathf.Lerp(0, -RotateAngle, timer / RotateTime));

            if (timer >= RotateTime)
            {
                transform.rotation = _defaultRotation;
                collider.enabled = true;
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }
}

/*  メモ
 *  汎用的なものにするため全方位に対応させる
 *  IDでスイッチとブロックの紐づけも(対応するIDのスイッチと床を動かす)
 *  必要な情報
 *  ・ID
 *  ・回転の向き
 *  ・回転の始点 => 横と縦で分けなきゃいけない
 *  ・回転度合い
 */
