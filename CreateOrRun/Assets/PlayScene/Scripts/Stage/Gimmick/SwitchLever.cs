using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLever : MonoBehaviour
{
    [SerializeField] private bool SmartFlg = false;

    public string Id { get; set; }
    public string SwitchId { set; get; }

    private bool _isRotate;
    private Transform _axis;

    private Quaternion _toRotate;
    private Quaternion _fromRotate;

    private void Start()
    {
        _axis = transform.Find("Model/Axis");

        if (_axis == null) return;

        _toRotate = Quaternion.Euler(0, 0, -30);
        _fromRotate = Quaternion.Inverse(_toRotate);

        _axis.rotation = _fromRotate;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (SmartFlg)
        {
            if (_isRotate) return;

            if (!other.CompareTag("Player")) return;

            SwitchManager.Instance.Activate(Id);
            StartCoroutine(nameof(RotateCor));

            //エフェクト
            ParticleManager.Instance.PlayBlueFlashEffect(gameObject.transform.position, Quaternion.identity, 1f);

            //効果音
            SEManager.Instance.PlaySE("Switch", 3);

            return;
        }

        if (!other.CompareTag("Player") || SwitchManager.Instance.SwitchDictionaty[SwitchId]) return;        

        //スイッチ(フラグ)ON
        SwitchManager.Instance.SwitchDictionaty[SwitchId] = true;

        //エフェクト
        ParticleManager.Instance.PlayBlueFlashEffect(gameObject.transform.position, Quaternion.identity, 1f);
    }

    public void OnReset()
    {
        if (!_isRotate) return;

        StartCoroutine(nameof(ResetCor));
    }

    private IEnumerator RotateCor()
    {
        _isRotate = true;

        var maxTime = 0.5f;
        var timer = 0f;

        for (;;)
        {
            _axis.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(30, -30, timer / maxTime));

            if (timer >= maxTime)
            {
                _axis.rotation = _toRotate;
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }
    private IEnumerator ResetCor()
    {
        _isRotate = false;

        var maxTime = 0.5f;
        var timer = 0f;

        for (;;)
        {
            _axis.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(-30, 30, timer / maxTime));

            if (timer >= maxTime)
            {
                _axis.rotation = _fromRotate;
                yield break;
            }

            yield return null;
            timer += Time.deltaTime;
        }
    }

}

/*  メモ
 *  SwitchManagerみたいなオブジェクトを準備してスイッチとブロックのID・状態を登録
 *  対応するIDのものを連動させる
 */