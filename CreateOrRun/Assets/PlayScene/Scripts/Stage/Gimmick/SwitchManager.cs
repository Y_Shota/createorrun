using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SwitchManager : Singleton<SwitchManager>
{
    //全スイッチと回転ブロックのDictinary
    private SortedDictionary<string, bool> _switchDictinary = new SortedDictionary<string, bool>();
    public SortedDictionary<string, bool> SwitchDictionaty => _switchDictinary;
    public bool IsReset { set; get; }
    public int ResetCount { set; get; }
    public string LastSwitchIdToString => _switchDictinary.LastOrDefault().Key;
    public int LastSwitchIdToInt => int.Parse(_switchDictinary.LastOrDefault().Key);

    public string FirstSwitchIdToString => _switchDictinary.FirstOrDefault().Key;
    public int FirstSwitchIdToInt => int.Parse(_switchDictinary.FirstOrDefault().Key);

    private readonly Dictionary<string, List<RotateBlock>> _rotateBlockMap = new Dictionary<string, List<RotateBlock>>();
    private readonly Dictionary<string, List<SwitchLever>> _switchLeverMap = new Dictionary<string, List<SwitchLever>>();


    private readonly UnityAction<Scene> _sceneUnloaded;

    public SwitchManager()
    {
        IsReset = false;
        ResetCount = 0;

        _sceneUnloaded = scene =>
        {
            Destroy();
            SceneManager.sceneUnloaded -= _sceneUnloaded;
        };

        SceneManager.sceneUnloaded += _sceneUnloaded;
    }

    public void RegisterSwitchLever(string id, SwitchLever switchLever)
    {
        if (!_switchLeverMap.ContainsKey(id))
        {
            _switchLeverMap.Add(id, new List<SwitchLever>());
        }
        _switchLeverMap[id].Add(switchLever);
        switchLever.Id = id;
    }
    public void RegisterRotateBlock(string id, RotateBlock rotateBlock)
    {
        if (!_rotateBlockMap.ContainsKey(id))
        {
            _rotateBlockMap.Add(id, new List<RotateBlock>());
        }
        _rotateBlockMap[id].Add(rotateBlock);
        rotateBlock.Id = id;
    }

    public void Activate(string id)
    {
        if (!_rotateBlockMap.ContainsKey(id)) return;

        foreach (var rotateBlock in _rotateBlockMap[id])
        {
            rotateBlock.OnRotate();
        }
    }

    public void SwitchesRegister(string blockId, SwitchLever switchLever)
    {
        //スイッチと回転床の登録
        _switchDictinary.Add(blockId, false);
        switchLever.SwitchId = blockId;
    }

    //全スイッチの起動フラグのリセット
    public void AllSwitchReset()
    {
        //登録したスイッチの個数分ループ
        for (int i = 0; i < _switchDictinary.Count; i++)
        {
            var key = (i + FirstSwitchIdToInt).ToString();
            if (_switchDictinary[key])
                _switchDictinary[key] = false;
        }
        //リセットフラグON
        IsReset = true;

        foreach (var rotateBlock in _rotateBlockMap.Values.SelectMany(rotateBlocks => rotateBlocks))
        {
            rotateBlock.OnReset();
        }
        foreach (var lever in _switchLeverMap.Values.SelectMany(lever => lever))
        {
            lever.OnReset();
        }
    }
}