using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Material))]
public class BackGroundManager : SingletonMonoBehaviour<BackGroundManager>
{
    private Texture2D _backGroundTexture;
    private Material _material;
    private MeshRenderer _renderer;

    void Start()
    {
        _renderer = gameObject.AddComponent<MeshRenderer>();
        _material = _renderer.material;
        _renderer.receiveShadows = false;
    }

    //�w�i�摜�̃Z�b�g
    public void InstantiateBackGround(int width, int height, float Far, Texture2D backGroundTex)
    {
        _backGroundTexture = backGroundTex;
        _material.SetTexture("_MainTex", _backGroundTexture);
        gameObject.transform.localScale = new Vector3(width / 7f, 1, height / 7f);
        gameObject.transform.localPosition = new Vector3(width / 2f, height / 2f, Far);
        gameObject.transform.localRotation = Quaternion.Euler(90, 0, 180);
    }
}
