using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//↓スクリプトをアタッチしたときに自動的に付加してくれるコンポーネント
//[RequireComponent(typeof(MeshFilter))]
//[RequireComponent(typeof(MeshRenderer))]
public class MeshCombiner : MonoBehaviour
{
    //ステージの親となているオブジェクト
    [SerializeField] GameObject StageParentObject;
    //統合したマテリアル
    [SerializeField] Material CombinedMaterial;

    void Start()
    {
        Combine();
    }

    void Combine()
    {
        //ステージの親にフィルターとレンダラー付加(あるなら無視)
        var pMeshFilter = CheckComponent<MeshFilter>(StageParentObject.gameObject);
        var pMashRender = CheckComponent<MeshRenderer>(StageParentObject.gameObject);

        //子オブジェクトのフィルター
        //MeshFilter[] meshFilters = StageParentObject.GetComponentsInChildren<MeshFilter>();
        ////↑の参照を入れるリスト
        //List<MeshFilter> meshes = new List<MeshFilter>();

        ////親(配列の最初の要素)を含まないので1スタート
        //for(int i = 1; i < meshFilters.Length; i++)
        //{
        //    meshes.Add(meshFilters[i]);
        //}

        var meshes = StageParentObject.GetComponentsInChildren<MeshFilter>().ToList();
        meshes.Remove(meshes.First());

        //統合するメッシュの配列
        CombineInstance[] combines = new CombineInstance[meshes.Count];

        for(int i = 0; i < meshes.Count; i++)
        {
            //共有するメッシュを登録
            combines[i].mesh = meshes[i].sharedMesh;
            //座標系もWorldで合わせる
            combines[i].transform = meshes[i].transform.localToWorldMatrix;

            //統合されたオブジェクトは非アクティブに
            meshes[i].gameObject.SetActive(false);
            Destroy(meshes[i].gameObject);
        }

        //統合したメッシュをセット
        pMeshFilter.mesh = new Mesh();

        //統合数が多い時のための文↓
        pMeshFilter.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        pMeshFilter.mesh.CombineMeshes(combines);


        //マテリアルもセット
        pMashRender.material = CombinedMaterial;


        //コライダー
        StageParentObject.AddComponent<MeshCollider>();
        //統合されたメッシュをもつ親オブジェクトをアクティブにしてひとつのメッシュとして扱う
        StageParentObject.gameObject.SetActive(true);
    }

    //コンポーネントが無ければそれを付加するテンプレート
    T CheckComponent<T>(GameObject obj) where T : Component
    {
        var comp = obj.GetComponent<T>();
        if (comp == null)        
            comp = obj.AddComponent<T>();
        
        return comp;
    }
}
/*  StageManagerにてステージを生成した後にメッシュの統合を行う
 *  メッシュ統合でひとつのマテリアルに統一することも可能っぽい
 */