using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnplacableArea : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Removal")) return;
        
         Destroy(other.gameObject);
    }
}
