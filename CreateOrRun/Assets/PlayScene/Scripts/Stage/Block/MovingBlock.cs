using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlock : MonoBehaviour, IMovableBlock
{
    public float Speed = 1f;
    public float Distance = 1f;
    public float Angle = 0;
    public Vector3 Movement => transform.position - _prevPosition;
    public bool CanDuplicated => true;

    private float _lastUpdateTime;
    private Vector3 _prevPosition;
    private Vector3 _basePosition;
    private Vector3 _direction;

    private void Start()
    {
        _prevPosition = transform.position;
        _basePosition = transform.position;
        _direction = Quaternion.Euler(0, 0, Angle) * Vector3.right;
    }
    
    private void FixedUpdate()
    {
        if (Mathf.Approximately(_lastUpdateTime, Time.time)) return;

        _lastUpdateTime = Time.time;
        _prevPosition = transform.position;

        var delta = Mathf.Sin(_lastUpdateTime * Speed) * Distance;
        transform.position = _basePosition + _direction * delta;
    }

}
