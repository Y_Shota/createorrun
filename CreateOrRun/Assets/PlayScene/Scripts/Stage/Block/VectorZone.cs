using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorZone : MonoBehaviour
{
    public float Force = 0.05f;
    public Vector2 Direction = new Vector2(1, 0);

    private Vector3 _direction;

    private void Start()
    {
        _direction = Direction;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        var rigid = other.GetComponent<Rigidbody>();
        rigid.MovePosition(rigid.position + _direction * Force);
    }
}
