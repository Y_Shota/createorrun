using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxSpawner : MonoBehaviour
{
    [Tooltip("アイテムボックスがなくなってから再生成されるまでの時間")]
    [SerializeField] private float WaitTime;
    [Tooltip("アイテムボックス")]
    [SerializeField] private GameObject ItemBoxPrefab;

    float _deltaTime;   //アイテムボックスが無い状態での経過時間
   

    void Start()
    {
        Spawn();
    }

    void Update()
    {
        //子オブジェクトがいない状態が WaitTime 秒続いたら新しいアイテムボックスを生成する

        if (transform.childCount > 0) return;

        _deltaTime += Time.deltaTime;
        if (_deltaTime < WaitTime) return;

        Spawn();
        _deltaTime = 0;
    }
    
    //アイテムボックスの生成
    void Spawn()
    {
        GameObject newItemBox           = Instantiate(ItemBoxPrefab);
        newItemBox.transform.parent     = transform;
        newItemBox.transform.position   = transform.position;
    }
}
