using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private int _order;
    public int Order
    {
        get => _order;
        set
        {
            _order = value;
            CheckPointManager.Instance.Register(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<PlayerController>();

        if (player == null) return;
        
        RankingManager.Instance.NotifyPassCheckPoint(player.PlayerId, Order);
    }
}
