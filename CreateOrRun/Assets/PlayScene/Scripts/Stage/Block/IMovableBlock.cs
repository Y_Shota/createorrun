using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovableBlock
{
    public Vector3 Movement { get; }
    public bool CanDuplicated { get; }
}
