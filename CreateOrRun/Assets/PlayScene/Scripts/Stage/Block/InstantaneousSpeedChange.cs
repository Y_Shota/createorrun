using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantaneousSpeedChange : MonoBehaviour
{
    [SerializeField] private float Magnification = 1;
    [SerializeField] private float EffectiveTime;
    [SerializeField] private string Key;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<PlayerController>();

        if (player == null) return;

        player.ChangeVelocityRateOrder(Key, Magnification, EffectiveTime);

        Destroy(gameObject);
    }
}
