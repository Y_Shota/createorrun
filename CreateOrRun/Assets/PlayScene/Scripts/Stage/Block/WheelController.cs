using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    [Range(1, 8)]
    public int Amount = 8;
    public float Radius = 1;
    public float Speed = 1;
    public Vector2 Scale = Vector2.one;
    public bool IsAnticlockwise = true; 

    [SerializeField] private GameObject RotateBlockOrigin;

    private float _lastUpdateTime;
    private readonly List<Transform> _rotateBlocks = new List<Transform>();
    private Vector3 _axis;

    private void Start()
    {
        _axis = IsAnticlockwise ? Vector3.forward : Vector3.back;

        for (var i = 0; i < Amount; ++i)
        {
            var blockTrans = Instantiate(RotateBlockOrigin, transform).transform;
            blockTrans.localScale = new Vector3(Scale.x, Scale.y, 0.9f);    //ステージと被らないようにZちょっと小さく
            blockTrans.localPosition = Quaternion.Euler(0, 0, 360f / Amount * i) * (Vector3.up * Radius);

            blockTrans.gameObject.SetActive(true);

            _rotateBlocks.Add(blockTrans);
        }
    }
    
    private void FixedUpdate()
    {
        if (Mathf.Approximately(_lastUpdateTime, Time.time)) return;

        _lastUpdateTime = Time.time;

        foreach (var rotateBlock in _rotateBlocks.Select(rotateBlock => rotateBlock.GetComponent<WheelBlock>()))
        {
            rotateBlock.UpdatePrevPosition();
        }
        
        transform.rotation *= Quaternion.AngleAxis(Speed * Time.deltaTime, _axis);

        foreach (var rotateBlock in _rotateBlocks)
        {   
            rotateBlock.rotation = Quaternion.identity;
        }
    }
}
