using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorBlock : MonoBehaviour, IMovableBlock
{
    public float Force = 0.05f;
    public Vector2 Direction = Vector2.right;

    public Vector3 Movement { get; private set; }
    public bool CanDuplicated => false;

    private void Start()
    {
        Movement = Direction * Force;
    }
}
