using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelBlock : MonoBehaviour, IMovableBlock
{
    public Vector3 Movement => transform.position - _prevPosition;
    public bool CanDuplicated => true;

    private Vector3 _prevPosition;

    private void Start()
    {
        UpdatePrevPosition();
    }

    public void UpdatePrevPosition()
    {
        _prevPosition = transform.position;
    }
}
