using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedDownBlockSpawner : MonoBehaviour
{
    [Tooltip("ブロックがなくなってから再生成されるまでの時間")]
    [SerializeField] private float WaitTime;
    [Tooltip("ブロック")]
    [SerializeField] private GameObject SpeedDownBlockPrefab;

    float _deltaTime;   //ブロックが無い状態での経過時間

    void Start()
    {
        Spawn();
    }

    void Update()
    {
        //子オブジェクトがいない状態が WaitTime 秒続いたら新しいブロックを生成する

        if (transform.childCount > 1) return;

        _deltaTime += Time.deltaTime;
        if (_deltaTime < WaitTime) return;

        Spawn();
        _deltaTime = 0;
    }

    //ブロックの生成
    void Spawn()
    {
        GameObject newItemBox           = Instantiate(SpeedDownBlockPrefab, transform);
        newItemBox.SetActive(true);
    }
}
