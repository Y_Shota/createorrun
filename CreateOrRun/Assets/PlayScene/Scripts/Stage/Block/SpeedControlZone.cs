using UnityEngine;

public class SpeedControlZone : MonoBehaviour
{
    [SerializeField] private float Magnification = 1;
    [SerializeField] private float EffectiveTime;
    [SerializeField] private string PositiveKey;
    [SerializeField] private string NegativeKey;


    private void OnTriggerStay(Collider other)
    {
        var player = other.gameObject.GetComponent<PlayerController>();

        if (player == null) return;

        player.ChangeVelocityRateOrder(Magnification > 0 ? PositiveKey : NegativeKey, Magnification, EffectiveTime);
    }
}
