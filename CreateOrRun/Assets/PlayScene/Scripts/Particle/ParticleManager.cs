using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//todo シングルトンにする
public class ParticleManager : SingletonMonoBehaviour<ParticleManager>
{
    //各パーティクルプレハブ
    [SerializeField] public GameObject JumpEffect;
    [SerializeField] public GameObject WallJumpEffect;
    [SerializeField] public GameObject DashEffect;
    [SerializeField] public GameObject CreateEffect;
    [SerializeField] public GameObject DestroyEffect;
    [SerializeField] public GameObject WallSmokeEffect;
    [SerializeField] public GameObject GravityWaveEffect;
    [SerializeField] public GameObject AfterImageEffect;
    [SerializeField] public GameObject BlueFlashEffect;
    [SerializeField] public GameObject TimeStopEffect;
    [SerializeField] public GameObject BuffEffect;
    [SerializeField] public GameObject DebuffEffect;
    [SerializeField] public GameObject CatchEffect;

    //引数：�@座標、�A向き(クォータニオン)、�B大きさ
    //ジャンプのエフェクト専用関数
    public void PlayJumpEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(JumpEffect, pos, dir, transform).transform.localScale *= size;
    }

    //壁ジャンプ
    public void PlayWallJumpEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(WallJumpEffect, pos, dir, transform).transform.localScale *= size;
    }

    //ダッシュ
    public GameObject PlayDashEffect(Vector3 pos, Quaternion dir, float size)
    {
        var ret = Instantiate(DashEffect, pos, dir, transform);
        ret.transform.localScale *= size;

        return ret;
    }

    //アイテム設置
    public void PlayCreateEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(CreateEffect, pos, dir, transform).transform.localScale *= size;
    }

    //アイテム除去
    public void PlayDestroyEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(DestroyEffect, pos, dir, transform).transform.localScale *= size;
    }

    //壁ずりスモーク
    public void PlayWallSmokeEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(WallSmokeEffect, pos, dir, transform).transform.localScale *= size;
    }

    //重力球ウェーブ
    public void PlayGravityWaveEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(GravityWaveEffect, pos, dir, transform).transform.localScale *= size;
    }

    //残像
    public void PlayAfterImageEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(AfterImageEffect, pos, dir, transform).transform.localScale *= size;
    }

    //青光
    public void PlayBlueFlashEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(BlueFlashEffect, pos, dir, transform).transform.localScale *= size;
    }

    //時間停止モノ
    public void PlayTimeStopEffect(Vector3 pos, Quaternion dir, float size, float time)
    {
        ParticleSystem particleSystem = TimeStopEffect.GetComponentInChildren<ParticleSystem>();
        var main = particleSystem.main;
        main.duration = time;
        Instantiate(particleSystem, pos, dir, transform).transform.localScale *= size; ;
    }

    //バフ(スピードアップ)
    public GameObject PlayBuffEffect(Vector3 pos, Quaternion dir, float size)
    {
        var ret = Instantiate(BuffEffect, pos, dir, transform);
        ret.transform.localScale *= size;
        return ret;
    }

    //デバフ(スピードダウン)
    public GameObject PlayDebuffEffect(Vector3 pos, Quaternion dir, float size)
    {
        var ret = Instantiate(DebuffEffect, pos, dir, transform);
        ret.transform.localScale *= size;
        return ret;
    }

    public void PlayCatchEffect(Vector3 pos, Quaternion dir, float size)
    {
        Instantiate(CatchEffect, pos, dir, transform).transform.localScale *= size;
    }

    //汎用
    public void PlayParticle(GameObject prefab, Vector3 pos, Quaternion dir, float size)
    {

    }
}
