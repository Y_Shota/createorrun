using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AfterImage : MonoBehaviour
{
    [SerializeField] private Transform PlayerPrefab = default;

    private List<Transform> bones;
    private List<Transform> targetBones;
    private int _boneCounts;

    private void GetBone(Transform trans, string str = "")
    {
        //子
        Transform playerModel = gameObject.transform.Find("Model");
        //↑の子(孫)
        Transform playerCtrl = playerModel.transform.Find("Character1_Ctrl_Reference");

        //Character1_Ctrl_Referenceの子のボーンを各リストに格納
        //foreach (Transform childTrans in playerCtrl) {}
        //全子供(孫含む)を取得する
        Transform[] childernTranses = trans.GetComponentsInChildren<Transform>().Select(t => t.transform).ToArray();

        for (int i = 0; i < childernTranses.Length; i++)
        {
            //パスを作る
            string path = str == "" ? childernTranses[i].name : str + "/" + childernTranses[i].name;
            //自身の中の同じ名前のオブジェクトを探す(ここも孫以下を見るように修正)
            Transform[] sameNameObject = playerModel.GetComponentsInChildren<Transform>().Select(t => t.transform).ToArray();
            //リストに入れる
            bones.Add(sameNameObject[i]);
            targetBones.Add(childernTranses[i]);
        }
    }

    public void ChangeTarget(Transform trans)
    {
        bones = new List<Transform>();
        targetBones = new List<Transform>();
        GetBone(trans);
        _boneCounts = bones.Count;
    }

    void Start()
    {
        bones = new List<Transform>();
        targetBones = new List<Transform>();
        GetBone(PlayerPrefab.Find("Model"));
        _boneCounts = bones.Count;
    }

    void Update()
    {
        // 位置と回転を合わせる
        for (int i = 0; i < _boneCounts; i++)
        {
            bones[i].position = Vector3.Lerp(bones[i].position, targetBones[i].position, 0.5f * Time.deltaTime);
            bones[i].rotation = Quaternion.Lerp(bones[i].rotation, targetBones[i].rotation, 0.5f * Time.deltaTime);
        }       
    }    
}
