using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefaultItem
{
    enum DefaultItems
    {
        BOMB,
        BLOCK,
        MAX
    }

    public struct DefaultItemState
    {
        public bool isCoolTime;    //クールタイム中か
        public float deltaTime;    //前回使用からの経過時間（クールタイム）
    }

    DefaultItems        _currentItem = DefaultItems.BLOCK;
    DefaultItemState[]  _defaultItemStates = new DefaultItemState[(int)DefaultItems.MAX];
    bool _isTrrigerPrevFrame = false;

    public DefaultItem() { _defaultItemStates[(int)DefaultItems.BLOCK].isCoolTime = true; }

    public void Update(PlayerController player)
    {
        //クールタイムの処理
        for (int i = 0; i < (int)DefaultItems.MAX; i++)
        {
            if (!_defaultItemStates[i].isCoolTime) continue;
            
            _defaultItemStates[i].deltaTime += Time.deltaTime;

            //ゲージの値を再設定
            string[]    gageNames       = { "BombGauge", "WallGauge" };
            float[]     coolTimes       = { player.GetComponent<ItemHandlerField>().BombCoolTime, player.GetComponent<ItemHandlerField>().BlockCoolTime };
            float       gaugeFillAmount = _defaultItemStates[i].deltaTime / coolTimes[i];
            Transform   itemImage       = UIManager.Instance.CanvasList[player.PlayerId].transform.Find("ItemImage(Clone)");

            itemImage.Find(gageNames[i]).GetComponent<Image>().fillAmount = gaugeFillAmount;
            itemImage.Find(gageNames[i] + "Back").GetComponent<Image>().fillAmount = 1 - gaugeFillAmount;

            //クールタイム解除
            if (_defaultItemStates[i].deltaTime >= coolTimes[i])
            {
                _defaultItemStates[i].isCoolTime = false;
                _defaultItemStates[i].deltaTime = 0;
                itemImage.Find(gageNames[i]).GetComponent<Image>().fillAmount = 1;
                itemImage.Find(gageNames[i] + "Back").GetComponent<Image>().fillAmount = 0;
            }
        }

        //使用
        if (!_isTrrigerPrevFrame)
        {
            if (player.IsUsedBombLeft)   UseBomb(Direction.Left);
            if (player.IsUsedBombRight)  UseBomb(Direction.Right);

            if (player.IsUsedBlockLeft)  UseBlock(Direction.Left);
            if (player.IsUsedBlockRight) UseBlock(Direction.Right);
        }

        if (player.IsRotationItem)      BlockHandler.Instance.Rotate(player.PlayerId);
        
        //長押しでのアイテム使用対策
        if (player.Pad.LTrigger >= 1 || player.Pad.RTrigger >= 1)  
            _isTrrigerPrevFrame = true;
        else
            _isTrrigerPrevFrame = false;


        void UseBomb(Direction direction)
        {
            if (_defaultItemStates[(int)DefaultItems.BOMB].isCoolTime) return;

            BombHandler.Instance.Use(player, direction);

            _defaultItemStates[(int)DefaultItems.BOMB].isCoolTime = true;

            //爆弾のゲージ残量0に
            Transform itemImage = UIManager.Instance.CanvasList[player.PlayerId].transform.Find("ItemImage(Clone)");
            itemImage.Find("BombGauge").GetComponent<Image>().fillAmount = 0;
            itemImage.Find("BombGaugeBack").GetComponent<Image>().fillAmount = 1;
        }

        void UseBlock(Direction direction)
        {
            if (_defaultItemStates[(int)DefaultItems.BLOCK].isCoolTime) return;

            BlockHandler.Instance.Use(player, direction);

            _defaultItemStates[(int)DefaultItems.BLOCK].isCoolTime = true;

            //ブロックのゲージ残量0に           
            Transform itemImage = UIManager.Instance.CanvasList[player.PlayerId].transform.Find("ItemImage(Clone)");
        }
    }
}
