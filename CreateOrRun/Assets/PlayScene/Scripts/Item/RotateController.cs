using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateController : MonoBehaviour
{
    [SerializeField] private float FramePerAngle;

    private void Start()
    {
        StartCoroutine(RotateCor());
    }

    private IEnumerator RotateCor()
    {
        for (;;)
        {
            transform.rotation *= Quaternion.Euler(0, 0, FramePerAngle);

            yield return null;
        }
    }
}
