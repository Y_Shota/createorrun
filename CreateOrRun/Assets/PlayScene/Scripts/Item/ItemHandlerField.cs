using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHandlerField : MonoBehaviour
{
    [Tooltip("投げる力 (x > 0)")]
    [SerializeField] private float ThrowForce;
    [Tooltip("ブロック設置時のプレイヤーとの距離 (x > 0)")]
    [SerializeField] private float BlockPutWide;
    [Tooltip("重力球設置時のプレイヤーとの距離 (x > 0)")]
    [SerializeField] private float GravitySpherePutWide;
    [Tooltip("爆弾のクールタイム (x > 0)")]
    [SerializeField] private float DefaultBombCoolTime;
    [Tooltip("ブロックのクールタイム (x > 0)")]
    [SerializeField] private float DefaultBlockCoolTime;

    public float ThrowForceValue => ThrowForce;
    public float BlockPutWideValue => BlockPutWide;
    public float GravitySpherePutWideValue => GravitySpherePutWide;
    public float BombCoolTime => DefaultBombCoolTime;
    public float BlockCoolTime => DefaultBlockCoolTime;
}
