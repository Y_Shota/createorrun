using System.Collections.Generic;
using UnityEngine;

public class BlockHandler : ItemHandler
{
    private static BlockHandler _instance = new BlockHandler();
    public static BlockHandler Instance => _instance;
    private BlockHandler() { }

    public override string Name => "Block";

    struct RotationAngle
    {
        public int _playerId;
        public Vector3 _rotation;

        public RotationAngle(int playerId, Vector3 rotation) { _playerId = playerId; _rotation = rotation; }
    }
    List<RotationAngle> _rotationAngles = new List<RotationAngle>();

    //更新
    public override void Update(PlayerController player)
    {
        if (player.IsRotationItem)  Rotate(player.PlayerId);
        if (player.IsUsedItemLeft)  Use(player, Direction.Left);
        if (player.IsUsedItemRight) Use(player, Direction.Right);
    }

    public override void Use(PlayerController player, Direction direction)
    {
        //アイテムオブジェクトの生成
        GameObject gameObject = GameObject.Find("ItemObjectGenerator").GetComponent<ItemObjectGenerator>().InstantiateBlock;

        //リリースフラグをtrueに
        gameObject.GetComponent<BlockController>().IsReleased = true;

        //親の変更
        gameObject.transform.parent = GameObject.Find("StageManager")?.transform;

        //少し設置位置をプレイヤーから離す
        Vector3 vector3 = player.transform.position;
        vector3.x += player.GetComponent<ItemHandlerField>().BlockPutWideValue * direction.Sign;
        gameObject.transform.position = vector3;

        //回転させる
        RotationAngle rotationAngle = FindRotationAngle(player.PlayerId);
        gameObject.transform.Rotate(rotationAngle._rotation);
        _rotationAngles.Remove(rotationAngle);

        //エフェクト
        ParticleManager.Instance.PlayCreateEffect(gameObject.transform.position, Quaternion.identity, 1f);

        //効果音
        SEManager.Instance.PlaySE("Wall", 5);

        player.StartUseAnimation(direction);

        return;

        ////プレイヤーの保持アイテムを空にする
        //player.Handler = EmptyHandler.Instance;
        //ItemUIManager.Instance.ChangeItemUI(player);
    }

    //回転
    public override void Rotate(int playerId)
    {
        //90度回転
        for (int i = 0; i < _rotationAngles.Count; i++) 
        {
            if (_rotationAngles[i]._playerId != playerId) continue;

            RotationAngle rotationAngle = _rotationAngles[i];
            rotationAngle._rotation.z += 90f;
            _rotationAngles[i] = rotationAngle;
            return;

        }
        _rotationAngles.Add(new RotationAngle(playerId, new Vector3(0, 0, 90f)));
    }

    RotationAngle FindRotationAngle(int playerId)
    {
        foreach (RotationAngle rotationAngle in _rotationAngles)
        {
            if (rotationAngle._playerId == playerId) return rotationAngle;
        }

        return new RotationAngle(playerId, Vector3.zero);
    }
}
