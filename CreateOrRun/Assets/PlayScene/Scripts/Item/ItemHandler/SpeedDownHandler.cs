using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedDownHandler : ItemHandler
{
    private static SpeedDownHandler _instance = new SpeedDownHandler();
    public static SpeedDownHandler Instance => _instance;
    private SpeedDownHandler() { }

    public override string Name => "SpeedDown";

    //更新
    public override void Update(PlayerController player)
    {
        if (player.IsUsedItemLeft) Use(player, Direction.Left);
        if (player.IsUsedItemRight) Use(player, Direction.Right);
    }

    //使用
    public override void Use(PlayerController player, Direction direction)
    {
        //アイテムオブジェクトの生成
        GameObject gameObject = GameObject.Find("ItemObjectGenerator").GetComponent<ItemObjectGenerator>().InstantiateSpeedDown;

        //タグの変更
        gameObject.tag = "Untagged";

        //リリースフラグをtrueに
        gameObject.GetComponent<SpeedDownController>().IsReleased = true;

        //アイテムの位置を使用する方向にする
        SetItemPositionPlayerSide(gameObject, player, direction);

        //発射
        Vector3 throwVector = new Vector3(1f, 0f, 0f) * player.GetComponent<ItemHandlerField>().ThrowForceValue * direction.Sign;
        gameObject.GetComponent<Rigidbody>().AddForce(throwVector, ForceMode.Impulse);

        //エフェクト
        ParticleManager.Instance.PlayCreateEffect(gameObject.transform.position, Quaternion.identity, 1f);

        //プレイヤーの保持アイテムを空にする
        player.Handler = EmptyHandler.Instance;
        ItemUIManager.Instance.ChangeItemUI(player);

        player.StartUseAnimation(direction);
    }
}
