using UnityEngine;

public class GravitySphereHandler : ItemHandler
{
    private static GravitySphereHandler _instance = new GravitySphereHandler();
    public static GravitySphereHandler Instance => _instance;
    private GravitySphereHandler() { }

    public override string Name => "GravitySphere";

    //更新
    public override void Update(PlayerController player)
    {
        if (player.IsUsedItemLeft)  Use(player, Direction.Left);
        if (player.IsUsedItemRight) Use(player, Direction.Right);
    }

    //使用
    public override void Use(PlayerController player, Direction direction)
    {
        //アイテムオブジェクトの生成
        GameObject gameObject = GameObject.Find("ItemObjectGenerator").GetComponent<ItemObjectGenerator>().InstantiateGravitySphere;

        //タグの変更
        gameObject.tag = "Untagged";

        //リリースフラグをtrueに
        GravitySphereController gravitySphereController = gameObject.GetComponent<GravitySphereController>();
        gravitySphereController.IsReleased = true;

        Vector3 vector3 = player.transform.position;
        vector3.x += player.GetComponent<ItemHandlerField>().GravitySpherePutWideValue * direction.Sign;
        gameObject.transform.position = vector3;

        //エフェクト
        ParticleManager.Instance.PlayCreateEffect(gameObject.transform.position, Quaternion.identity, 1f);

        //効果音
        SEManager.Instance.PlaySE("Gravity");

        //プレイヤーの保持アイテムを空にする
        player.Handler = EmptyHandler.Instance;
        ItemUIManager.Instance.ChangeItemUI(player);

        player.StartUseAnimation(direction);
    }
}
