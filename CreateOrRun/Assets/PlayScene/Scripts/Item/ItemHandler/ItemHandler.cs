using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemHandler
{
    //操作を行うオブジェクトの名前
    public abstract string Name{ get; }

    //更新
    public virtual void Update(PlayerController player) { }

    //使用
    public virtual void Use(PlayerController player, Direction direction) { }

    //回転
    public virtual void Rotate(int playerId) { }

    //プレイヤーの隣にオブジェクトの位置を変更する
    public virtual void SetItemPositionPlayerSide(GameObject gameObject, PlayerController player, Direction direction)
    {
        float   half            = 0.5f;
        float   gap             = 0.3f;
        float   shiftWidth      = (gameObject.transform.localScale.x + player.transform.localScale.x) * half + gap;
        float   shiftPositionX  = shiftWidth * direction.Sign;
        Vector3 newPosition     = player.transform.position;
        newPosition.x          += shiftPositionX;

        gameObject.transform.position = newPosition;
    }
}
