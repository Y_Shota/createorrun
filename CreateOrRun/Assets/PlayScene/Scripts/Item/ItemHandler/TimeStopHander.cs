using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeStopHander : ItemHandler
{
    private static TimeStopHander _instance = new TimeStopHander();
    public static TimeStopHander Instance => _instance;
    private TimeStopHander() { }

    public override string Name => "TimeStop";

    //更新
    public override void Update(PlayerController player)
    {
        if (player.IsUsedItemLeft)  Use(player, Direction.Left);
        if (player.IsUsedItemRight) Use(player, Direction.Right);
    }

    //使用
    public override void Use(PlayerController player, Direction direction)
    {
        //プレイヤーの保持アイテムを空にする
        player.Handler = EmptyHandler.Instance;
        ItemUIManager.Instance.ChangeItemUI(player);

        //最下位以外使えない
        var rank = RankingManager.Instance.GetRank(player.PlayerId);
        if (rank != PlayerManager.Instance.Players.Count - 1) return;

        player.StartUseAnimation(direction);

        //時間停止モノ
        foreach(var other in PlayerManager.Instance.PlayerControllers)
        {
            if (other == player) continue;

            var time = 3 - RankingManager.Instance.GetRank(other.PlayerId);

            other.TakeTimeItem(time);

            //エフェクト
            ParticleManager.Instance.PlayTimeStopEffect(other.transform.position, Quaternion.identity, 1, time);
        }
    }
}
