
public class EmptyHandler : ItemHandler
{
    private static EmptyHandler _instance = new EmptyHandler();
    public static EmptyHandler Instance => _instance;
    private EmptyHandler() { }
    public override string Name => "Empty";
}
