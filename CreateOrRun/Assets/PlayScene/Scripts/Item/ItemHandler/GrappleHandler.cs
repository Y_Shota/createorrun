using UnityEngine;

public class GrappleHandler : ItemHandler
{
    private static GrappleHandler _instance = new GrappleHandler();
    public static GrappleHandler Instance => _instance;
    private GrappleHandler() { }

    public override string Name => "Grapple";

    //更新
    public override void Update(PlayerController player)
    {
        if (player.IsUsedItemLeft)  Use(player, Direction.Left);
        if (player.IsUsedItemRight) Use(player, Direction.Right);
    }

    //使用
    public override void Use(PlayerController player, Direction direction)
    {
        //使用者の順位が1位の場合はアイテムを破棄
        int userRank = RankingManager.Instance.GetRank(player.PlayerId);
        if (userRank == 0)
        {
            player.Handler = EmptyHandler.Instance;
            ItemUIManager.Instance.ChangeItemUI(player);
            return;
        }

        //アイテムオブジェクトの生成
        GameObject gameObject = GameObject.Find("ItemObjectGenerator").GetComponent<ItemObjectGenerator>().InstantiateGrapple;

        //タグの変更
        gameObject.tag = "Untagged";

        //アイテムの位置を使用する方向にする
        SetItemPositionPlayerSide(gameObject, player, direction);

        //一つ上の順位にいるプレイヤーを標的にする
        int targetRank  = userRank - 1;
        int targetId    = RankingManager.Instance.GetPlayerId(targetRank);
        PlayerController TargetPlayer = PlayerManager.Instance.PlayerControllers[targetId];

        //アイテム使用
        GrappleController grappleController = gameObject.GetComponent<GrappleController>();
        grappleController.enabled           = true;
        grappleController.Use(player, TargetPlayer);

        //エフェクト
        ParticleManager.Instance.PlayCreateEffect(gameObject.transform.position, Quaternion.identity, 1f);

        //プレイヤーの保持アイテムを空にする
        player.Handler = EmptyHandler.Instance;
        ItemUIManager.Instance.ChangeItemUI(player);

        player.StartUseAnimation(direction);
    }
}
