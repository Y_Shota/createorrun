using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpHandler : ItemHandler
{
    private static SpeedUpHandler _instance = new SpeedUpHandler();
    public static SpeedUpHandler Instance => _instance;
    private SpeedUpHandler() { }

    public override string Name => "SpeedUp";

    //更新
    public override void Update(PlayerController player)
    {
        if (player.IsUsedItemLeft) Use(player, Direction.Left);
        if (player.IsUsedItemRight) Use(player, Direction.Right);
    }

    //使用
    public override void Use(PlayerController player, Direction direction)
    {
        //アイテムオブジェクトの生成
        GameObject gameObject = GameObject.Find("ItemObjectGenerator").GetComponent<ItemObjectGenerator>().InstantiateSpeedUp;

        //プレイヤーのスピードアップ
        gameObject.GetComponent<SpeedUpController>().SpeedUpPlayer(player);

        //エフェクト
        ParticleManager.Instance.PlayCreateEffect(gameObject.transform.position, Quaternion.identity, 1f);

        //プレイヤーの保持アイテムを空にする
        player.Handler = EmptyHandler.Instance;
        ItemUIManager.Instance.ChangeItemUI(player);

        player.StartUseAnimation(direction);
    }
}