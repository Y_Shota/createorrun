using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObjectGenerator : SingletonMonoBehaviour<ItemObjectGenerator>
{
    [Tooltip("爆弾の生成を行うか")]
    [SerializeField] public bool IsGenerateBomb;
    [Tooltip("ブロックの生成を行うか")]
    [SerializeField] public bool IsGenerateBlock;
    [Tooltip("加速アイテムの生成を行うか")]
    [SerializeField] public bool IsGenerateSpeedUp;
    [Tooltip("減速アイテムの生成を行うか")]
    [SerializeField] public bool IsGenerateSpeedDown;
    [Tooltip("重力球の生成を行うか")]
    [SerializeField] public bool IsGenerateGravitySphere;
    [Tooltip("グラップルの生成を行うか")]
    [SerializeField] public bool IsGenerateGrapple;
    [Tooltip("時間停止の生成を行うか")]
    [SerializeField] public bool IsGenerateTimeStop;

    [Tooltip("爆弾")]
    [SerializeField] private GameObject ItemBombPrefab;
    [Tooltip("ブロック")]
    [SerializeField] private GameObject ItemBlockPrefab;
    [Tooltip("加速アイテム")]
    [SerializeField] private GameObject ItemSpeedUpPrefab;
    [Tooltip("減速アイテム")]
    [SerializeField] private GameObject ItemSpeedDownPrefab;
    [Tooltip("重力球")]
    [SerializeField] private GameObject ItemGravitySpherePrefab;
    [Tooltip("グラップル")]
    [SerializeField] private GameObject ItemGrapplePrefab;
    [Tooltip("時間停止")]
    [SerializeField] private GameObject ItemTimeStopPrefab;

    //爆弾生成
    public GameObject InstantiateBomb => Instantiate(ItemBombPrefab);

    //ブロック生成
    public GameObject InstantiateBlock => Instantiate(ItemBlockPrefab);

    //加速アイテム生成
    public GameObject InstantiateSpeedUp => Instantiate(ItemSpeedUpPrefab);

    //減速アイテム生成
    public GameObject InstantiateSpeedDown => Instantiate(ItemSpeedDownPrefab);

    //重力球生成
    public GameObject InstantiateGravitySphere => Instantiate(ItemGravitySpherePrefab);

    //グラップル生成
    public GameObject InstantiateGrapple => Instantiate(ItemGrapplePrefab);

    //グラップル生成
    public GameObject InstantiateTimeStop => Instantiate(ItemTimeStopPrefab);
}
