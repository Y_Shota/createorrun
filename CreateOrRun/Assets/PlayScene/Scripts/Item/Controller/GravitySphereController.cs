using UnityEngine;

public class GravitySphereController : MonoBehaviour
{
    [Tooltip("重力の強さ (x > 0)")]
    [SerializeField] private float GravityForce;
    [Tooltip("重力球の力が適用される半径 (x > 0)")]
    [SerializeField] private float RangeRadius;
    [Tooltip("使用されてから効果を出すまでの時間 (x > 0)")]
    [SerializeField] private float WaitTime;
    [Tooltip("効果の継続時間 (x > 0)")]
    [SerializeField] private float EffectTime;

    float        _deltaTime;                    //使用されてからの経過時間
    bool         _isActivated;                  //有効化されているか

    public bool  IsReleased { get; set; }       //プレイヤーから離れた後か
    public float Gravity => GravityForce;       //重力

    void Start()
    {
    }

    void Update()
    {
        if (!IsReleased) return;

        _deltaTime += Time.deltaTime;

        //経過時間に対する処理
        if (!_isActivated)
        {
            //使用されてから一定時間が経過していたら有効化する
            if (_deltaTime >= WaitTime) Activate();
        }
        else
        {
            //有効化されてから一定時間経過していたら消滅させる
            if (_deltaTime >= EffectTime) Destroy(gameObject);
        }
    }

    //重力球を有効化する
    void Activate()
    {
        SphereCollider childSphereCollider  = transform.GetChild(0).GetComponent<SphereCollider>();
        childSphereCollider.radius          = RangeRadius;
        childSphereCollider.enabled         = true;

        _deltaTime      = 0;
        _isActivated    = true;

        //エフェクト
        ParticleManager.Instance.PlayGravityWaveEffect(gameObject.transform.position, Quaternion.identity, 1);

        //効果音
        SEManager.Instance.PlaySE("GravitySuction");
    }
}
