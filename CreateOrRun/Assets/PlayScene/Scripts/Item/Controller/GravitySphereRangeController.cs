using UnityEngine;

public class GravitySphereRangeController : MonoBehaviour
{
    void Start()
    {
        GetComponent<Collider>().enabled = false;
    }

    void Update()
    {
    }

    void OnTriggerStay(Collider collider)
    {
        //効果範囲内にいるプレイヤーに対して一定の力をかける

        if (!collider.CompareTag("Player")) return;

        float   gravity         = transform.parent.GetComponent<GravitySphereController>().Gravity;
        Vector3 direction       = (transform.parent.position - collider.transform.position).normalized;
        Vector3 gravityVector   = direction * gravity;

        collider.GetComponent<Rigidbody>().AddForce(gravityVector);
    }
}
