using UnityEngine;

public class RopeController : MonoBehaviour
{
    void Start()
    {
    }

    void Update()
    {
    }

    void OnTriggerEnter(Collider other)
    {
        //ステージに触れたら親の削除フラグをtrueに
        //設置前のブロックに触れたらブロックを削除
        if (other.transform.root.name != "StageManager") return;
        if (other.CompareTag("Intangible"))
        {
            Destroy(other.gameObject);
            return;
        }

        transform.parent.gameObject.GetComponent<GrappleController>().IsDestroy = true;
    }
}
