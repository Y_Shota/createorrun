using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    [Tooltip("爆発までの時間 (x > 0)")]
    [SerializeField] private float DestroyTimerLimit;

    //設置からの経過時間
    private float _deltaTime;

    struct BuriedPlayer
    {
        public bool IsResolved { get; set; }
        public GameObject Player { get; private set; }

        public BuriedPlayer(bool isResolved, GameObject player) { IsResolved = isResolved; Player = player; }
    }

    List<BuriedPlayer> _buriedPlayers;
    bool _CheckedBuried;

    //プレイヤーから離れた後か
    public bool IsReleased { get; set; }

    void Start()
    {
        _buriedPlayers = new List<BuriedPlayer>();
    }

    void Update()
    {
        //使用後一定時間が経過したら自オブジェクトを破棄する

        if (!IsReleased) return;
        if (!_CheckedBuried) return;

        if (GetComponent<Collider>().isTrigger) Activate();

        UpdateBuried();

        _deltaTime += Time.deltaTime;
        if (_deltaTime >= DestroyTimerLimit) Destroy(gameObject);


        //埋まっている状態が解決したプレイヤーを通常の状態に戻す
        void UpdateBuried()
        {
            List<BuriedPlayer> removeBuriedPlayers = new List<BuriedPlayer>();

            //埋まり状態でなくなったプレイヤーを探す
            for (int i = 0; i < _buriedPlayers.Count; i++)
            {
                BuriedPlayer buriedPlayer = _buriedPlayers[i];

                if (buriedPlayer.IsResolved)
                {
                    removeBuriedPlayers.Add(buriedPlayer);
                }
            }

            //埋まっていないプレイヤーを埋まり中プレイヤーのリストから除外
            foreach(BuriedPlayer removeBuriedPlayer in removeBuriedPlayers)
            {
                RevertBuriedPlayer(removeBuriedPlayer.Player);
                _buriedPlayers.Remove(removeBuriedPlayer);
            }
        }

        //有効化
        void Activate()
        {
            GetComponent<Collider>().isTrigger = false;
            gameObject.tag = "Removal";
        }
    }

    private void LateUpdate()
    {
        if (!IsReleased) return;
        if (_CheckedBuried) return;

        _CheckedBuried = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        //設置後最初の当たり判定時のみ処理を行う
        //埋まっているプレイヤーオブジェクトの登録

        if (!GetComponent<Collider>().isTrigger) return;
        if (other.gameObject.layer == LayerMask.NameToLayer("IgnorePlayer")) return;
        if (other.gameObject.layer == LayerMask.NameToLayer("UnplacableArea")) return;

        if (other.CompareTag("Player"))
        {
            ChangeLayerToIgnorePlayer(other.gameObject);
            BuriedPlayer buriedPlayer = new BuriedPlayer(false, other.gameObject);
            AddColliderObject(buriedPlayer.Player);
            _buriedPlayers.Add(buriedPlayer);
            return;
        }

        if (other.transform.parent == null) return;
        if (other.transform.parent.CompareTag("Player"))
        {
            ChangeLayerToIgnorePlayer(other.transform.parent.gameObject);
            BuriedPlayer buriedPlayer = new BuriedPlayer(false, other.transform.parent.gameObject);
            AddColliderObject(buriedPlayer.Player);
            _buriedPlayers.Add(buriedPlayer);
            return;
        }


        //プレイヤーのレイヤーをIgnorePlayerに変更する
        //ブロックに埋まったプレイヤーが行動不能にならないように
        void ChangeLayerToIgnorePlayer(GameObject player)
        {
            int layerIgnorePlayer = LayerMask.NameToLayer("IgnorePlayer");

            player.layer = layerIgnorePlayer;
            foreach (Transform child in player.transform)
            {
                if (child.CompareTag("FieldDetector")) child.gameObject.layer = layerIgnorePlayer;
            }
        }

        //当たり判定の追加
        void AddColliderObject(GameObject player)
        {
            //全てのレイヤーを変えると埋まり状態から脱出したかが確認できないため
            //その判定をとるためのオブジェクトを追加
            GameObject newGameObject = new GameObject("ColliderObjectForCheckedBuried");
            newGameObject.transform.SetParent(player.transform);
            newGameObject.transform.localPosition = new Vector3(0, 0, 0);

            CapsuleCollider capsuleCollider = player.GetComponent<CapsuleCollider>();
            CapsuleCollider collider = newGameObject.AddComponent<CapsuleCollider>();
            collider.height = capsuleCollider.height;
            collider.radius = capsuleCollider.radius;
            collider.isTrigger = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        //埋まり状態が解決したプレイヤーを判別する

        if (_buriedPlayers.Count == 0) return;
        if (other.gameObject.layer == LayerMask.NameToLayer("IgnorePlayer")) return;
        if (!other.transform.parent.CompareTag("Player")) return;

        GameObject player = other.transform.parent.gameObject;
        for (int i = 0; i < _buriedPlayers.Count; i++)
        {
            BuriedPlayer buriedPlayer = _buriedPlayers[i];
            if (buriedPlayer.Player != player) continue;

            buriedPlayer.IsResolved = true;
            _buriedPlayers[i] = buriedPlayer;
        }
    }

    void OnDestroy()
    {
        //埋まっていた全プレイヤーの状態をもとに戻す
        foreach (BuriedPlayer buriedPlayer in _buriedPlayers)
        {
            RevertBuriedPlayer(buriedPlayer.Player);
        }
    }

    //埋まったプレイヤーに対して変更を行った項目を変更前の状態に戻す
    void RevertBuriedPlayer(GameObject player)
    {
        //レイヤーを元に戻す
        ChangeLayerToDefaultPlayer(player);

        //当たり判定用のオブジェクト削除
        Destroy(player.transform.Find("ColliderObjectForCheckedBuried").gameObject);


        //プレイヤーのレイヤーをPlayerに変更する
        void ChangeLayerToDefaultPlayer(GameObject player)
        {
            player.layer = LayerMask.NameToLayer("Player");

            int layerFieldDetector = LayerMask.NameToLayer("FieldDetector");
            foreach (Transform child in player.transform)
            {
                if (child.CompareTag("FieldDetector"))
                {
                    child.gameObject.layer = layerFieldDetector;
                    continue;
                }
                if(child.name == "OuterCollider")
                {
                    child.gameObject.layer = LayerMask.NameToLayer("Default");
                }
                if (child.name == "InnerCollider")
                {
                    child.gameObject.layer = LayerMask.NameToLayer("Player");
                }
            }
        }
    }
}
