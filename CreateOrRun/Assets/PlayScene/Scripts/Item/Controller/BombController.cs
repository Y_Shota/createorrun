using UnityEngine;

public class BombController : MonoBehaviour
{

    public GameObject _particleObject;          //パーティクル

    [Tooltip("爆発までの時間 (x > 0)")]
    [SerializeField] private float ExplosionTimerLimit;

    //設置からの経過時間
    private float _deltaTime;

    //プレイヤーから離れた後か
    public bool IsReleased { get; set; }


    void Start()
    {
    }

    void Update()
    {
        //使用後一定時間が経過したら爆発する

        if (!IsReleased) return;

        _deltaTime += Time.deltaTime;

        if (_deltaTime >= ExplosionTimerLimit) Explosion();
    }


    //爆発
    private void Explosion(Collider collider = null)
    {
        //エフェクト
        Instantiate(_particleObject, this.transform.position, Quaternion.identity);

        //効果音
        SEManager.Instance.PlaySE("Bomb", 5);

        //引数オブジェクトの破壊
        if (collider != null) Destroy(collider.gameObject);
    }

    //衝突時処理
    private void OnTriggerEnter(Collider other)
    {
        //衝突したオブジェクトが破壊可能なら爆発
        if (other.CompareTag("Removal")) Explosion(other);

        //自オブジェクトの破壊
        Destroy(gameObject);
    }
}
