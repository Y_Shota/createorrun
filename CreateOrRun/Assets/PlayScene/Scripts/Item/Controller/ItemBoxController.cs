using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemBoxController : MonoBehaviour
{

    int _generateItemSize;

    [Serializable]
    public class ItemRate
    {
        public string name;
        public float  rate;
    }

    [Serializable]
    public class ItemRates
    {
        public List<ItemRate> rates;
    }

    [Tooltip("色変更で使用するテクスチャー")]
    [SerializeField] private List<ItemRates> ItemRateOfRank;
    void Start()
    {
        StartCoroutine(Spawning());

        var itemObjectGenerator = ItemObjectGenerator.Instance;

        if (itemObjectGenerator.IsGenerateBomb)             _generateItemSize++;
        if (itemObjectGenerator.IsGenerateBlock)            _generateItemSize++;
        if (itemObjectGenerator.IsGenerateSpeedUp)          _generateItemSize++;
        if (itemObjectGenerator.IsGenerateSpeedDown)        _generateItemSize++;
        if (itemObjectGenerator.IsGenerateGravitySphere)    _generateItemSize++;
        if (itemObjectGenerator.IsGenerateGrapple)          _generateItemSize++;
        if (itemObjectGenerator.IsGenerateTimeStop)         _generateItemSize++;
    }

    void Update()
    {
        var time = Time.deltaTime;
        transform.rotation *= Quaternion.Euler(0, time * 100f, time * 50f);
    }

    //衝突処理
    void OnTriggerStay(Collider collider)
    {
        //衝突したオブジェクトが"Player"タグを持っていれば
        //アイテムを生成して自オブジェクトを破壊する
        if(collider.CompareTag("Player"))
        {
            //アイテム生成
            //GeneratePlayerItemByRandom(collider.gameObject.GetComponent<PlayerController>());
            GeneratePlayerItem(collider.gameObject.GetComponent<PlayerController>());

            //エフェクト生成
            ParticleManager.Instance.PlayDestroyEffect(gameObject.transform.position, Quaternion.identity, 1f);

            //効果音
            SEManager.Instance.PlaySE("ItemGet");

            //自オブジェクトを破壊
            Destroy(gameObject);
        }
    }

    private IEnumerator Spawning()
    {
        const float time = 0.5f;
        var timer = 0f;

        var from = Vector3.zero;
        var to = transform.localScale;

        for (;;)
        {
            transform.localScale = Vector3.Lerp(from, to, timer / time);

            if (timer >= time) yield break;

            timer += Time.deltaTime;
            yield return null;
        }
    }

    void GeneratePlayerItem(PlayerController playerController)
    {
        if (PlayerManager.Instance.Players.Count <= 1) GeneratePlayerItemByRandom(playerController);
        else                                           GeneratePlayerItemByRanking(playerController);
    }

    void GeneratePlayerItemByRanking(PlayerController playerController)
    {
        if (playerController.Handler.Name != "Empty") return;

        List<ItemHandler> handlers = new List<ItemHandler>();            //生成されるアイテム配列

        ItemObjectGenerator itemObjectGenerator = ItemObjectGenerator.Instance;
        if (itemObjectGenerator.IsGenerateBomb)             handlers.Add(BombHandler.Instance);
        if (itemObjectGenerator.IsGenerateBlock)            handlers.Add(BlockHandler.Instance);
        if (itemObjectGenerator.IsGenerateSpeedUp)          handlers.Add(SpeedUpHandler.Instance);
        if (itemObjectGenerator.IsGenerateSpeedDown)        handlers.Add(SpeedDownHandler.Instance);
        if (itemObjectGenerator.IsGenerateGravitySphere)    handlers.Add(GravitySphereHandler.Instance);
        if (itemObjectGenerator.IsGenerateGrapple)          handlers.Add(GrappleHandler.Instance);
        if (itemObjectGenerator.IsGenerateTimeStop)         handlers.Add(TimeStopHander.Instance);

        int     rank            = RankingManager.Instance.GetRank(playerController.PlayerId);   //1位 = 0
        int     adjustedRank    = (int)Math.Round((float)rank / PlayerManager.Instance.Players.Count * 4, MidpointRounding.AwayFromZero);   //4人プレイ以外に対応するためにアイテム取得用の順位を作る
        float   random          = UnityEngine.Random.Range(0f, 1f);
        float   itemRangeMax    = 0;

        //アイテムを生成する
        for (int i = 0; i < _generateItemSize; i++)
        {
            foreach (ItemRate rate in ItemRateOfRank[adjustedRank].rates)
            {
                if (rate.name != handlers[i].Name) continue;

                itemRangeMax += rate.rate;
                if (itemRangeMax <= random) break;

                playerController.Handler = handlers[i];
                ItemUIManager.Instance.ChangeItemUI(playerController);

                return;
            }
        }
    }

    //アイテム生成
    void GeneratePlayerItemByRandom(PlayerController playerController)
    {
        if (playerController.Handler.Name != "Empty") return;

        //生成フラグがtrueのアイテムハンドラーのインスタンスをランダムで渡す

        ItemHandler[] handlers = new ItemHandler[_generateItemSize];            //生成されるアイテム配列

        int count = 0;
        ItemObjectGenerator itemObjectGenerator = ItemObjectGenerator.Instance;
        if (itemObjectGenerator.IsGenerateBomb)             handlers[count++] = BombHandler.Instance;
        if (itemObjectGenerator.IsGenerateBlock)            handlers[count++] = BlockHandler.Instance;
        if (itemObjectGenerator.IsGenerateSpeedUp)          handlers[count++] = SpeedUpHandler.Instance;
        if (itemObjectGenerator.IsGenerateSpeedDown)        handlers[count++] = SpeedDownHandler.Instance;
        if (itemObjectGenerator.IsGenerateGravitySphere)    handlers[count++] = GravitySphereHandler.Instance;
        if (itemObjectGenerator.IsGenerateGrapple)          handlers[count++] = GrappleHandler.Instance;
        if (itemObjectGenerator.IsGenerateTimeStop)         handlers[count++] = TimeStopHander.Instance;

        int randomItem = UnityEngine.Random.Range(0, handlers.Length);
        playerController.Handler = handlers[randomItem];

        ItemUIManager.Instance.ChangeItemUI(playerController);
    }
}
