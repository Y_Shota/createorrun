using UnityEngine;

public class SpeedDownController : MonoBehaviour
{
    [Tooltip("減速時の速度 (x > 0)")]
    [SerializeField] private float Velocity;
    [Tooltip("アイテムを投げてから消えるまでの秒数 (x > 0)")]
    [SerializeField] private float TimeLimit;

    //使用後か
    public bool IsReleased { get; set; }

    //使用時からの経過時間
    private float _deltaTime;

    void Start()
    {
    }

    void Update()
    {
        if (!IsReleased) return;

        _deltaTime += Time.deltaTime;

        if (_deltaTime >= TimeLimit)
        {
            Destroy(gameObject);
        }
    }

    //衝突時処理
    private void OnTriggerEnter(Collider other)
    {
        //衝突したプレイヤーの速度を落とす
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().ChangeVelocityRateOrder("DownItem", 0.5f, 2f, false);
            //エフェクト            
            //ParticleManager.Instance.PlayDebuffEffect(other.transform.position, Quaternion.identity, 1);
            //効果音
            SEManager.Instance.PlaySE("Debuff", 1.5f);
        }

        Destroy(gameObject);
    }
}
