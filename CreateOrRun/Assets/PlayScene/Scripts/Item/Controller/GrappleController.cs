using UnityEngine;

public class GrappleController : MonoBehaviour
{
    [Tooltip("伸びる速度 (x > 0)")]
    [SerializeField] private float ChasingSpeed;
    [Tooltip("引っ張る力 (x > 0)")]
    [SerializeField] private float PullingSpeed;

    bool                    _isGrappled;                   //掴んだか
    bool                    _isReleased;                   //プレイヤーから離れた後か
    PlayerController        _usedPlayer;                   //アイテムを使用したプレイヤー
    PlayerController        _targetPlayer;                 //ターゲットしているプレイヤー
    Vector3                 _directionUsedToTarget;        //使用プレイヤーからターゲットプレイヤーへの方向

    Transform               Rope => transform.GetChild(0); //ロープの変形情報

    public bool IsDestroy  { get; set; }

    void Start()
    {
    }

    void Update()
    {
        if (!_isReleased)    return;

        Deformation();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isGrappled) return;

        //プレイヤーと衝突した場合は掴み状態になる
        //ステージオブジェクトと衝突した場合は自分を削除する
        if (!other.CompareTag("Player"))
        {
            var parent = other.transform.parent;
            if (!parent)
            {
                //親がいない場合
                //Destroy(gameObject);
                return;
            }
            if (parent.gameObject.CompareTag("Player"))
            {
                //衝突オブジェクトの親がプレイヤーだった場合
                if (other.gameObject.transform.parent.GetComponent<PlayerController>().PlayerId == _usedPlayer.PlayerId) return;

                Grapple();
                return;
            }
            else if(other.transform.root.name == "StageManager")
            {
                //衝突オブジェクトのルートがステージマネージャーだった場合
                Destroy(gameObject);
                return;
            }

            return;
        }

        if (other.GetComponent<PlayerController>().PlayerId == _usedPlayer.PlayerId) return;
        Grapple();
    }

    void OnDestroy()
    {
        if (!_isGrappled) return;
        _usedPlayer.GetComponent<Rigidbody>().isKinematic   = false;
        _targetPlayer.GetComponent<Rigidbody>().isKinematic = false;
    }

    //使用
    public void Use(PlayerController usedPlayer, PlayerController targetPlayer)
    {
        _usedPlayer     = usedPlayer;
        _targetPlayer   = targetPlayer;
        _isReleased     = true;

        Rope.gameObject.GetComponent<Collider>().enabled = true;
    }

    //伸びる
    void Extend()
    {
        //先端部（自オブジェクト）の位置をターゲットに近づける
        Vector3 direction   = (_targetPlayer.transform.position - transform.position).normalized;
        transform.position += direction * ChasingSpeed;

        //ロープを伸ばす
        Vector3 newScale = Rope.localScale;
        newScale.y       = (transform.position - _usedPlayer.transform.position).magnitude * 0.5f;
        Rope.localScale  = newScale;
    }

    //縮む
    void Shrink()
    {
        Vector3 direction = (transform.position - _usedPlayer.transform.position).normalized;
        transform.position -= direction * PullingSpeed;

        //ロープを縮める
        Vector3 newScale    = Rope.localScale;
        newScale.y         -= PullingSpeed;
        Rope.localScale     = newScale;

        if (Rope.localScale.y <= 0f) IsDestroy = true;
    }

    //変形する
    void Deformation()
    {
        if (!_usedPlayer)    return;
        if (!_targetPlayer)  return;

        //伸縮する
        if (_isGrappled)    Shrink();
        else                Extend();

        //ロープが使用プレイヤー -> 自オブジェクト間で繋がるようにする
        Vector3 aim             = transform.position - _usedPlayer.transform.position;
        transform.localRotation = Quaternion.LookRotation(aim) * Quaternion.Euler(90, 0, 0);

        float ropeLength         = Rope.localScale.y;
        float newLocalPositionY  = -ropeLength;
        Rope.localPosition       = new Vector3(0, newLocalPositionY, 0);


        //収縮中ロープの先端（使用プレイヤ側）の位置が使用プレイヤーの位置
        //掴む側の位置が標的プレイヤーの位置
        if (_isGrappled)
        {
            Vector3 newPlayerPosition   = new Vector3();
            newPlayerPosition.y         = newLocalPositionY;

            Vector3 localScale          = Rope.localScale;
            Rope.localScale             = new Vector3(1f, 1f, 1f);

            newPlayerPosition           = Rope.TransformPoint(newPlayerPosition);

            Rope.localScale             = localScale;
            newPlayerPosition.z         = 0;

            _usedPlayer.transform.position      = newPlayerPosition;
            _targetPlayer.transform.position    = transform.position;
        }

        //破棄フラグがtrueであれば自オブジェクトを破棄しプレイヤー同士をすこし引き離す
        if (!IsDestroy) return;

        Destroy(gameObject);

        Recoil();
    }

    //掴んだ時の処理
    void Grapple()
    {
        transform.position = new Vector3(_targetPlayer.transform.position.x, _targetPlayer.transform.position.y, 0);

        _isGrappled = true;

        _usedPlayer.GetComponent<Rigidbody>().isKinematic   = true;
        _targetPlayer.GetComponent<Rigidbody>().isKinematic = true;

        _directionUsedToTarget = Vector3.Normalize(transform.position - _usedPlayer.transform.position);

        //エフェクト
        ParticleManager.Instance.PlayCatchEffect(_targetPlayer.transform.position, Quaternion.identity, 1);

        //効果音
        SEManager.Instance.PlaySE("Catch", 2);
    }

    private void Recoil()
    {
        _usedPlayer.ChangeVelocityOrder(_directionUsedToTarget * 6f, 1);
        _targetPlayer.ChangeVelocityOrder(_directionUsedToTarget * -3f, 1);

        /*
        if (_usedPlayer.Dir == Direction.Right)
        {
            _usedPlayer.ChangeVelocityOrder(new Vector3(6, 4), 1);
            _targetPlayer.ChangeVelocityOrder(new Vector3(-3, 1), 1);
        }
        else
        {
            _usedPlayer.ChangeVelocityOrder(new Vector3(-6, 4), 1);
            _targetPlayer.ChangeVelocityOrder(new Vector3(3, 1), 1);
        }
        */
    }
}
