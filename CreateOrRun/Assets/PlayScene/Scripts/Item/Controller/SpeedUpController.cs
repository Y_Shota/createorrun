using UnityEngine;

public class SpeedUpController : MonoBehaviour
{
    [Tooltip("加速時の速度 (x > 0)")]
    [SerializeField] private float Velocity;
    [Tooltip("加速を行う秒数 (x > 0)")]
    [SerializeField] private float TimeLimit;

    //使用後か
    private bool IsUsed { get; set; }

    //使用時からの経過時間
    private float _deltaTime;

    //使用プレイヤー
    private PlayerController _player;

    void Start()
    {
        
    }

    void Update()
    {
        //使用後一定時間が経過したら自オブジェクトを破棄する

        if (!IsUsed) return;

        _deltaTime += Time.deltaTime;

        if (_deltaTime >= TimeLimit)
        {
            Destroy(gameObject);
        }
    }

    public void SpeedUpPlayer(PlayerController player)
    {
        //速度をあげる
        _player                 = player;
        IsUsed                  = true;

        _player.ChangeVelocityRateOrder("UpItem", 2f);

        //エフェクト
        //ParticleManager.Instance.PlayBuffEffect(player.transform.position, Quaternion.identity, 1);

        //透明にする
        GetComponent<Renderer>().material.color = Color.clear;
    }
}
