﻿using System;
using UnityEngine;

class FieldDetector : MonoBehaviour
{
    //検知方向列挙
    private enum DetectedType { Right, Left, Up, Down, FloorEdge };
    [Tooltip("検知する方向を指定")]
    [SerializeField] private DetectedType Type;

    public int GetDetectedType => (int)Type;

    //プレイヤ
    private PlayerController _player;
    private bool _wasDetect;

    //生成時処理
    private void Awake()
    {
        var player = transform.parent.gameObject;
        _player = player.GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        _wasDetect = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.transform.root.gameObject.name != "StageManager" &&
            !other.gameObject.CompareTag("Field")) return;
        
        _wasDetect = true;
    }

    public void UpdateDetectedState()
    {
        switch (Type)
        {
            case DetectedType.Right: _player.IsTouchingWallRight = _wasDetect; break;
            case DetectedType.Left: _player.IsTouchingWallLeft = _wasDetect; break;
            case DetectedType.Up: break;
            case DetectedType.Down: _player.IsTouchingFloor = _wasDetect; break;
            case DetectedType.FloorEdge: _player.IsGroundingEdge = _wasDetect; break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
