using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuterCollider : MonoBehaviour
{
    private Rigidbody _masterRigid;
    private PlayerController _player;

    private readonly List<Type> _duplicateList = new List<Type>(); 

    private void Start()
    {
        _masterRigid = transform.parent.GetComponent<Rigidbody>();
        _player = transform.parent.GetComponent<PlayerController>();
    }

    private void LateUpdate()
    {
        _duplicateList.Clear();
    }

    private void OnTriggerStay(Collider other)
    {
        //フィールド以外とは判定を行わない
        if (!other.gameObject.CompareTag("Field")) return;
        //時間停止中は動かない
        if (_player.IsTookTimeItem) return;

        //動くブロックのとき移動量を取得
        var iMovableBlock = other.GetComponent<IMovableBlock>();
        if (iMovableBlock == null || _duplicateList.Contains(iMovableBlock.GetType())) return;
        
        _masterRigid.MovePosition(_masterRigid.position + iMovableBlock.Movement);

        //重複チェック
        if (!iMovableBlock.CanDuplicated)
        {
            _duplicateList.Add(iMovableBlock.GetType());
        }

        //下方向へ動くとき落ちるのを回避
        if (_player.IsGrounding) _masterRigid.MovePosition(_masterRigid.position + new Vector3(0, -0.01f));
    }
}
