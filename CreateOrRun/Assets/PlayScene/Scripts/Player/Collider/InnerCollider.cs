using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InnerCollider : MonoBehaviour
{
    [SerializeField] private float RespawnTime = 1.5f;

    private bool _isDetected;
    private float _detectedTimer;

    private PlayerController _master;
    private Rigidbody _masterRigid;
    private CapsuleCollider _collider;

    private void Start()
    {
        _master = transform.parent.GetComponent<PlayerController>();
        _masterRigid = _master.GetComponent<Rigidbody>();
        _collider = GetComponent<CapsuleCollider>();
    }

    private void Update()
    {
        if (_isDetected)
        {
            _detectedTimer += Time.deltaTime;

            if (_detectedTimer >= RespawnTime)
            {
                _master.RespawnCheckPoint();
            }
        }
        else
        {
            _detectedTimer = 0;
        }
    }

    private void FixedUpdate()
    {
        _isDetected = false;
    }

    private void OnTriggerStay(Collider other)
    {
        //フィールド以外とは判定を行わない
        if (!other.gameObject.CompareTag("Field")) return;

        //コライダーの上下の座標取得
        var colliderCenter = _masterRigid.position + _collider.center;
        var topPoint = colliderCenter + new Vector3(0, _collider.height * 0.5f);
        var bottomPoint = colliderCenter - new Vector3(0, _collider.height * 0.5f);

        //重なっているフィールドの判定を取得
        var overLapColliders = Physics.OverlapCapsule(
            topPoint, 
            bottomPoint,
            _collider.radius);
        var targetColliders = overLapColliders.Where(c => c.CompareTag("Field"));

        foreach (var targetCollider in targetColliders)
        {
            var isCollision = Physics.ComputePenetration(
                _collider, transform.position, transform.rotation,
                targetCollider, targetCollider.transform.position, targetCollider.transform.rotation,
                out var direction, out var distance
            );

            if (!isCollision) continue;
            
            _masterRigid.position += direction * distance;
            _isDetected = true;
        }
    }
}
