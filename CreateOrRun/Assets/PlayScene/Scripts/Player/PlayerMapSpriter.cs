using System;
using UnityEngine;

public class PlayerMapSpriter : MonoBehaviour
{
    private Vector2Int _prevPosition;
    private Texture2D _mapTexture;

    [SerializeField]
    private Vector2 spritePosition;
    [SerializeField]
    private Vector2 spriteScale;

    public Sprite MapSprite { get; private set; }

    private void Start()
    {
        _prevPosition = new Vector2Int((int)Math.Round(transform.position.x), (int)Math.Round(transform.position.y - 0.5f));

        Generate();
        Clear();

        var id = GetComponent<PlayerController>().PlayerId;

        var image = UIManager.Instance.AddImagePlayerCanvas(id);
        image.sprite = StageManager.Instance.MiniMapSprite;
        image.rectTransform.localScale = spriteScale * 3;
        image.rectTransform.anchorMin = new Vector2(0, 1);
        image.rectTransform.anchorMax = new Vector2(0, 1);
        image.rectTransform.pivot = new Vector2(0, 1);
        image.rectTransform.anchoredPosition = spritePosition;

        image = UIManager.Instance.AddImagePlayerCanvas(id);
        image.sprite = MapSprite;
        image.rectTransform.anchoredPosition = new Vector2(0.5f, 0.5f);
        image.rectTransform.localScale = spriteScale * 3;
        image.rectTransform.anchorMin = new Vector2(0, 1);
        image.rectTransform.anchorMax = new Vector2(0, 1);
        image.rectTransform.pivot = new Vector2(0, 1);
        image.rectTransform.anchoredPosition = spritePosition;
    }

    private void Update()
    {
        UpdateTexture();

        _prevPosition = new Vector2Int((int)Math.Round(transform.position.x), (int)Math.Round(transform.position.y - 0.5f));
    }

    private void UpdateTexture()
    {
        for (var x = -1; x <= 1; ++x)
        {
            for (var y = -1; y <= 1; ++y)
            {
                for (var px = 0; px < 4; ++px)
                {
                    for (var py = 0; py < 4; ++py)
                    {
                        _mapTexture.SetPixel((_prevPosition.x + x) * 4 + px, (_prevPosition.y + y) * 4 + py, Color.clear);
                    }
                }
            }
        }
        for (var x = -1; x <= 1; ++x)
        {
            for (var y = -1; y <= 1; ++y)
            {
                for (var px = 0; px < 4; ++px)
                {
                    for (var py = 0; py < 4; ++py)
                    {
                        _mapTexture.SetPixel(((int)Math.Round(transform.position.x) + x) * 4 + px, ((int)Math.Round(transform.position.y - 0.5f) + y) * 4 + py, Color.red);
                    }
                }
            }
        }
        _mapTexture.Apply();
    }

    private void Generate()
    {
        var texSize = StageManager.Instance.Size * 4;
        _mapTexture = new Texture2D(texSize.x, texSize.y);

        _mapTexture.filterMode = FilterMode.Point;

        MapSprite = Sprite.Create(_mapTexture, new Rect(0, 0, _mapTexture.width, _mapTexture.height), new Vector2(0.5f, 0.5f));
    }

    private void Clear()
    {
        for (var mip = 0; mip < _mapTexture.mipmapCount; ++mip)
        {
            var colors = _mapTexture.GetPixels(mip);
            for (var i = 0; i < colors.Length; ++i)
            {
                colors[i] = Color.clear;
            }
            _mapTexture.SetPixels(colors, mip);
        }
        _mapTexture.Apply();
    }
}
