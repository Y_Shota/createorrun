﻿using UnityEngine;

//走る乗チア
class StateRun : PlayerState
{
    public override string Name => "Run";

    public override void Check(PlayerController player)
    {
        player.State =
            !player.IsGrounding ? Fall :    //地面にいないなら落下
            player.NeedIsJumping ? Jump :       //ジャンプキーが押されたらジャンプ
            !player.IsMoving ? Idle :       //動いていないなら待機
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        player.SetAnimationId(1);
        player.UseGravity = false;
    }

    public override void FixedUpdate(PlayerController player)
    {
        player.FixedMoveGround();
        player.ControlVelocity();
    }

    public override void End(PlayerController player)
    {
        player.UseGravity = true;
    }
}
