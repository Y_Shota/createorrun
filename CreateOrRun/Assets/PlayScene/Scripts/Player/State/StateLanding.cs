﻿class StateLanding : PlayerState
{
    public override string Name => "Landing";
    public override void Check(PlayerController player)
    {
        player.State =
            player.IsMoving ? Run :
            player.NeedIsJumping ? Jump :
            Idle;
    }

    public override void Start(PlayerController player)
    {
        player.Landing();
    }
}

