﻿//壁ずり状態
class StateWallSlide : PlayerState
{
    public override string Name => "WallSlide";

    public override void Check(PlayerController player)
    {
        player.State =
            player.NeedIsJumping ? WallJump :   //ジャンプキーが押されたら壁ジャンプ
            !player.IsWallSliding ? Fall :  //壁釣りが終わったら落下
            player.IsGrounding ? Idle :     //接地したら待機
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        player.SetAnimationId(3);
        player.StartWallSlide();
    }

    public override void FixedUpdate(PlayerController player)
    {
        player.FixedUpdateWallSlide();
    }

    public override void Update(PlayerController player)
    {
        player.UpdateWallSlide();
    }

    public override void End(PlayerController player)
    {
        player.EndWallSlide();
    }
}
