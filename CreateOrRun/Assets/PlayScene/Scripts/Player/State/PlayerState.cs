
//プレイヤの状態遷移用抽象クラス
public abstract class PlayerState
{
    //状態の名前
    public abstract string Name
    {
        get;
    }

    //状態が開始されるときに呼ばれる
    public virtual void Start(PlayerController player) {}
    //遷移先が存在するかのチェックに使う
    public abstract void Check(PlayerController player);
    //更新処理
    public virtual void Update(PlayerController player) {}
    //前更新処理
    public virtual void FixedUpdate(PlayerController player) {}
    //状態が終了するときに呼ばれる
    public virtual void End(PlayerController player) {}
    //デバッグ用文字列変換
    public override string ToString()
    {
        return "State: " + Name;
    }

    //プレイヤの状態列挙
    public static readonly PlayerState Idle = new StateIdle();              //待機
    public static readonly PlayerState Run = new StateRun();                //走る
    public static readonly PlayerState Fall = new StateFall();              //落下
    public static readonly PlayerState Rise = new StateRise();              //上昇
    public static readonly PlayerState Landing = new StateLanding();        //着地
    public static readonly PlayerState Jump = new StateJump();              //ジャンプ
    public static readonly PlayerState DoubleJump = new StateDoubleJump();  //二段ジャンプ
    public static readonly PlayerState WallSlide = new StateWallSlide();    //壁ずり
    public static readonly PlayerState WallJump = new StateWallJump();      //壁ジャンプ
    public static readonly PlayerState KeepState = new StateDummy();        //状態維持用ダミー
}
