﻿using UnityEngine;

//ジャンプ状態
class StateJump : StateFall
{
    public override string Name => "Jump";
    
    public override void Check(PlayerController player)
    {
        player.State =
            player.IsWallSliding && !player.IsRising ? WallSlide :  //壁方行に入力があるなら壁ずり
            player.IsDoubleJump ? DoubleJump :  //二段ジャンプが必要なら二段ジャンプ
            !player.IsRising ? Fall :
            player.IsGrounding && !player.IsRising ? Landing :         //着地したら待機状態
            player.NeedsJumpEnd ? Rise :
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        base.Start(player);
        player.SetAnimationId(2);
        player.StartJump();
    }

    public override void Update(PlayerController player)
    {
        base.Update(player);
        player.Jumping();
    }

    public override void End(PlayerController player)
    {
        base.End(player);
        player.EndJump();
    }
}

