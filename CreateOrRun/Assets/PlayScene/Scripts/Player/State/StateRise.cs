﻿ class StateRise : PlayerState
 {
     public override string Name => "Rise";
     public override void Check(PlayerController player)
     {
         player.State =
             !player.IsRising ? Fall :
             player.IsDoubleJump ? DoubleJump :  //二段ジャンプが必要なら二段ジャンプ
             player.IsGrounding ? Landing :      //着地したら着地状態
             KeepState;
     }

     public override void Update(PlayerController player)
     {
         player.MoveAir();
     }
     public override void FixedUpdate(PlayerController player)
     {
         player.ControlVelocity();
     }
}
