﻿//壁ジャンプ状態
class StateWallJump : StateFall
{
    public override string Name => "WallJump";

    public override void Check(PlayerController player)
    {
        player.State =
            player.IsWallJumping ? KeepState :  //壁ジャンプが必要なら壁ジャンプ
            player.IsDoubleJump ? DoubleJump :  //2段ジャンプが必要なら2段ジャンプ
            player.IsWallSliding ? WallSlide :  //壁方行に入力があるなら壁ずり
            !player.IsRising ? Fall :            //左右移動がある場合落下
            player.IsGrounding ? Landing :      //着地したら着地状態
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        player.SetAnimationId(5);
        player.StartWallJump();
    }

    public override void Update(PlayerController player)
    {
        player.WallJumping();
    }

    public override void End(PlayerController player)
    {
        player.EndWallJump();
    }
}
