﻿//落下状態
class StateFall : PlayerState
{
    public override string Name => "Fall";

    public override void Check(PlayerController player)
    {
        player.State =
            player.IsWallSliding && !player.IsRising ? WallSlide :  //壁方行に入力があるなら壁ずり
            player.IsDoubleJump ? DoubleJump :  //二段ジャンプが必要なら二段ジャンプ
            player.IsGrounding ? Landing :      //着地したら着地状態
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        player.IsTouchingFloor = false;
        player.SetAnimationId(6);
    }

    public override void Update(PlayerController player)
    {
        player.MoveAir();
    }
    public override void FixedUpdate(PlayerController player)
    {
        player.ControlVelocity();
    }
}