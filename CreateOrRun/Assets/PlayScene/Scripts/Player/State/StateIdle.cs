﻿//待機状態
class StateIdle : PlayerState
{
    public override string Name => "Idle";

    public override void Check(PlayerController player)
    {
        player.State =
            !player.IsGrounding ? Fall :    //地面にいないなら落下
            player.NeedIsJumping ? Jump :       //ジャンプキーが押されたらジャンプ
            player.IsMoving ? Run :         //左右キーが押されたら走る
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        player.SetAnimationId(0);
        player.StartIdle();
    }

    public override void Update(PlayerController player)
    {
        player.UpdateIdle();
    }

    public override void End(PlayerController player)
    {
        player.UseGravity = true;
        player.EndIdle();
    }
}
