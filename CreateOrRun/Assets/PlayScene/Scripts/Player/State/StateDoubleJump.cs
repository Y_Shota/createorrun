﻿//2段ジャンプ状態
class StateDoubleJump : StateJump
{
    public override string Name => "DoubleJump";

    public override void Check(PlayerController player)
    {
        player.State =
            player.IsWallSliding && !player.IsRising ? WallSlide :  //壁方行に入力があるなら壁ずり
            !player.IsRising ? Fall :
            player.IsGrounding && !player.IsRising ? Landing :         //着地したら待機状態
            player.NeedsJumpEnd ? Rise :
            KeepState;
    }

    public override void Start(PlayerController player)
    {
        base.Start(player);

        player.SetAnimationId(4);
        player.CanDoubleJump = false;
    }
}
