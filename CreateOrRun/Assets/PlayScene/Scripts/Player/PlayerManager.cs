using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

public class PlayerManager : SingletonMonoBehaviour<PlayerManager>
{
    [SerializeField] private int PlayerCount;
    [SerializeField] private GameObject Player;
    [SerializeField] private List<GameObject> ModelPrefabs;
       
    private readonly List<GameObject> _playerList = new List<GameObject>();
    private readonly List<PlayerController> _playerControllerList = new List<PlayerController>();

    public IReadOnlyList<GameObject> Players => _playerList.AsReadOnly();
    public IReadOnlyList<PlayerController> PlayerControllers => _playerControllerList.AsReadOnly();


    void Start()
    {
        //セレクトシーンの選択データを使用して初期化
        //プレイシーンから直で開始できるよう条件分岐で呼び出し
        GameObject selectedDataStorageObject = GameObject.Find("SelectedDataStorage");
        if (selectedDataStorageObject)
        {
            InstantiateFromSelectedDataStorage(selectedDataStorageObject);
            return;
        }

        //範囲内にクランプ
        if (PlayerCount < 1) PlayerCount = 1;
        if (PlayerCount > 4) PlayerCount = 4;

        CameraManager.Instance.CameraNum = PlayerCount;
        RankingManager.Instance.SetPlayerCount(PlayerCount);

        //プレイヤの人数分
        for (var i = 0; i < PlayerCount; ++i)
        {
            var player = Instantiate(Player, Vector3.zero, Quaternion.identity, transform);
            var playerController = player.GetComponent<PlayerController>();

            _playerList.Add(player);
            _playerControllerList.Add(playerController);

            //モデル生成
            var model = Instantiate(ModelPrefabs[i], player.transform);
            model.name = "Model";

            playerController.PlayerId = i;
            playerController.Pad = GamePad.all[i];

            //プレイヤ追従のカメラ
            CameraManager.Instance.CreatePlayerCamera(player.transform, i);
        }

        if (PlayerCount == 3)
        {
            CameraManager.Instance.CreateWholeCamera();
        }
    }

    private void InstantiateFromSelectedDataStorage(GameObject selectedDataStorageObject)
    {
        SelectedDataStorage selectedDataStorage = selectedDataStorageObject.GetComponent<SelectedDataStorage>();

        CameraManager.Instance.CameraNum = selectedDataStorage.PlayerCount;
        RankingManager.Instance.SetPlayerCount(selectedDataStorage.PlayerCount);

        //プレイヤの人数分
        for (var player_i = 0; player_i < selectedDataStorage.PlayerCount; ++player_i)
        {
            //プレイヤーの生成・登録
            var player = Instantiate(Player, Vector3.zero, Quaternion.identity, transform);
            var playerController = player.GetComponent<PlayerController>();
            _playerList.Add(player);
            _playerControllerList.Add(playerController);

            //個別の値の割り当て
            {
                //プレイヤーID
                playerController.PlayerId = player_i;

                //ゲームパッドID
                playerController.Pad = GamePad.all[selectedDataStorage.PlayerGamePadId(player_i)];

                //モデル生成
                var model = Instantiate(ModelPrefabs[selectedDataStorage.PlayerCharacter(player_i)], player.transform);
                model.name = "Model";

                //色
                //Material[] playerModelMaterials = model.GetComponentInChildren<Renderer>().materials;
                //Material[] newMaterials = new Material[playerModelMaterials.Length];
                //for (int material_i = 0; material_i < playerModelMaterials.Length; material_i++)
                //{
                //    Material material = new Material(playerModelMaterials[material_i].shader);

                //    material.CopyPropertiesFromMaterial(playerModelMaterials[material_i]);
                //    material.color = selectedDataStorage.PlayerColor(i);

                //    newMaterials[material_i] = material;
                //}

                int usedMaterialCount = 0;
                foreach (Transform child in model.transform)
                {
                    Renderer renderer = child.GetComponent<Renderer>();
                    if (renderer == null) continue;

                    Material[] materials = new Material[renderer.materials.Length];
                    for (int i = 0; i < renderer.materials.Length; i++)
                    {
                        materials[i] = selectedDataStorage.PlayerMaterials(player_i)[usedMaterialCount + i];
                    }
                    renderer.materials = materials;
                    usedMaterialCount += renderer.materials.Length;
                }
            }
            //プレイヤ追従のカメラ
            CameraManager.Instance.CreatePlayerCamera(player.transform, player_i);
        }

        if (selectedDataStorage.PlayerCount == 3)
        {
            CameraManager.Instance.CreateWholeCamera();
        }
    }

    public void SetStartPosition(Vector3 position)
    {
        position.z = 0;
        foreach (var player in _playerList)
        {
            player.transform.position = position;
        }
    }

    public void ActivateMiniMap()
    {
        foreach (var player in _playerList)
        {
            player.GetComponent<PlayerMapSpriter>().enabled = true;
        }
    }
}
