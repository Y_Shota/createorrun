using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Tooltip("速度 (x > 0)")]
    [SerializeField] private float Speed;
    [Tooltip("空中移動抵抗 (x > 1)")]
    [SerializeField] private float AirResistance;
    [Tooltip("ジャンプ力 (x > 0)")]
    [SerializeField] private float JumpPower;
    [Tooltip("ジャンプ時間 (x > 0)")]
    [SerializeField] private float JumpTime;
    [Tooltip("加速度上限 (x > 0)")]
    [SerializeField] private float MaxVelocity;
    [Tooltip("壁ずり速度 (x > 0)")]
    [SerializeField] private float WallSlideSpeed;
    [Tooltip("壁ずり可能時間 (x > 0)")]
    [SerializeField] private float WallSlideLimit;
    [Tooltip("壁ジャンプ力 (x > 0)")]
    [SerializeField] private float WallJumpPower;
    [Tooltip("壁ジャンプ保持時間 (x > 0)")]
    [SerializeField] private float WallJumpKeepTime;
    [Tooltip("壁ジャンプの角度(度)　(0 <= x < 360)")]
    [SerializeField] private float WallJumpAngle;

    [SerializeField] private bool ShouldDebugLog;
    [SerializeField] private bool ShouldDashEffect;
    
    [SerializeField] private float AfterImageSpeed;
    [SerializeField] private float PlayerScale;

    private static readonly IReadOnlyList<Vector3> RayDirections = new List<Vector3>{ Vector3.right, Vector3.left };

    private Rigidbody _rigid;       //剛体
    private List<FieldDetector> _fieldDetectors;
    private readonly Dictionary<string, float> _accelerationOrder = new Dictionary<string, float>();
    private readonly Dictionary<string, Coroutine> _accelerationOrderCoroutine = new Dictionary<string, Coroutine>();
    private Coroutine _idleCoroutine;
    private bool _isOperable = true;
    private Animator _animator;
    private GameObject _model;
    private float _rayDistance;
    private Vector3 _rayOffset;
    private readonly DefaultItem _defaultItem = new DefaultItem();
    private float _accelerationRate;
    private int _defermentWallJump;
    private float _jumpTimer;

    public GamePad Pad { get; set; }

    public  Vector3 PrevVelocity { get; set; }

    private ItemHandler _handler;   //保持アイテム操作用クラス
    public ItemHandler Handler
    {
        get => _handler;
        set
        {
            if(value.Name == "Empty")
            {
                //引数が空アイテムであればアイテムの保持を終える
                _handler    = value;
            }
            else if (_handler.Name == "Empty")
            {
                //アイテムを保持していなければ引数のアイテムを生成して保持
                _handler    = value;
            }
        }
    }

    private PlayerState _state;     //プレイヤの状態
    public PlayerState State        //プレイヤの状態アクセス
    {
        get => _state;
        set
        {
            //不変用
            if (PlayerState.KeepState == value) return;

            //終了処理を呼び出し、代入後開始処理
            _state?.End(this);
            _state = value;
            _state.Start(this);
        }
    }
    public Direction Dir { get; private set; } = Direction.Right; //向いている方向 

    public bool UseGravity
    {
        get => _rigid.useGravity;
        set => _rigid.useGravity = value;
    }

    public float TimeWallSlide { get; set; }                    //壁ずり経過時間
    public float TimeWallJump { get; set; }                     //壁ジャンプ経過時間

    public bool IsMovingRight => Pad.LStickX > 0; //右移動中か
    public bool IsMovingLeft => Pad.LStickX < 0;  //左移動中か
    public bool IsMoving => IsMovingRight || IsMovingLeft;          //移動中か
    public bool IsRising => _rigid.velocity.y > 0;                         //上昇中か
    public bool NeedIsJumping => Pad.ADown;       //ジャンプすべきか
    public bool NeedsJumpEnd => _jumpTimer >= JumpTime;
    public bool IsJumping => Pad.A && !NeedsJumpEnd;               //ジャンプ中か
    public bool IsDoubleJump => NeedIsJumping && CanDoubleJump;     //二段ジャンプ中か
    public bool IsWallJumping { get; set; }                         //壁ジャンプ中か
    public bool IsGrounding => IsTouchingFloor || (!IsTouchingWall && IsGroundingEdge); //接地中か
    public bool IsGroundingEdge { get; set; }                   //端っこ接地中か
    public bool IsTouchingWallRight { get; set; }               //右の壁に触れているか
    public bool IsTouchingWallLeft { get; set; }                //左の壁に触れているか
    public bool IsTouchingFloor { get; set; }                   //床に触れているか
    public bool IsTouchingWall => IsTouchingWallRight || IsTouchingWallLeft;                                    //壁に触れているか
    public bool IsWallSlidingRight => IsTouchingWallRight && (IsMovingRight || _defermentWallJump > 0) && TimeWallSlide < WallSlideLimit;   //右壁ずり状態か
    public bool IsWallSlidingLeft => IsTouchingWallLeft && (IsMovingLeft || _defermentWallJump > 0) && TimeWallSlide < WallSlideLimit;      //左壁ずり状態か
    public bool IsWallSliding => IsWallSlidingRight || IsWallSlidingLeft;                                       //壁ずり状態か
    public bool IsUsedBombLeft => Pad.LTrigger >= 1 && Pad.X;    //左方向に爆弾を使用したか
    public bool IsUsedBombRight => Pad.RTrigger >= 1 && Pad.X;   //右方向に爆弾を使用したか
    public bool IsUsedBlockLeft => Pad.LTrigger >= 1 && !Pad.X;    //左方向にブロックを使用したか
    public bool IsUsedBlockRight => Pad.RTrigger >= 1 && !Pad.X;   //右方向にブロックを使用したか
    public bool IsUsedItemLeft => Pad.LBDown;    //左方向にアイテムを使用したか
    public bool IsUsedItemRight => Pad.RBDown;   //右方向にアイテムを使用したか
    public bool IsRotationItem => Pad.YDown;     //アイテムを回転するか

    public bool CanDoubleJump { get; set; }                    //二段ジャンプ可能か

    public bool IsTookTimeItem { get; private set; }


    public int PlayerId { get; set; }   //プレイヤID

    //生成時
    private void Awake()
    {
        transform.localScale *= PlayerScale;
    }

    //開始処理
    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();

        _rigid = GetComponent<Rigidbody>();
        State = PlayerState.Idle;
        Handler = EmptyHandler.Instance;

        _fieldDetectors = GetComponentsInChildren<FieldDetector>().ToList();

        _model = GetComponentInChildren<Animator>().gameObject;

        var capsuleCollider = GetComponent<CapsuleCollider>();
        _rayDistance = capsuleCollider.radius * 1.01f;
        _rayOffset = Vector3.down * capsuleCollider.height * 0.5f * PlayerScale;
    }

    //前更新処理
    private void FixedUpdate()
    {
        State.FixedUpdate(this);
    }

    //更新処理
    private void Update()
    {
        if (ShouldDebugLog) Debug.Log(PlayerId.ToString() + State);
        
        //フィールド検知状態を更新
        foreach (var fieldDetector in _fieldDetectors)
        {
            fieldDetector.UpdateDetectedState();
        }
        UpdateDirection();
        CalculateVelocityX();
        State.Check(this);

        EnchantRankBoost();

        //操作可能か
        if (!_isOperable) return;

        State.Update(this);

        Handler.Update(this);
        _defaultItem.Update(this);
    }

    //後更新処理
    private void LateUpdate()
    {
        PrevVelocity = _rigid.velocity;
    }

    //衝突判定
    private void OnCollisionEnter(Collision collision)
    {
        if (Vector3.Angle(collision.contacts[0].normal,Vector3.up) < 40)
        {
            Landing();
        }
    }

    private void CalculateVelocityX()
    {
        _accelerationRate = _accelerationOrder.Values.Aggregate(1f, (current, rate) => current * rate);
        

        if (_accelerationOrder.Count <= 0) return;

        var particle =
            _accelerationRate > 1 ? ParticleManager.Instance.PlayBuffEffect(transform.position, Quaternion.identity, 1) :
            _accelerationRate < 1 ? ParticleManager.Instance.PlayDebuffEffect(transform.position, Quaternion.identity, 1) :
            null;
        
        if (particle != null) particle.transform.parent = transform;
    }

    private void EnchantRankBoost()
    {
        var rank = RankingManager.Instance.GetRank(PlayerId);
        if (rank == 0 || rank != PlayerManager.Instance.Players.Count -1) return;
        
        ChangeVelocityRateOrder("RankBoost", 1.2f, 0.5f, false);
    }

    private void UpdateDirection()
    {
        if (IsGrounding)
        {
            Dir = Math.Sign(Pad.LStickX) switch
            {
                -1 => Direction.Left,
                1 => Direction.Right,
                _ => Dir
            };
        }

        _model.transform.localScale = new Vector3(1, 1, Dir.Sign);
    }

    public void SetAnimationId(int id)
    {
        if (_animator == null) return;

        _animator.SetInteger("StateId", id);
    }

    public void RespawnCheckPoint()
    {
        var checkPointId = RankingManager.Instance.GetCurrentCheckPoint(PlayerId);
        var checkPoint = CheckPointManager.Instance.CheckPointsMap[checkPointId].First();
        var respawnPos = checkPoint.transform.position;
        respawnPos.z = 0;
        transform.position = respawnPos;
    }

    public void StartIdle()
    {
        _idleCoroutine = StartCoroutine(nameof(StartIdleImpl));
    }

    public void UpdateIdle()
    {
        if (_rigid.velocity.magnitude > 0 && _idleCoroutine == null) _idleCoroutine = StartCoroutine(nameof(StartIdleImpl));
    }

    public void EndIdle()
    {
        StopCoroutine(nameof(StartIdleImpl));
    }

    private IEnumerator StartIdleImpl()
    {
        const float limit = 0.5f;

        var timer = 0f;
        var startVelocity = _rigid.velocity;
        for (;;)
        {
            var rate = timer / limit;
            //Debug.Log(_rigid.velocity + ":" + rate);
            _rigid.velocity = Vector3.Lerp(startVelocity, Vector3.zero, rate);

            if (rate >= 1)
            {
                _idleCoroutine = null;
                UseGravity = false;
                yield break;
            }

            yield return null;

            timer += Time.deltaTime;
        }
    }

    //着地時
    public void Landing()
    {
        CanDoubleJump = true;
        TimeWallSlide = 0;
        _rigid.velocity = new Vector3(PrevVelocity.x, 0);
    }

    //地面での移動時に呼ばれる
    public void FixedMoveGround()
    {
        var forceX = Pad.LStickX * Time.deltaTime * 30;

        if (IsTouchingWallRight && forceX > 0) return;
        if (IsTouchingWallLeft && forceX < 0) return;

        forceX *= Speed * _accelerationRate;

        var force = new Vector3(forceX, 0);
        
        //前後へレイを飛ばし坂があるなら法線を取得し移動方向を調整
        foreach (var direction in RayDirections)
        {
            var ray = new Ray(transform.position + _rayOffset, direction);
            Debug.DrawRay(ray.origin, ray.direction * _rayDistance, Color.red, 0.02f, false);
            if (!Physics.Raycast(ray, out var hit, _rayDistance, LayerMask.GetMask("Slope"))) continue;
            if (Vector3.Angle(Vector3.up, hit.normal.normalized) > 50f) continue;

            force = Quaternion.FromToRotation(Vector3.up, hit.normal.normalized) * force;
            break;
        }
        _rigid.velocity = force;

        //ダッシュエフェクト
        if (!ShouldDashEffect || _rigid.velocity.magnitude < 1f) return;
        ParticleManager.Instance.PlayDashEffect(gameObject.transform.position + _rayOffset, Quaternion.Euler(0, 45, 45), _rigid.velocity.magnitude * 0.3f);
    } 

    //空中での移動時に呼ばれる
    public void MoveAir()
    {
        var forceX = Pad.LStickX * Time.deltaTime * 30;

        if (IsTouchingWallRight && forceX > 0) return;
        if (IsTouchingWallLeft && forceX < 0) return;

        forceX *= Speed * _accelerationRate / AirResistance;

        _rigid.AddForce(forceX, 0, 0, ForceMode.Impulse);
    }

    //ジャンプする必要がある時に呼ばれる
    public void StartJump()
    {
        _rigid.velocity = new Vector3(_rigid.velocity.x, JumpPower);
        //ジャンプのエフェクト
        ParticleManager.Instance.PlayJumpEffect(gameObject.transform.position, Quaternion.identity, 1f);
    }

    public void Jumping()
    {
        if (IsJumping) return;

        _rigid.velocity -= new Vector3(0, 9.8f * Time.deltaTime);

        _jumpTimer += Time.deltaTime;
    }

    public void EndJump()
    {
        _jumpTimer = 0;
    }

    public void StartWallSlide()
    {
        _defermentWallJump = 10;

        if (IsWallSlidingLeft)
        {
            Dir = Direction.Left;
        }
        else if (IsWallSlidingRight)
        {
            Dir = Direction.Right;
        }
    }

    //壁ずり中に呼ばれる
    public void FixedUpdateWallSlide()
    {
        TimeWallSlide += Time.deltaTime;
        _rigid.velocity = new Vector3(0, -WallSlideSpeed, 0);
    }

    public void UpdateWallSlide()
    {
        if (IsTouchingWallLeft && !IsMovingLeft || IsTouchingWallRight && !IsMovingRight)
        {
            --_defermentWallJump;
        }

        //壁ずりしはじめて0.1sでエフェクト(すぐに出さないため)
        if (TimeWallSlide <= 0.1f) return;

        //右か左かでずらす
        var effectPos = gameObject.transform.position + new Vector3(Dir.Sign * 0.5f, 0.5f);

        //壁ずりエフェクト
        ParticleManager.Instance.PlayWallSmokeEffect(effectPos, Quaternion.identity, 1f);
    }

    public void EndWallSlide()
    {
        _defermentWallJump = 0;
    }

    //壁ジャンプする必要がある時に呼ばれる
    public void StartWallJump()
    {
        IsWallJumping = true;
        TimeWallSlide = 0;

        var wallJumpPower = (JumpPower + WallJumpPower) / 2;

        var jumpVector = Quaternion.Euler(0, 0, WallJumpAngle) * Vector3.right;
        if (IsTouchingWallLeft)
        {
            Dir = Direction.Right;
            _rigid.velocity = jumpVector * wallJumpPower;
        }
        else if (IsTouchingWallRight)
        {
            Dir = Direction.Left;
            _rigid.velocity = Vector3.Scale(jumpVector, new Vector3(-1, 1)) * wallJumpPower;
        }

        //壁じゃんエフェクト
        ParticleManager.Instance.PlayWallJumpEffect(gameObject.transform.position, Quaternion.identity, 1f);
    }
    
    //壁ジャンプ中に呼ばれる
    public void WallJumping()
    {
        TimeWallJump += Time.deltaTime;

        if (!IsWallJumping) MoveAir();

        if (TimeWallJump < WallJumpKeepTime) return;

        IsWallJumping = false;
    }

    public void EndWallJump()
    {
        TimeWallJump = 0;
    }

    //加速度を調整する時に呼ばれる
    public void ControlVelocity()
    {
        var x = 
            Mathf.Abs(_rigid.velocity.x) > _accelerationRate * MaxVelocity ? Mathf.Sign(_rigid.velocity.x) * _accelerationRate * MaxVelocity :
            _rigid.velocity.x;
        var y = 
            State == PlayerState.Fall && _rigid.velocity.y > 0 ? 0 :
            _rigid.velocity.y;

        _rigid.velocity = new Vector3(x, y);
    }

    public void ChangeVelocityOrder(Vector3 force, float freezeTime = 0)
    {
        _rigid.velocity = force;

        DisableOperateOrder(freezeTime);
    }
    

    //移動速度を外部から変更
    public void ChangeVelocityRateOrder(string key, float rate, float second = 1, bool playSe = true)
    {
        var coroutine = StartCoroutine(ChangeVelocityRateOrderImpl());
        if (_accelerationOrderCoroutine.ContainsKey(key))
        {
            StopCoroutine(_accelerationOrderCoroutine[key]);
            _accelerationOrderCoroutine[key] = coroutine;
        }
        else
        {
            _accelerationOrderCoroutine.Add(key, coroutine);
        }

        IEnumerator ChangeVelocityRateOrderImpl()
        {
            if (_accelerationOrder.ContainsKey(key))
            {
                _accelerationOrder[key] = rate;
            }
            else
            {
                _accelerationOrder.Add(key, rate);

                //効果音
                if (playSe) SEManager.Instance.PlaySE("Accelerator");
            }
            yield return new WaitForSeconds(second);

            _accelerationOrder.Remove(key);
        }
    }

    public void DisableOperateOrder(float second)
    {
        StartCoroutine(DisableOperateOrderImpl()); 
        
        IEnumerator DisableOperateOrderImpl()
        {
            _isOperable = false;

            yield return new WaitForSeconds(second);

            _isOperable = true;
        }
    }
    

    public void FreezeOrder(float second)
    {
        StartCoroutine(FreezeOrderImpl());

        IEnumerator FreezeOrderImpl()
        {
            enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;

            yield return new WaitForSeconds(second);

            enabled = true;
            GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    public void StopAnimationOrder(float second)
    {
        StartCoroutine(StopAnimationOrderImpl());

        IEnumerator StopAnimationOrderImpl()
        {
            _animator.SetFloat("Speed", 0);

            yield return new WaitForSeconds(second);

            _animator.SetFloat("Speed", 1);
        }
    }

    public void StartUseAnimation(Direction dir)
    {
        StartCoroutine(UseCor(dir == Dir ? "UseForward" : "UseBack"));

        IEnumerator UseCor(string flagName)
        {
            _animator.SetBool(flagName, true);
            yield return null;
            yield return null;
            yield return null;

            for (; ; )
            {
                var info = _animator.GetCurrentAnimatorStateInfo(1);
                if (info.normalizedTime >= 1 && !info.IsName("Idle"))
                {
                    _animator.SetBool(flagName, false);
                    yield break;
                }
                yield return null;
            }
        }
    }

    public void TakeTimeItem(float second)
    {
        StartCoroutine(TakeTimeItemCor());

        FreezeOrder(second);
        DisableOperateOrder(second);
        StopAnimationOrder(second);

        IEnumerator TakeTimeItemCor()
        {
            IsTookTimeItem = true;
            SEManager.Instance.PlayLoopSE("TimeStop", 1);
            yield return new WaitForSeconds(second);
            IsTookTimeItem = false;
            SEManager.Instance.SetLoopSE(false);
        }

    }
}
