using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private List<float> DistanceList = new List<float>() {0.7f, 1, 1.3f};
    
    private int _distIndex = 1;
    private GamePad _pad;
    private Vector3 _basePosition;

    private void Start()
    {
        if (PlayerManager.Instance.Players.Count == 2) transform.localPosition *= 1.5f;
        _basePosition = transform.localPosition;

        var player = transform.parent.GetComponent<PlayerController>();
        _pad = player.Pad;
    }

    private void Update()
    {
        if (!_pad.RStickPressDown) return;

        StopAllCoroutines();
        StartCoroutine(ChangeDistanceCor());

        IEnumerator ChangeDistanceCor()
        {
            const float time = 0.3f;
            var timer = 0f;

            _distIndex = (_distIndex + 1) % DistanceList.Count;

            var from = transform.localPosition;
            var to = _basePosition * DistanceList[_distIndex];

            for (;;)
            {
                transform.localPosition = Vector3.Lerp(from, to, timer / time);

                if (timer >= time) yield break;

                timer += Time.deltaTime;
                yield return null;
            }
        }
    }

}
