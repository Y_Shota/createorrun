using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : SingletonMonoBehaviour<CameraManager>
{
    [SerializeField] private GameObject CameraPrefab;
    [SerializeField] private GameObject CameraDefaultPrefab;
    public int CameraNum { get; set; }
    public List<Camera> Cameras => _cameras;
    public List<Rect> CameraRects => _viewportsList[CameraNum - 1];

    private List<Camera> _cameras;
    private List<List<Rect>> _viewportsList;
    private Camera _wholeCamera;
    

    protected override void Awake()
    {
        base.Awake();

        _cameras = new List<Camera>();
        _viewportsList = new List<List<Rect>>()
        {
            //一人
            new List<Rect>
            {
                new Rect(0, 0, 1, 1)
            },
            //二人
            new List<Rect>
            {
                new Rect(0, 0, 0.5f, 1),
                new Rect(0.5f, 0, 0.5f, 1)
            },
            //三人
            new List<Rect>
            {
                new Rect(0, 0.5f, 0.5f, 0.5f),
                new Rect(0.5f, 0.5f, 0.5f, 0.5f),
                new Rect(0, 0, 0.5f, 0.5f)
            },
            //四人
            new List<Rect>
            {
                new Rect(0, 0.5f, 0.5f, 0.5f),
                new Rect(0.5f, 0.5f, 0.5f, 0.5f),
                new Rect(0, 0, 0.5f, 0.5f),
                new Rect(0.5f, 0, 0.5f, 0.5f)
            }
        };
    }

    public void CreatePlayerCamera(Transform playerTransform, int cameraId)
    {
        var cameraObj = Instantiate(CameraPrefab, playerTransform);
        var camera = cameraObj.GetComponent<Camera>();
        _cameras.Add(camera);
        camera.rect = _viewportsList[CameraNum - 1][cameraId];
    }

    public void CreateWholeCamera()
    {
        var cameraObj = Instantiate(CameraDefaultPrefab, transform);
        _wholeCamera = cameraObj.GetComponent<Camera>();
        _wholeCamera.rect = _viewportsList[3][3];
    }

    public void SetPositionWholeCamera(Vector2Int size)
    {
        if (_wholeCamera == null) return;
        
        _wholeCamera.transform.position = new Vector3(size.x / 2f, size.y / 2f, -(size.x > size.y ? size.x : size.y) * 0.8f);
    }


    //UI用カメラ
    public void CreateUiCamera(Transform parentTransform, int cameraId)
    {
        var cameraObj = Instantiate(CameraPrefab, new Vector3(0, 1, -10), Quaternion.identity);
        cameraObj.transform.SetParent(parentTransform);
        var camera = cameraObj.GetComponent<Camera>();

        camera.rect = _viewportsList[CameraNum - 1][cameraId];
        camera.clearFlags = CameraClearFlags.Depth;
        camera.cullingMask = LayerMask.NameToLayer("UI"); //UI
        camera.depth = 1;
    }
}
