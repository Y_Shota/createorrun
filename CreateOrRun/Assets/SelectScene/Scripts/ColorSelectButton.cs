using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSelectButton : MonoBehaviour
{
    [Tooltip("色変更で使用するテクスチャー")]
    [SerializeField] private List<Color> ColorList;

    [Serializable]
    public class ColorTextures
    {
        public List<Texture> colorTextures;
    }

    [Serializable]
    public class CharacterTextures
    {
        public string name;
        public List<ColorTextures> charactorTextures;
    }
    //[モデル].charactorTextures[色].[部位]
    [Tooltip("色変更で使用するテクスチャー")]
    [SerializeField] private List<CharacterTextures> CharacterTexturesList;

    //現在選択している番号
    int _currentSelect;

    public Color SelectedColor => ColorList[_currentSelect];

    public List<Texture> SelectedTextures => CharacterTexturesList[transform.parent.GetComponent<PlayerSelectorDirector>().SelectedCharactorId].charactorTextures[_currentSelect].colorTextures;

    void Start()
    {
        _currentSelect                      = 0;
        GetComponent<Button>().image.color  = ColorList[_currentSelect];
    }

    void Update()
    {
        //入力を受け付けているかを確認しする
        PlayerSelectorDirector playerSelectorDirector = transform.parent.GetComponent<PlayerSelectorDirector>();
        if (!playerSelectorDirector.IsSelected(gameObject)) return;
        if (playerSelectorDirector.IsWaiting) return;

        //左右方向の入力に対する処理
        if (playerSelectorDirector.IsSelectOnLeft) ChangeSelectedColor(Direction.Left);
        if (playerSelectorDirector.IsSelectOnRight) ChangeSelectedColor(Direction.Right);
    }

    void ChangeSelectedColor(Direction direction)
    {
        int newSelect = _currentSelect + direction.Sign;  //新しく選択される要素
        int lastCount = (int)ColorList.Count - 1;         //配列の最終要素

        //配列サイズから出ていないかチェック
        if (newSelect < 0)          newSelect = lastCount;
        if (newSelect > lastCount)  newSelect = 0;

        //選択色を変更
        GetComponent<Button>().image.color = ColorList[newSelect];
        _currentSelect = newSelect;

        //色の変更をディレクターに伝える
        transform.parent.GetComponent<PlayerSelectorDirector>().IsChangedColor = true;

        //Debug.Log("ID:" + transform.parent.GetComponent<PlayerSelectorDirector>().Id.ToString() + "  Select:" + _currentSelect.ToString());
    }
}