using System.Collections.Generic;
using UnityEngine;

public class CharactorSelectButton : MonoBehaviour
{
    [Tooltip("選択可能なキャラクター")]
    [SerializeField] private List<GameObject> CharactorPrefabList;

    //現在選択している番号
    int _currentSelect;

    //選択されているキャラクター
    GameObject _selectCharactor;

    public int SelectedId => _currentSelect;    //選択されているキャラクターのID

    public List<Material> SelectedMaterials { get; private set; }

    void Start()
    {
        SelectedMaterials = new List<Material>();
    }

    void Update()
    {
        if (!_selectCharactor) InstantiateSelectCharactor();

        //入力を受け付けているかを確認しする
        PlayerSelectorDirector playerSelectorDirector = transform.parent.GetComponent<PlayerSelectorDirector>();
        if (!playerSelectorDirector.IsSelected(gameObject)) return;
        if (playerSelectorDirector.IsWaiting) return;

        //左右方向の入力に対する処理
        if (playerSelectorDirector.IsSelectOnLeft)  ChangeSelectedCharactor(Direction.Left);
        if (playerSelectorDirector.IsSelectOnRight) ChangeSelectedCharactor(Direction.Right);
    }

    void ChangeSelectedCharactor(Direction direction)
    {
        int newSelect = _currentSelect + direction.Sign;  //新しく選択される要素
        int lastCount = CharactorPrefabList.Count - 1;    //配列の最終要素

        //配列サイズから出ていないかチェック
        if (newSelect < 0)          newSelect = lastCount;
        if (newSelect > lastCount)  newSelect = 0;

        //選択キャラクターを変更
        _currentSelect = newSelect;
        Destroy(_selectCharactor);
        InstantiateSelectCharactor();
    }

    void InstantiateSelectCharactor()
    {
        _selectCharactor = Instantiate(CharactorPrefabList[_currentSelect]);
        _selectCharactor.transform.position     = new Vector3(transform.position.x, transform.position.y - 40f, transform.position.z - 20f);
        _selectCharactor.transform.localScale   = new Vector3(40f, 40f, 40f);
        _selectCharactor.transform.rotation     = Quaternion.Euler(0, 180f, 0);
        _selectCharactor.transform.SetParent(transform);
        SetSelectedCharactorColorTexture();
    }

    //rgbをマテリアル色に加算し色変更
    public void SetSelectedCharactorColor(Color color)
    {
        //自分と子供のマテリアルの色を変更（孫以降は見てない）
        ChangeMaterialColor(_selectCharactor.transform);

        foreach (Transform child in _selectCharactor.transform)
        {
            ChangeMaterialColor(child);
        }


        void ChangeMaterialColor(Transform materialTransform)
        {
            Renderer renderer = materialTransform.GetComponent<Renderer>();
            if (renderer == null) return;

            materialTransform.GetComponent<Renderer>().materials = GetChangedColorMaterials(renderer.materials);
        }

        Material[] GetChangedColorMaterials(Material[] playerModelMaterials)
        {
            Material[] newMaterials = new Material[playerModelMaterials.Length];
            for (int material_i = 0; material_i < playerModelMaterials.Length; material_i++)
            {
                Material material = new Material(playerModelMaterials[material_i].shader);

                material.CopyPropertiesFromMaterial(playerModelMaterials[material_i]);
                material.color = color;

                newMaterials[material_i] = material;
            }

            return newMaterials;
        }
    }

    //マテリアルごとの使用テクスチャを色に対応するテクスチャに変更
    public void SetSelectedCharactorColorTexture()
    {
        //効果音
        SelectSceneSEManager.Instance.PlaySelectSE();

        List<Texture> colorTextures = transform.parent.GetComponent<PlayerSelectorDirector>().SelectedTextures;
        int usedTextureCount = 0;

        SelectedMaterials.Clear();
        foreach (Transform child in _selectCharactor.transform)
        {
            ChangeMaterialColor(child);
        }


        //1オブジェクトについているマテリアルをまとめて変更
        void ChangeMaterialColor(Transform materialTransform)
        {
            Renderer renderer = materialTransform.GetComponent<Renderer>();
            if (renderer == null) return;

            Material[] newMaterials = new Material[renderer.materials.Length];

            for (int i = 0; i < renderer.materials.Length; i++)
            {
                Material material = new Material(renderer.materials[i].shader);
                material.CopyPropertiesFromMaterial(renderer.materials[i]);
                material.mainTexture = colorTextures[usedTextureCount + i];
                newMaterials[i] = material;
                SelectedMaterials.Add(material);
            }
            usedTextureCount += renderer.materials.Length;
            renderer.materials = newMaterials;
        }
    }
}
