using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectedDataStorage : MonoBehaviour
{
    //プレイヤーの選択項目
    public struct PlayerData
    {
        public int              GamePadId { get; set; }     //ゲームパッドのID
        public int              CharacterID { get; set; }   //選択されているキャラクター
        public Color            Color { get; set; }         //色
        public List<Material>   Materials { get; set; }     //使用マテリアル

        public PlayerData(int gamePadId, int characterId, Color color, List<Material> materials)
        {
            GamePadId       = gamePadId;
            CharacterID     = characterId;
            Color           = color;
            Materials       = materials;
        }
    }

    [Tooltip("オブジェクトが生存するシーン名")]
    [SerializeField] private List<string> MoveToSceneNames;

    //プレイヤーの選択データのリスト
    List<PlayerData> _playerDatas;

    //選択されたステージのID
    int _stageId;

    public int              PlayerCount             => _playerDatas.Count;              //プレイヤー数
    public int              PlayerGamePadId(int id) => _playerDatas[id].GamePadId;      //ゲームパッドのID
    public int              PlayerCharacter(int id) => _playerDatas[id].CharacterID;    //選択されているキャラクター(0:兎, 1:馬, 2:伽藍鳥, 3:獅子)
    public Color            PlayerColor(int id)     => _playerDatas[id].Color;          //選択されている色
    public List<Material>   PlayerMaterials(int id) => _playerDatas[id].Materials;      //選択されているマテリアルの配列
    public PlayerData       Data(int id)            => _playerDatas[id];                //選択されているデータを纏めた構造体
    public int              StageId                 => _stageId;                        //選択されているステージ

    void Start()
    {
        //シーン遷移でオブジェクトを破棄しない
        DontDestroyOnLoad(gameObject);

        //プレイシーンスタートしたときにバグらないようデフォ値設定
        _playerDatas = new List<PlayerData>();

        _stageId = 0;

        SceneManager.sceneLoaded += MoveGameObjectToScene;

        //オブジェクトをシーン間で移動させる
        void MoveGameObjectToScene(Scene nextScene, LoadSceneMode mode)
        {
            //次のシーンにこのオブジェクトを残すかを決定
            foreach (string moveToSceneName in MoveToSceneNames)
            {
                if (nextScene.name == moveToSceneName) return;
            }

            //残さない
            SceneManager.sceneLoaded -= MoveGameObjectToScene;
            Destroy(gameObject);
        }
    }

    //選択されたデータの受け取り
    public void FetchSelectedDatas(SelectSceneManager selectSceneManager)
    {
        _playerDatas    = selectSceneManager.SelectedPlayerDatas;
        _stageId        = selectSceneManager.SelectedStageId;
    }
}
