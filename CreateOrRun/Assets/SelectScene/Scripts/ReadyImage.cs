using UnityEngine;
using UnityEngine.UI;

public class ReadyImage : MonoBehaviour
{
    public enum State { SELECTED, DETERMINED, MAX};

    [Tooltip("選択表示のスプライト")]
    [SerializeField] private Sprite[] ReadyImageList;

    void Start()
    {
        gameObject.GetComponent<Image>().sprite = ReadyImageList[(int)State.SELECTED];
    }

    void Update()
    {
        
    }

    public void ChangeState(State state)
    {
        //効果音
        SelectSceneSEManager.Instance.PlayDecisionSE();

        gameObject.GetComponent<Image>().sprite = ReadyImageList[(int)state];
    }
}
