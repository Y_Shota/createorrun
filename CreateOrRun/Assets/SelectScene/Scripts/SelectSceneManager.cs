using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectSceneManager : MonoBehaviour
{
    [Tooltip("セレクター")]
    [SerializeField] private GameObject PlayerSelectorPrefab;
    [Tooltip("プレイヤーの最大人数")]
    [SerializeField] private int PlayerCountMax;
    [Tooltip("次のシーン名")]
    [SerializeField] private string NextSceneName;
    [Tooltip("セレクターの位置")]
    [SerializeField] private List<Vector3> SelectorPositionList;

    List<int> _registeredControllerIdList;

    void Start()
    {
        //セレクターの生成
        for (int i = 0; i < PlayerCountMax; i++) 
        {
            if (SelectorPositionList.Count <= i) return;

            GameObject newGameObject = Instantiate(PlayerSelectorPrefab);

            newGameObject.transform.position = SelectorPositionList[i];
            newGameObject.GetComponent<PlayerSelectorDirector>().Id = i;
            newGameObject.transform.SetParent(gameObject.transform);
        }

        _registeredControllerIdList = new List<int>();
    }

    void Update()
    {
        //新規プレイヤーの登録がされたか
        for (int id_i = 0; id_i < PlayerCountMax; id_i++)
        {
            if (!GamePad.all[id_i].ADown) continue;
            if (_registeredControllerIdList.Contains(id_i)) continue;

            //新規プレーヤーを登録
            PlayerSelectorDirector playerSelectorDirector = transform.GetChild(_registeredControllerIdList.Count).gameObject.GetComponent<PlayerSelectorDirector>();
            playerSelectorDirector.Id = id_i;
            playerSelectorDirector.ActivateSelector();
            _registeredControllerIdList.Add(id_i);
        }


        //全プレイヤーが選択を終了したかを確認
        int determinedCount = 0;
        int determinedMax   = _registeredControllerIdList.Count;
        if (determinedMax <= 0) return;
        for (int i = 0; i < determinedMax; i++)
        {
            PlayerSelectorDirector selectorDirector = gameObject.transform.GetChild(i).gameObject.GetComponent<PlayerSelectorDirector>();
            if (selectorDirector.IsDetermined) determinedCount++;
        }
        if (determinedCount < determinedMax) return;

        //選択されたデータを保持用のオブジェクトに持たせる
        GameObject.Find("SelectedDataStorage").GetComponent<SelectedDataStorage>().FetchSelectedDatas(this);
        enabled = false;
        FadeManager.Instance.StartFadeOut(NextSceneName, 0.5f);
        //Title、SelectシーンのBGM削除
        SceneManager.MoveGameObjectToScene(GameObject.Find("TestBGMDirector"), SceneManager.GetActiveScene());
    }

    //プレイヤーごとに選択されているデータのリストを取得
    public List<SelectedDataStorage.PlayerData> SelectedPlayerDatas
    {
        get
        {
            List<SelectedDataStorage.PlayerData> playerDatas = new List<SelectedDataStorage.PlayerData>();

            //登録されているプレイヤーの選択情報をリストにまとめる
            foreach (Transform child in transform)
            {
                PlayerSelectorDirector selector = child.gameObject.GetComponent<PlayerSelectorDirector>();


                if (!selector.IsRegistered) continue;

                SelectedDataStorage.PlayerData playerData   = selector.SelectedPlayerData;
                List<Material>                 materials    = new List<Material>();
                for (int material_i = 0; material_i < playerData.Materials.Count; material_i++)
                {
                    Material material = new Material(playerData.Materials[material_i].shader);
                    material.CopyPropertiesFromMaterial(playerData.Materials[material_i]);
                    materials.Add(material);
                }
                playerData.Materials = materials;

                playerDatas.Add(playerData);
            }

            //色の被りは明暗で区別
            for (int i = playerDatas.Count - 1; i > 0; i--)
            {
                SelectedDataStorage.PlayerData playerData = playerDatas[i];

                for (int check_i = 0; check_i < i; check_i++) 
                {
                    if (playerData.CharacterID != playerDatas[check_i].CharacterID) continue;
                    if (playerData.Materials[0].mainTexture != playerDatas[check_i].Materials[0].mainTexture) continue;

                    foreach(Material material in playerData.Materials)
                    {
                        material.color *= 0.7f;
                    }
                }
            }

            return playerDatas;
        }
    }

    //選択されたステージを取得
    public int SelectedStageId
    {
        get
        {
            //< stageId, count >
            Dictionary<int, int> selectedCounterDictionary = new Dictionary<int, int>();

            //プレイヤーごとに選択しているステージIDを取得し、IDごとに投票数をまとめる
            foreach (Transform child in transform)
            {
                PlayerSelectorDirector selector = child.gameObject.GetComponent<PlayerSelectorDirector>();
                if (!selector.IsDetermined) continue;

                int stageId = selector.SelectedStageId;

                //ステージIDのカウントを増やす
                if (selectedCounterDictionary.ContainsKey(key: stageId) == false)
                {
                    selectedCounterDictionary.Add(stageId, 0);
                    continue;
                }

                selectedCounterDictionary[selector.SelectedStageId]++;
            }

            //選択人数でソートし、最多得票のステージを取得する
            List<int> topSelectedList = new List<int>();
            int topSelectedMax = 0;
            foreach (var selectedCounter in selectedCounterDictionary)
            {
                int selectedCount = selectedCounter.Value;

                if (selectedCount < topSelectedMax) continue;

                if (selectedCount > topSelectedMax)
                {
                    topSelectedList.Clear();
                    topSelectedMax = selectedCount;
                }

                topSelectedList.Add(selectedCounter.Key);
            }

            //最多得票のステージIDを決定
            //最多得票ステージが複数ある場合は最多得票ステージの中からランダム
            int determinedStageId;
            if (topSelectedList.Count <= 1) determinedStageId = topSelectedList[0];
            else                            determinedStageId = topSelectedList[Random.Range(0, topSelectedList.Count)];

            //決定されたステージIDがランダムステージのIDであればランダムでステージを選択する
            StageSelectButton stageSelectButton = transform.GetChild(0).Find("StageSelectButton").GetComponent<StageSelectButton>();
            if (determinedStageId == stageSelectButton.RandomStageId) return stageSelectButton.RandomSelectStageId;

            //決定されたステージIDを返す
            return determinedStageId;
        }
    }
}
