using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SelectSceneSEManager : SingletonMonoBehaviour<SelectSceneSEManager>
{
    [Tooltip("セレクト音")]
    [SerializeField] private AudioClip SelectSE;
    [Tooltip("決定音")]
    [SerializeField] private AudioClip DecisionSE;
    private AudioSource _audioSource;


    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = ConfigManager.Instance.VolumeSE;
    }

    void Update()
    {
    }

    public void PlaySelectSE()
    {
        _audioSource.PlayOneShot(SelectSE);
    }

    public void PlayDecisionSE()
    {
        _audioSource.PlayOneShot(DecisionSE);
    }
}
