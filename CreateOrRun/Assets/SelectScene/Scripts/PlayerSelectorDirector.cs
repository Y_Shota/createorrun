using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectorDirector : MonoBehaviour
{
    //選択項目
    enum ButtonItem
    {
        CHARACTOR = 0,
        COLOR,
        STAGE,
        MAX,
    }

    [Tooltip("選択に使用されるスティックの角度")]
    [SerializeField] private float SelectMoveDefeat;
    [Tooltip("待ち時間")]
    [SerializeField] private float SelectMoveWaitTime;
    [Tooltip("待ち時間")]
    [SerializeField] private List<Vector3> SelectArrowPositionList;

    Button[]    _selectButtons = new Button[(int)ButtonItem.MAX]; //選択ボタンのリスト

    int         _currentSelect; //選択されているボタンの要素番号
    float       _deltaTime;     //選択変更時からのデルタタイム
    bool        _isDetermined;  //選択完了済みか
    bool        _isRegistered;  //登録済みか
    bool        _isWaiting;     //待ち時間中か

    public int              SelectedCharactorId => _selectButtons[(int)ButtonItem.CHARACTOR].GetComponent<CharactorSelectButton>().SelectedId;
    public Color            SelectedColor       => _selectButtons[(int)ButtonItem.COLOR].GetComponent<ColorSelectButton>().SelectedColor;
    public List<Material>   SelectedMaterials   => _selectButtons[(int)ButtonItem.CHARACTOR].GetComponent<CharactorSelectButton>().SelectedMaterials;
    public List<Texture>    SelectedTextures    => _selectButtons[(int)ButtonItem.COLOR].GetComponent<ColorSelectButton>().SelectedTextures;
    public int              SelectedStageId     => _selectButtons[(int)ButtonItem.STAGE].GetComponent<StageSelectButton>().SelectedId;
    public SelectedDataStorage.PlayerData SelectedPlayerData => new SelectedDataStorage.PlayerData(Id, SelectedCharactorId, SelectedColor, SelectedMaterials);

    public int      Id { get; set; }
    public bool     IsChangedColor { get; set; }
    public bool     IsSelectOnUp    => (GamePad.all[Id].LStickY > SelectMoveDefeat)  & !_isDetermined;
    public bool     IsSelectOnDown  => (GamePad.all[Id].LStickY < -SelectMoveDefeat) & !_isDetermined;
    public bool     IsSelectOnLeft  => (GamePad.all[Id].LStickX < -SelectMoveDefeat) & !_isDetermined;
    public bool     IsSelectOnRight => (GamePad.all[Id].LStickX > SelectMoveDefeat)  & !_isDetermined;
    public bool     IsSelectOnSpace => (GamePad.all[Id].ADown) & !_isDetermined;
    public bool     IsSelectOnStart => GamePad.all[Id].StartDown || Input.GetKeyDown(KeyCode.Return);
    public bool     IsDetermined    => _isDetermined; 
    public bool     IsRegistered    => _isRegistered;
    public bool     IsWaiting       => _isWaiting;
    public bool     IsSelected(GameObject gameObject) => _selectButtons[_currentSelect].gameObject == gameObject;

    void Start()
    {
        //セレクターの初期設定
        InitializeSelector();
    }

    void Update()
    {
        if (!_isRegistered) return;

        if (IsSelectOnStart)
        {
            //スタートボタンが押された場合
            ReadyImage readyImage = transform.Find("ReadyImage").gameObject.GetComponent<ReadyImage>();

            //選択状態を変更する
            if (_isDetermined)
            {
                //現在が選択済み状態の場合
                readyImage.ChangeState(ReadyImage.State.SELECTED);
                _isDetermined = false;
            }
            else
            {
                //現在が選択中状態の場合
                readyImage.ChangeState(ReadyImage.State.DETERMINED);
                _isDetermined = true;
            }
            return;
        }
        
        //表示キャラクターの色を選択色に
        if (IsChangedColor)
        {
            //_selectButtons[(int)ButtonItem.CHARACTOR].gameObject.GetComponent<CharactorSelectButton>().SetSelectedCharactorColor(SelectedColor);
            _selectButtons[(int)ButtonItem.CHARACTOR].gameObject.GetComponent<CharactorSelectButton>().SetSelectedCharactorColorTexture();
            IsChangedColor = false;
        }

        //選択中のボタンが最上下で瞬間移動しないよう移動後一定時間操作を受け付けない
        if (_isWaiting)
        {
            _deltaTime += Time.deltaTime;

            if (_deltaTime < SelectMoveWaitTime) return;

            _deltaTime = 0;
            _isWaiting = false;
        }

        //入力に合わせた処理
        if (IsSelectOnUp)
        {
            //上入力が行われた場合
            ChangeSelectedUp();
        }
        else if (IsSelectOnDown)
        {
            //下入力が行われた場合
            ChangeSelectedDown();
        }
        else if (IsSelectOnSpace)
        {
            //スペースキー（ゲームパッド_A）入力が行われた場合
            ChangeSelectedDown();
        }

    }

    void LateUpdate()
    {        
        if (IsSelectOnLeft)
        {
            //左入力が行われた場合
            _isWaiting = true;

            return;
        }
        if (IsSelectOnRight)
        {
            //右入力が行われた場合
            _isWaiting = true;

            return;
        }
    }

    //選択項目を上に移動
    public void ChangeSelectedUp()
    {
        //選択範囲外に出ないようチェック
        if (_currentSelect == 0) return;

        _currentSelect--;
        _isWaiting = true;

        transform.Find("Arrow").transform.localPosition = SelectArrowPositionList[_currentSelect];

        return;
    }

    //選択項目を下に移動
    public void ChangeSelectedDown()
    {
        //選択範囲外に出ないようチェック
        if (_currentSelect >= (int)ButtonItem.MAX - 1) return;

        _currentSelect++;
        _isWaiting = true;

        transform.Find("Arrow").transform.localPosition = SelectArrowPositionList[_currentSelect];

        return;
    }

    //セレクターの有効化
    public void ActivateSelector()
    {
        //選択ボタンの有効化
        foreach(Button button in _selectButtons)
        {
            button.gameObject.SetActive(true);
        }

        //ブラインドの無効化
        transform.Find("BlindcurtainImage").gameObject.SetActive(false);

        //選択矢印の有効化
        transform.Find("Arrow").gameObject.SetActive(true);

        _isRegistered   = true;
        _isWaiting      = true;
    }

    //セレクターの準備
    void InitializeSelector()
    {
        //選択項目ごとのボタンを取得
        for (int i = 0; i < (int)ButtonItem.MAX; i++)
        {
            _selectButtons[i] = gameObject.transform.GetChild(i).GetComponent<Button>();
            _selectButtons[i].gameObject.SetActive(false);
        }
    }
}
