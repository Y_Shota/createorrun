using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectButton : MonoBehaviour
{
    [Tooltip("選択可能なステージ数")]
    [SerializeField] private int StageCountMax;

    //現在選択している番号
    int _currentSelect;

    //選択されているステージID
    public int SelectedId => _currentSelect;

    //表示するテキスト
    public string SelectedIdText => (_currentSelect == StageCountMax) ? "?" : (_currentSelect + 1).ToString();

    //ランダムステージとして扱うステージID
    public int RandomStageId => StageCountMax;

    //乱数でステージIDを生成
    public int RandomSelectStageId => Random.Range(0, StageCountMax);
    
    void Start()
    {
        _currentSelect = 0;
        transform.Find("Text").GetComponent<Text>().text = SelectedIdText;
    }

    void Update()
    {
        //入力を受け付けているかを確認しする
        PlayerSelectorDirector playerSelectorDirector = transform.parent.GetComponent<PlayerSelectorDirector>();
        if (!playerSelectorDirector.IsSelected(gameObject)) return;
        if (playerSelectorDirector.IsWaiting) return;

        //左右方向の入力に対する処理
        if (playerSelectorDirector.IsSelectOnLeft) ChangeSelectedStage(Direction.Left);
        if (playerSelectorDirector.IsSelectOnRight) ChangeSelectedStage(Direction.Right);
    }

    void ChangeSelectedStage(Direction direction)
    {
        //効果音
        SelectSceneSEManager.Instance.PlaySelectSE();

        int newSelect = _currentSelect + direction.Sign;    //新しく選択される要素
        int lastCount = StageCountMax;                      //配列の最終要素

        //配列サイズから出ていないかチェック
        if (newSelect < 0)          newSelect = lastCount;
        if (newSelect > lastCount)  newSelect = 0;

        //選択ステージを変更
        _currentSelect = newSelect;
        transform.Find("Text").GetComponent<Text>().text = SelectedIdText;
    }


}
