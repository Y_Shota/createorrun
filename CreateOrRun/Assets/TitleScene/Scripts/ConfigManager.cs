using UnityEngine;
using UnityEngine.UI;

public class ConfigManager : SingletonMonoBehaviour<ConfigManager>
{
    GameObject _currentObject;
    GameObject _prevObject;

    //BGM音量
    public float VolumeBGM { get; private set; } = 0.5f;

    //SE音量
    public float VolumeSE { get; private set; } = 1f;

    //エフェクトのサイズ？
    public float EffectQuality { get; private set; } = 1f;

    void Start()
    {
        //シーン遷移でオブジェクトを破棄しない
        DontDestroyOnLoad(gameObject);                
    }

    public void SetUpConfig(Slider sliderBGM, Slider sliderSE, Slider sliderEffectQuality)
    {
        //スライダーの値が変更したらそれに対応した値が変わる
        sliderBGM.onValueChanged.AddListener(value => VolumeBGM = value);
        sliderBGM.onValueChanged.AddListener(delegate { TestSEDirector.Instance.PlaySelectSE(); });
        sliderSE.onValueChanged.AddListener(value => VolumeSE = value);
        sliderSE.onValueChanged.AddListener(delegate { TestSEDirector.Instance.PlaySelectSE(); });
        sliderEffectQuality.onValueChanged.AddListener(value => EffectQuality = value);
        sliderEffectQuality.onValueChanged.AddListener(delegate { TestSEDirector.Instance.PlaySelectSE(); });
    }
}
