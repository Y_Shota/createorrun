using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSEDirector :  SingletonMonoBehaviour<TestSEDirector>
{
    [Tooltip("セレクト音")]
    [SerializeField] private AudioClip SelectSE;
    [Tooltip("決定音")]
    [SerializeField] private AudioClip DecisionSE;

    private AudioSource _audioSource;


    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(this);
    }

    void Update()
    {
    }

    public void PlaySelectSE()
    {
        _audioSource.volume = ConfigManager.Instance.VolumeSE;
        _audioSource.PlayOneShot(SelectSE);
    }

    public void PlayDecisionSE()
    {
        _audioSource.volume = ConfigManager.Instance.VolumeSE;
        _audioSource.PlayOneShot(DecisionSE);
    }
}
