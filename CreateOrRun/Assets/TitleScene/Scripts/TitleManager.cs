using UnityEngine;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{
    [Tooltip("BGM音量スライダー")]
    [SerializeField] private Slider SliderBGM;
    [Tooltip("SE音量スライダー")]
    [SerializeField] private Slider SliderSE;
    [Tooltip("エフェクトスライダー")]
    [SerializeField] private Slider SliderEffectQuality;

    void Start()
    {
        ConfigManager.Instance.SetUpConfig(SliderBGM, SliderSE, SliderEffectQuality);
        Destroy(gameObject);
    }

    void Update()
    {
    }
}
