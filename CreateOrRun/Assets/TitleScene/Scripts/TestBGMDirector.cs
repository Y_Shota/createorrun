using UnityEngine;

public class TestBGMDirector : MonoBehaviour
{
    void Start()
    {
        //セレクトシーンでもBGM続行
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        GetComponent<AudioSource>().volume = ConfigManager.Instance.VolumeBGM;
    }
}