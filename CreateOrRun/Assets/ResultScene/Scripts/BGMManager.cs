using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("ConfigManager"))
            GetComponent<AudioSource>().volume = ConfigManager.Instance.VolumeBGM;
        else
            GetComponent<AudioSource>().volume = 0.5f;


    }

}
