using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinnerGenerator : MonoBehaviour
{    
    [SerializeField] private List<GameObject> ModelPrefabs;
    private Animator _animator;

    private void Start()
    {       
        //ランキングマネージャ
        var rankingManager = RankingManager.Instance;
        if (!rankingManager)
        {
            Instantiate(ModelPrefabs[0], transform);
            _animator = GetComponentInChildren<Animator>();
            return;
        }
        //勝者の取得＆表示
        //var text = GetComponent<Text>();
        //text.text = (rankingManager.GetTopPlayerId + 1) + "Pの勝ち";

        var selectedDatastorageObject = GameObject.Find("SelectedDataStorage");
        if (selectedDatastorageObject)
        {
            //モデル表示
            InstantiateWinnerModel(rankingManager.GetTopPlayerId, selectedDatastorageObject);
            _animator = GetComponentInChildren<Animator>();
            return;
        }
        Instantiate(ModelPrefabs[0], transform);
        _animator = GetComponentInChildren<Animator>();

    }

    private void Update()
    {
        var info = _animator.GetCurrentAnimatorStateInfo(0);
        if (info.normalizedTime >= 1 && info.IsName("Winpose"))
        {
            StartCoroutine(WaitAnimation());
        }
    }

    private void InstantiateWinnerModel(int id, GameObject gameObject)
    {
        SelectedDataStorage selectedDataStorage = gameObject.GetComponent<SelectedDataStorage>();
        GameObject winner = Instantiate(ModelPrefabs[selectedDataStorage.PlayerCharacter(id)], this.transform);
        int usedMaterialCount = 0;
        foreach (Transform child in winner.transform)
        {
            Renderer renderer = child.GetComponent<Renderer>();
            if (renderer == null) continue;
            if (usedMaterialCount >= selectedDataStorage.PlayerMaterials(id).Count) break;

            Material[] materials = new Material[renderer.materials.Length];
            for (int i = 0; i < renderer.materials.Length; i++)
            {
                materials[i] = selectedDataStorage.PlayerMaterials(id)[usedMaterialCount + i];
            }
            renderer.materials = materials;
            usedMaterialCount += renderer.materials.Length;
        }
    }

    private IEnumerator WaitAnimation()
    {
        yield return new WaitForSeconds(1);
        _animator.SetBool("isWinPoseFinished", true);
    }
}
